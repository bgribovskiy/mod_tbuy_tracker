<?php
/**
 * Created by PhpStorm.
 * User: giuseppe.guarino
 * Date: 21/09/15
 * Time: 16.33
 */

$old_config = (boolean) Mage::getStoreConfig('tracker/google_tag_manager/all_data_on_head');
if ($old_config === true) {
    $coreChange = new Mage_Core_Model_Config();
    $data = [
        Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_PROD,
        Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_CATEGORY,
        Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_SEARCH,
        Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_THANKYOU
    ];
    $coreChange->saveConfig('tracker/google_tag_manager/all_data_on_head', implode(",", $data));
}
