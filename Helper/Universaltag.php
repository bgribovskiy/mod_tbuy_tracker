<?php

class Tbuy_Tracker_Helper_Universaltag extends Tbuy_Tracker_Helper_Data {

	const TRACKER_CODE = 'universaltag';

	public function isEnabled($module = self::TRACKER_CODE) {
		return Mage::getStoreConfig("tracker/{$module}/enabled");
	}
}
