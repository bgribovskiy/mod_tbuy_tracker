<?php

class Tbuy_Tracker_Helper_Facebookpixel extends Tbuy_Tracker_Helper_Data
{

    const TRACKER_CODE = 'facebookpixel';

    /**
     * @param string $module
     * @return mixed
     */
    public function isEnabled($module = self::TRACKER_CODE)
    {
        return Mage::getStoreConfig("tracker/{$module}/enabled");
    }

    /**
     * @param string $module
     * @return bool
     */
    public function isEnabledDpa($module = self::TRACKER_CODE)
    {
        if (!$this->isEnabled()) {
            return false;
        }

        return Mage::getStoreConfigFlag("tracker/{$module}/enabled_dpa");
    }

    /**
     * @return mixed|string
     */
    public function isEnabledForViewContent()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/enabled_for_view_content");
    }

    /**
     * @return mixed|string
     */
    public function viewContent()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/view_content");
    }
    
    /**
     * @return mixed|string
     */
    public function isEnabledForSearch()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/enabled_for_search");
    }

    /**
     * @return mixed|string
     */
    public function search()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/search");
    }

    /**
     * @return mixed|string
     */
    public function isEnabledForAddToCart()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/enabled_for_add_to_cart");
    }

    /**
     * @return mixed|string
     */
    public function addToCart()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/add_to_cart");
    }

    /**
     * @return mixed|string
     */
    public function isEnabledForAddToWishlist()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/enabled_for_add_to_wishlist");
    }

    /**
     * @return mixed|string
     */
    public function addToWishlist()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/add_to_wishlist");
    }

    /**
     * @return mixed|string
     */
    public function isEnabledForInitiateCheckout()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/enabled_for_initiate_checkout");
    }

    /**
     * @return mixed|string
     */
    public function initiateCheckout()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/initiate_checkout");
    }

    /**
     * @return mixed|string
     */
    public function isEnabledForAddPaymentInfo()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/enabled_for_add_payment_info");
    }

    /**
     * @return mixed|string
     */
    public function addPaymentInfo()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/add_payment_info");
    }

    /**
     * @return mixed|string
     */
    public function isEnabledForPurchase()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/enabled_for_purchase");
    }

    /**
     * @return mixed|string
     */
    public function purchase()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/purchase");
    }

    /**
     * @return mixed|string
     */
    public function isEnabledForLead()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/enabled_for_lead");
    }

    /**
     * @return mixed|string
     */
    public function lead()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/lead");
    }

    /**
     * @return mixed|string
     */
    public function isEnabledForCompleteRegistration()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/enabled_for_complete_registration");
    }

    /**
     * @return mixed|string
     */
    public function completeRegistration()
    {
        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/complete_registration");
    }


    /**
     * @return string
     */
    public function getDefaultTrackType(){

        if (!$this->isEnabled()) {
            return "";
        }
        return Mage::getStoreConfig("tracker/facebookpixel/default_track_type");
    }
}
