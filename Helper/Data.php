<?php

class Tbuy_Tracker_Helper_Data extends Mage_Core_Helper_Abstract {

	const GLOBAL_COLLECT_PAYMENT_METHOD = "globalcollect_merchant_link";
	const GLOBAL_COLLECT_PAYMENT_METHOD_HOSTED = "globalcollect_merchant_link_hosted";
	const GESTPAYS2S_PAYMENT_METHOD = "gestpays2s";
	const GOOGLE_ADS_PAGE_TYPE_HOME = 'home';
	const GOOGLE_ADS_PAGE_TYPE_SEARCH_RESULTS = 'searchresults';
	const GOOGLE_ADS_PAGE_TYPE_CATEGORY = 'category';
	const GOOGLE_ADS_PAGE_TYPE_PRODUCT = 'product';
	const GOOGLE_ADS_PAGE_TYPE_OTHER = 'other';
	const GOOGLE_ADS_PAGE_TYPE_CART = 'cart';
	const GOOGLE_ADS_PAGE_TYPE_PURCHASE = 'purchase';

	/** @var string used by the Tbuy_Holes module */
	const HOLE_PUNCHING_GOOGLE_ADS_KEY = "tracker.googleads";

	/** @var string used by the Tbuy_Holes module */
	const HOLE_PUNCHING_TRACKER_KEY = "tracker";
	const DEBUG_LOG_FILE = "tbuy_tracker.log";

	public function getPaymentProductCode($payment) {
		$paymentMethod = $payment->getMethod();
		$key = $paymentMethod;
		if ($paymentMethod == self::GLOBAL_COLLECT_PAYMENT_METHOD_HOSTED) {
			$additionalInfo = $payment->getAdditionalInformation();
			$gcPaymentProducId = $additionalInfo['payment_product_id'];
			$key = "gc_{$gcPaymentProducId}";
		} elseif ($paymentMethod == self::GESTPAYS2S_PAYMENT_METHOD) {
			$ccTypeCode = $payment->getCcType();
			$key = "gp_{$ccTypeCode}";
		}
		return $key;
	}

    /**
     * Get URL path
     *
     * @return string
     */
    public function getUrlPath() {
        $request = Mage::app()->getRequest();
        // get the module name
        $moduleName = $request->getModuleName();
        if($moduleName == 'pcheckout') {
            // fix for piquadro override
            $moduleName = 'checkout';
        }
        $controllerName = $request->getControllerName();
        $actionName = $request->getActionName();
        $path = implode('_', array(
            $moduleName,
            $controllerName,
            $actionName,
        ));
        return $path;
    }

	public function log($msg, $level = Zend_Log::DEBUG) {
		if (!Mage::getStoreConfig('tracker/log/enabled') || $msg == "")
			return "";
		Mage::log($msg, $level, self::DEBUG_LOG_FILE);
	}

	public function isCriteoEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/criteo/enabled', $storeId);
	}

	public function isCriteoDetectDeviceEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/criteo/detectdevice', $storeId);
	}

	public function getCriteoDefaultDevice($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/criteo/defaultdevice', $storeId);
	}

	public function isCriteoHomeEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return $this->isCriteoEnabled($storeId) && Mage::getStoreConfig('tracker/criteo/home_enabled', $storeId);
	}

	public function isCriteoListingEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return $this->isCriteoEnabled($storeId) && Mage::getStoreConfig('tracker/criteo/listing_enabled', $storeId);
	}

	public function isCriteoBasketEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return $this->isCriteoEnabled($storeId) && Mage::getStoreConfig('tracker/criteo/basket_enabled', $storeId);
	}

	public function getCriteoLoader($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/criteo/loader', $storeId);
	}

	public function getCriteoUseSku($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/criteo/use_skus', $storeId);
	}

    public function getAppendStoreId($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
        return Mage::getStoreConfig('tracker/criteo/append_store_id', $storeId);
    }

	public function getCriteoUseConfigurable($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/criteo/use_configurable_in_cart', $storeId);
	}

	public function getCriteoAccount($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/criteo/account', $storeId);
	}

	public function getCriteoListingLimit($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/criteo/listing_limit', $storeId);
	}

	public function isGoogleAdsEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		if (Mage::getStoreConfig('tracker/google_ads/feed_dependence', $storeId)) {
			return Mage::getStoreConfig('feed/googlemerchant/enabled', $storeId) && Mage::getStoreConfig('tracker/google_ads/enabled', $storeId);
		}
		return Mage::getStoreConfig('tracker/google_ads/enabled', $storeId);
	}

	public function getGoogleAdsConversionId($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/google_ads/conversion_id', $storeId);
	}

	public function getGoogleAdsConversionLabel($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/google_ads/conversion_label', $storeId);
	}

	public function getGoogleAdsPageTypeHome() {
		return self::GOOGLE_ADS_PAGE_TYPE_HOME;
	}

	public function getGoogleAdsPageTypeSearchResults() {
		return self::GOOGLE_ADS_PAGE_TYPE_SEARCH_RESULTS;
	}

	public function getGoogleAdsPageTypeCategory() {
		return self::GOOGLE_ADS_PAGE_TYPE_CATEGORY;
	}

	public function getGoogleAdsPageTypeProduct() {
		return self::GOOGLE_ADS_PAGE_TYPE_PRODUCT;
	}

	public function getGoogleAdsPageTypeOther() {
		return self::GOOGLE_ADS_PAGE_TYPE_OTHER;
	}

	public function getGoogleAdsPageTypeCart() {
		return self::GOOGLE_ADS_PAGE_TYPE_CART;
	}

	public function getGoogleAdsPageTypePurchase() {
		return self::GOOGLE_ADS_PAGE_TYPE_PURCHASE;
	}

	public function getGoogleAdsIsReccomendedEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/google_ads/enable_reccomended', $storeId);
	}

	public function getGoogleAdsIsCategoryEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/google_ads/enable_category', $storeId);
	}

	public function getGoogleAdsIsPValueEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/google_ads/enable_pvalue', $storeId);
	}

	public function getGoogleAdsIsQuantityEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/google_ads/enable_quantity', $storeId);
	}

	public function getGoogleAdsIsCustomerAgeEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/google_ads/enable_age', $storeId);
	}

	public function getGoogleAdsIsCustomerGenderEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/google_ads/enable_gender', $storeId);
	}

	public function getGoogleAdsIsCustomerHasAccountEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/google_ads/enable_hasaccount', $storeId);
	}

	public function getGoogleAdsIsCustomerCqsEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/google_ads/enable_cqs', $storeId);
	}

	public function getGoogleAdsIsCustomerRpEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/google_ads/enable_rp', $storeId);
	}

	public function getGoogleAdsIsCustomerLyEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/google_ads/enable_ly', $storeId);
	}

	public function getGoogleAdsIsCustomerHsEnabled($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/google_ads/enable_hs', $storeId);
	}

	public function getGoogleAdsValidOrderStatuses($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
		return Mage::getStoreConfig('tracker/google_ads/order_statuses', $storeId);
	}

    /**
     * Check if we need to use dimension6 data in checkout page
     *
     * @param null|int $storeId
     * @return bool
     */
    public function addDimension6DataOnCheckOut($storeId = null) {
        return (bool) Mage::getStoreConfig('tracker/google_tag_manager/use_dimension_six_in_checkout', $storeId);
    }

	public function getAgeFromDob($dob) {
		return floor((time() - strtotime($dob)) / 31556926);
	}

	private function buildCategoriesMultiselectValues(Varien_Data_Tree_Node $node, $values, $level = 0) {
		$level++;

		$nonEscapableNbspChar = html_entity_decode('&#160;', ENT_NOQUOTES, 'UTF-8');

		$values[$node->getId()]['value'] = $node->getId();
		$values[$node->getId()]['label'] = str_repeat($nonEscapableNbspChar, ($level - 1) * 4) . $node->getName();
		$values[$node->getId()]['is_active'] = $node->getIsActive();

		foreach ($node->getChildren() as $child)
			$values = $this->buildCategoriesMultiselectValues($child, $values, $level);

		return $values;
	}

	public function load_tree() {
		$tree = Mage::getResourceSingleton('catalog/category_tree')->load();

		//$store = 1;
		$parentId = 1;

		$tree = Mage::getResourceSingleton('catalog/category_tree')->load();

		$root = $tree->getNodeById($parentId);

		if ($root && $root->getId() == 1)
			$root->setName(Mage::helper('catalog')->__('Root'));

		$collection = Mage::getModel('catalog/category')->getCollection()
				//->setStoreId($store)
				->addAttributeToSelect('name')
				->addAttributeToSelect('is_active');

		$tree->addCollectionData($collection, true);

		return $this->buildCategoriesMultiselectValues($root, array());
	}

    /**
     * Get all products sku in cart
     *
     * @return array
     */
    public function getCartProductsSku()
    {
        $skus = array();
        $cart = Mage::getModel('checkout/cart')->getQuote();
        foreach ($cart->getAllItems() as $item) {
            $skus[] = $item->getProduct()->getId().'_'.Mage::app()->getStore()->getStoreId();
        }

        return $skus;
    }

    /**
     * Get all products sku in order
     * @order Mage_Sales_Model_Order
     * @return array
     */
    public function getOrderProductsSku(Mage_Sales_Model_Order $order)
    {
        $skus = array();
        $items = $order->getAllVisibleItems();
        foreach ($items as $item) {
            $skus[] = $item->getProduct()->getId().'_'.Mage::app()->getStore()->getStoreId();
        }

        return $skus;
    }
}
