<?php

class Tbuy_Tracker_Helper_Webtrekk extends Tbuy_Tracker_Helper_Data {

	const GLOBAL_COLLECT_PAYMENT_METHOD = "globalcollect_merchant_link_hosted";
	const WEBTREKK_FUNCTION_PREFIX = "webtrekk";

	public function getProductCategory($product) {
		$category = null;
		$currentStore = Mage::app()->getStore()->getId();
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		if (Mage::registry("current_category"))
			$category = Mage::getModel('catalog/category')->load(Mage::registry("current_category")->getId());

		if (!$category) {
			$categories = $product->getCategoryCollection();
			foreach ($categories as $cat) {
				//	level is lower
				if (!is_null($category) && $cat->getLevel() < $category->getLevel())
					continue;

				//	level is equal but id is newer (take the old one)
				if (!is_null($category) && ($cat->getLevel() == $category->getLevel() && $cat->getId() > $category->getId()))
					continue;
				$category = $cat;
			}
		}
                
                if($category)
                    $category = Mage::getModel('catalog/category')->load($category->getId());
                
		Mage::app()->setCurrentStore($currentStore);
		return !$category ? "" : $category->getName();
	}

	public function myJsonEncode($arr) {
		$json = array();
		foreach ($arr as $k => $val) {
			$json[] = "{$k}:\"{$val}\"";
		}
		return "{" . implode(",", $json) . "}";
	}

	public function getDefaultStore() {
		return Mage::getStoreConfig("webtrekk/settings/currency_default") != "" ? Mage::getStoreConfig("webtrekk/settings/currency_default") != "" : Mage_Core_Model_App::ADMIN_STORE_ID;
	}

	public function getBaseStoreId() {
		if (Mage::getStoreConfig("webtrekk/settings/currency_default") != "")
			return Mage::getStoreConfig("webtrekk/settings/currency_default");
		return Mage::app()
						->getWebsite()
						->getDefaultGroup()
						->getDefaultStoreId();
	}

	public function getBaseCurrency() {
		$store = $this->getDefaultStore();
		if ($store > 0)
			return Mage::app()->getStore($this->getBaseStoreId())->getCurrentCurrencyCode();
		else
			return Mage::app()->getBaseCurrencyCode();
	}

	public function log($msg, $level = Zend_Log::DEBUG) {
		if (!Mage::getStoreConfig("webtrekk/settings/log") || $msg == "")
			return "";
		Mage::log($msg, $level, self::DEBUG_LOG_FILE);
	}

}
