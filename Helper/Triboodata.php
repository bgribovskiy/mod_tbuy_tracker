<?php

class Tbuy_Tracker_Helper_Triboodata extends Tbuy_Tracker_Helper_Data {

    const TRACKER_CODE = 'triboodata';

    public function isEnabled($module = self::TRACKER_CODE) {
        return Mage::getStoreConfig("tracker/{$module}/enabled");
    }

}