<?php

class Tbuy_Tracker_Helper_Yandex extends Tbuy_Tracker_Helper_Data {

    const TRACKER_CODE = 'yandex';

    public function isEnabled($module = self::TRACKER_CODE) {
        return Mage::getStoreConfig("tracker/{$module}/enabled");
    }

}
