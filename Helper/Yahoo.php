<?php

class Tbuy_Tracker_Helper_Yahoo extends Tbuy_Tracker_Helper_Data {

	const TRACKER_CODE = 'yahoo';

	public function isEnabled($module = self::TRACKER_CODE) {
		return Mage::getStoreConfig("tracker/{$module}/enabled");
	}

	public function isHomeEnabled($module = self::TRACKER_CODE) {
		return $this->isEnabled() && Mage::getStoreConfig("tracker/{$module}/home_enabled");
	}
	
	public function isProductEnabled($module = self::TRACKER_CODE) {
		return $this->isEnabled() && Mage::getStoreConfig("tracker/{$module}/product_enabled");
	}

	public function isListingEnabled($module = self::TRACKER_CODE) {
		return $this->isEnabled() && Mage::getStoreConfig("tracker/{$module}/listing_enabled");
	}

	public function isBasketEnabled($module = self::TRACKER_CODE) {
		return $this->isEnabled() && Mage::getStoreConfig("tracker/{$module}/basket_enabled");
	}
        
	public function isWishlistEnabled($module = self::TRACKER_CODE) {
		return $this->isEnabled() && Mage::getStoreConfig("tracker/{$module}/wishlist_enabled");
	}
	
	public function isSuccessPageEnabled($module = self::TRACKER_CODE) {
		return $this->isEnabled() && Mage::getStoreConfig("tracker/{$module}/success_page_enabled");
	}
	
	public function getUseSku($module = self::TRACKER_CODE) {
		return Mage::getStoreConfig("tracker/{$module}/use_skus");
	}

	public function getUseConfigurable($module = self::TRACKER_CODE) {
		return Mage::getStoreConfig("tracker/{$module}/use_configurable_in_cart");
	}

	public function getTag($module = self::TRACKER_CODE) {

        $path = Mage::helper('tracker')->getUrlPath();

		if ($path == "cms_index_noRoute")
			$path = "cms_index_index";

		$tags = array();
		if (Mage::getStoreConfig("tracker/{$module}/tags") != "") {
			$tags = unserialize(Mage::getStoreConfig("tracker/{$module}/tags"));
		}
		return isset($tags[$path]['code']) ? $tags[$path]['code'] : "0";
	}

	public function getListingLimit($module = self::TRACKER_CODE) {
		return Mage::getStoreConfig("tracker/{$module}/listing_limit");
	}

}
