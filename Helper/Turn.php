<?php

class Tbuy_Tracker_Helper_Turn extends Tbuy_Tracker_Helper_Data {

    const TRACKER_CODE = 'turn';

    public function isEnabled($module = self::TRACKER_CODE) {
        return Mage::getStoreConfig("tracker/{$module}/enabled");
    }


	public function getTurnCode($module = self::TRACKER_CODE) {

		return Mage::getStoreConfig("tracker/{$module}/code");
	}

	public function getCurrentCategoryCode($module = self::TRACKER_CODE) {

		$_current_category = Mage::registry('current_category');
		if(is_object($_current_category)){
			$cat_Id = $_current_category->getId();
			$category_codes =  unserialize(Mage::getStoreConfig("tracker/{$module}/category_codes"));
			if(isset($category_codes[$cat_Id])){
				return $category_codes[$cat_Id]['code'];
			}
		return "";
		}
		return "2";
	}


}
