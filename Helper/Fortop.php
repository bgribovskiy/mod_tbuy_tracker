<?php

class Tbuy_Tracker_Helper_Fortop extends Tbuy_Tracker_Helper_Data {

	const TRACKER_CODE = 'fortop';

	public function isEnabled($module = self::TRACKER_CODE) {
		return Mage::getStoreConfig("tracker/{$module}/enabled");
	}

	public function getOfferId(){
		if (!$this->isEnabled()) {
			return "";
		}
		return Mage::getStoreConfig("tracker/fortop/offer_id");
	}

}
