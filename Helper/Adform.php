<?php

class Tbuy_Tracker_Helper_Adform extends Tbuy_Tracker_Helper_Data {

	const TRACKER_CODE = 'adform';

	public function isEnabled($module = self::TRACKER_CODE) {
		return Mage::getStoreConfig("tracker/{$module}/enabled");
	}

	public function getCategories() {
		if (!Mage::registry("current_category"))
			return array();

		$category = Mage::getModel('catalog/category')->load(Mage::registry("current_category")->getId());
		$coll = $category->getResourceCollection();
		$pathIds = $category->getPathIds();
		$coll->addAttributeToSelect('name');
		$coll->addAttributeToFilter('entity_id', array('in' => $pathIds));
		$coll->addAttributeToFilter('level', array('gt' => 1));
		foreach ($coll as $cat)
			$cg[] = $cat->getName();
		return $cg;
	}

	public function getProduct() {
		if (!Mage::registry("current_product"))
			return null;
		$product = Mage::registry("current_product"); //Mage::getModel("catalog/product")->load(Mage::registry("current_product")->getId());
		return $product;
	}

	public function getProductCategory($product) {
		$category = null;
		$currentStore = Mage::app()->getStore()->getId();
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		if (Mage::registry("current_category"))
			$category = Mage::getModel('catalog/category')->load(Mage::registry("current_category")->getId());

		if (!$category) {
			$categories = $product->getCategoryCollection();
			foreach ($categories as $cat) {
				//	level is lower
				if (!is_null($category) && $cat->getLevel() < $category->getLevel())
					continue;

				//	level is equal but id is newer (take the old one)
				if (!is_null($category) && ($cat->getLevel() == $category->getLevel() && $cat->getId() > $category->getId()))
					continue;
				$category = $cat;
			}
		}
		
		if($category){
			$category = Mage::getModel('catalog/category')->load($category->getId());
		}
		
		Mage::app()->setCurrentStore($currentStore);
		return !$category ? "" : $category->getName();
	}

	public function getUrlPath() {
        $path = Mage::helper('tracker')->getUrlPath();
        return $path;
	}

	public function getCurrentUrlPath() {
		$baseUrl = Mage::getBaseUrl();
		$path = str_replace($baseUrl, "", Mage::helper('core/url')->getCurrentUrl());
		$path = preg_replace("/^(.*)\.html\/?.*/", "$1", $path);
		return $path;
	}

	public function isProductPage() {
		return $this->getUrlPath() == 'catalog_product_view';
	}

	public function isCartPage() {
		return $this->getUrlPath() == 'checkout_cart_index';
	}

	public function isSuccessPage() {
		return $this->getUrlPath() == 'checkout_onepage_success';
	}

	public function getTag($module = self::TRACKER_CODE) {

		$path = Mage::helper('tracker')->getUrlPath();

		if ($path == "cms_index_noRoute")
			$path = "cms_index_index";

		$tags = array();
		if (Mage::getStoreConfig("tracker/{$module}/pointid") != "") {
			$tags = unserialize(Mage::getStoreConfig("tracker/{$module}/pointid"));
		}
		return isset($tags[$path]['code']) ? $tags[$path]['code'] : "0";
	}

	public function isEnabledForHomepage(){
		if (!$this->isEnabled()) {
			return "";
		}
		return Mage::getStoreConfig("tracker/adform/enabled_for_homepage");
	}

	public function homePagename(){
		if (!$this->isEnabled()) {
			return "";
		}
		return Mage::getStoreConfig("tracker/adform/home_pagename");
	}

	public function isEnabledForProduct(){
		if (!$this->isEnabled()) {
			return "";
		}
		return Mage::getStoreConfig("tracker/adform/enabled_for_product");
	}

	public function productPagename(){
		if (!$this->isEnabled()) {
			return "";
		}
		return Mage::getStoreConfig("tracker/adform/product_pagename");
	}

	public function isEnabledForCategory(){
		if (!$this->isEnabled()) {
			return "";
		}
		return Mage::getStoreConfig("tracker/adform/enabled_for_category");
	}

	public function categoryPagename(){
		if (!$this->isEnabled()) {
			return "";
		}
		return Mage::getStoreConfig("tracker/adform/category_pagename");
	}

	public function isEnabledForCart(){
		if (!$this->isEnabled()) {
			return "";
		}
		return Mage::getStoreConfig("tracker/adform/enabled_for_cart");
	}

	public function cartPagename(){
		if (!$this->isEnabled()) {
			return "";
		}
		return Mage::getStoreConfig("tracker/adform/cart_pagename");
	}
	public function isEnabledForCheckout(){
		if (!$this->isEnabled()) {
			return "";
		}
		return Mage::getStoreConfig("tracker/adform/enabled_for_checkout");
		}

	public function checkoutPagename(){
		if (!$this->isEnabled()) {
			return "";
		}
		return Mage::getStoreConfig("tracker/adform/checkout_pagename");
	}

	public function isEnabledForSuccess(){
		if (!$this->isEnabled()) {
			return "";
		}
		return Mage::getStoreConfig("tracker/adform/enabled_for_successpage");
	}

	public function successPagename(){
		if (!$this->isEnabled()) {
			return "";
		}
		return Mage::getStoreConfig("tracker/adform/success_pagename");
	}
	public function isEnabledForStaticPage(){
		if (!$this->isEnabled()) {
			return "";
		}
		return Mage::getStoreConfig("tracker/adform/enabled_for_staticpage");
	}

	public function getCurrentCategoryCode($module = self::TRACKER_CODE) {

		$_current_category = Mage::registry('current_category');
		if(is_object($_current_category)){
			$cat_Id = $_current_category->getId();
			$category_codes =  unserialize(Mage::getStoreConfig("tracker/{$module}/category_codes"));
			if(isset($category_codes[$cat_Id])){
				return $category_codes[$cat_Id]['code'];
			}
		}
		return "";
	}

}
