<?php

class Tbuy_Tracker_Helper_Publicidees extends Mage_Core_Helper_Abstract {


    public function getTagHome(){
        
        $tagHome = Mage::getStoreConfig("publicidees/settings/tag_home",Mage::app()->getStore());
        return $tagHome; 
    }
    
    public function getTagCategory(){
        
        $tagCategory = Mage::getStoreConfig("publicidees/settings/tag_category",Mage::app()->getStore());
        return $tagCategory; 
    }
    
    public function getTagSubcategory(){
        
        $tagSubcategory = Mage::getStoreConfig("publicidees/settings/tag_subcategory",Mage::app()->getStore());
        return $tagSubcategory; 
    }
    
    public function getTagProduct(){
        
        $tagProduct = Mage::getStoreConfig("publicidees/settings/tag_product",Mage::app()->getStore());
        return $tagProduct; 
    }
    
    public function getTagCart(){
        
        $tagCart = Mage::getStoreConfig("publicidees/settings/tag_cart",Mage::app()->getStore());
        return $tagCart; 
    }
    
    public function getTagThankyouPage(){
        
        $tagThankyouPage = Mage::getStoreConfig("publicidees/settings/tag_thankyou_page",Mage::app()->getStore());
        return $tagThankyouPage; 
    }
}
