<?php
/**
 * Created by PhpStorm.
 * User: nicola
 * Date: 30/01/17
 * Time: 12.31
 */

class Tbuy_Tracker_Helper_Hubspot  extends Tbuy_Tracker_Helper_Data
{
    const TRACKER_CODE = 'hubspot';

    public function isEnabled($module = self::TRACKER_CODE)
    {
        return Mage::getStoreConfig("tracker/{$module}/enabled");
    }

    public function isEnabledRegistrationCheck($module = self::TRACKER_CODE)
    {
        return $this->isEnabled() && Mage::getStoreConfig("tracker/{$module}/check_registration");
    }

    public function isEnabledLoginCheck($module = self::TRACKER_CODE)
    {
        return $this->isEnabled() && Mage::getStoreConfig("tracker/{$module}/check_login");
    }

    public function getTrackingCode($module = self::TRACKER_CODE)
    {
        return Mage::getStoreConfig("tracker/{$module}/trackingcode");
    }

    public function getFormGuid($module = self::TRACKER_CODE)
    {
        return Mage::getStoreConfig("tracker/{$module}/form_guid");
    }
}