<?php

class Tbuy_Tracker_Helper_Googletagmanager extends Mage_Core_Helper_Abstract
{

    const PAYMENT_METHOD_CASHONDELIVERY = "contrassegno";
    const PAYMENT_METHOD_BANKTRANSFER = "bonifico";
    const PAYMENT_METHOD_CREDITCARD = "Carta di Credito";
    const PAYMENT_METHOD_REALTIMEBANK = "Real-time Bank";

    const HEAD_DATA_PROD = "prod";
    const HEAD_DATA_CATEGORY = "cate";
    const HEAD_DATA_SEARCH = "search";
    const HEAD_DATA_THANKYOU = "thankyou";
    const HEAD_DATA_CHECKOUT = "checkout";

    protected static $prodPosition         = 1;
    protected        $allowResetPosition   = 1;
    protected        $_name_attribute_type = null;
    protected        $_name_attribute_id   = null;
    protected        $_tax_rates_by_class  = null;
    protected        $_product_json        = array();
    protected        $_color               = array();
    protected        $_default_store_id    = null;
    protected        $_globalCollectData   = null;
    protected        $_actionMapping       = array(
        "by_percent"      => array("type" => "%"),
        "by_fixed"        => array("type" => ""),
        "cart_fixed"      => array("type" => ""),
        //"tbuy_sd_percent" => array("type" => ""),
        "tbuy_sd_fixed"   => array("type" => ""),
        "tbuy_sd_percent" => array("type" => "%"),
        "to_fixed"        => array("type" => "")
    );
    static protected $categories = array();
    private $_sizes = array();
    private $_tags = array();

    /**
     * @param      $product
     * @param      $getCurrentCategory
     * @param null $childProductId
     *
     * @return mixed
     */
    protected function _json($product, $getCurrentCategory, $childProductId = null)
    {
        if (isset($this->_product_json[$product->getId() . $getCurrentCategory . $childProductId])) {
            return $this->_product_json[$product->getId() . $getCurrentCategory . $childProductId];
        }

        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $price        = $this->_price($product);

        // secondo le specifiche, vogliamo i nomi sempre e solo in italiano, su qualsiasi storeview
        $product = Mage::helper('utils/product')->loadProduct(
            $product, array("color", "sku", "google_tagmanager_brand"), $this->getDefaultStoreId()
        );

        $_name = Mage::getResourceModel('catalog/product')->getAttributeRawValue($product->getId(), "name", $this->getDefaultStoreId());

        // TODO refactoring: funzione con controllo anche sul parent se il child non ha il valore. Mettere in configurazione l'attributo "color"
        if (isset($this->_color[$product->getColor()])) {
            $color = $this->_color[$product->getColor()];
        } else {
            $colorId = Mage::getResourceModel('catalog/product')->getAttributeRawValue(
                $product->getId(), 'color', $this->getDefaultStoreId()
            );
            $color = Mage::helper('utils/product')->getAttributeValue(
                array("code" => "color", "value" => $colorId), $this->getDefaultStoreId()
            );
            $this->_color[$product->getColor()] = $color;
        }

        // XXX nel carrello e nel checkout manca la categoria del prodotto. siccome
        // non riesco a tirarla fuori modificando config.xml, carichiamola a manina
        // XXX pare che le categorie ignorino l'appEmulation di magento, per cui il
        // nome è sempre nella lingua dello store e non in quella che vogliamo noi.
        $categoryName = $this->_category($this->getDefaultStoreId(), $product, $getCurrentCategory);
        $tag_list = $this->_getTagList();

        $size = null;
        if (!is_null($childProductId) && $this->useDimension6()) {
            $size = $this->_getDimesion6Data($childProductId);
        }

        $jsonArray = array(
            'name'     => $_name,
            'id'       => $this->_getSku($product),
            'price'    => $price,
            'currency' => $currencyCode,
            'brand'    => $this->getCurrentBrand($product),
            'category' => $categoryName,
            'variant'  => $color,
            'list'     => $this->getPageCategory() . $tag_list,
            'position' => self::$prodPosition++
        );
        if (!is_null($size)) {
            $jsonArray['dimension6'] = $size;
        }

        if(Mage::getStoreConfigFlag('tracker/google_tag_manager/trigger_configurator_change') &&
            $product->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) {
            $jsonArray['master_id'] = $product->getSku();
        }

        $this->_product_json[$product->getId() . $getCurrentCategory . $childProductId] = $jsonArray;

	    // @TODO
	    // maybe it's better using objects
	    // jsonArray should be an object and passed to the event.
	    // Then in the observer get the object and modify the array inside the data
	    // and finally set the object again.
	    // set the object
	    //      $jsonObject = new Varien_Object();
	    //      $jsonObject->setData('json_array', $jsonArray);
	    // in the dispatch event params
	    //      array('json_object'=>$jsonObject, ...)
	    // in the observer
	    //      $jsonObject = $observer->getJsonObject()
	    //      $jsonArray = $jsonObject->getJsonArray();
	    //      ... (do stuff with jsonArray) ...
	    //      $jsonObject->setJsonArray($jsonArray);
	    // don't know if the next code is necessary
	    //      $observer->setJsonObject($jsonObject);
	    // after the dispatched event
	    //      $jsonArray = $jsonObject->getJsonArray();

	    Mage::dispatchEvent("tracker_google_tagmanager_after_jsonarray", array("product"=>$product, "jsonArray"=>$jsonArray, "keyIndex"=>$product->getId() . $getCurrentCategory . $childProductId, "obj"=>$this));

        return $this->_product_json[$product->getId() . $getCurrentCategory . $childProductId];
    }

    /**
     * @param $product Mage_Catalog_Model_Product
     * @return mixed
     */
    protected function _getSku($product) {
        $sku = $product->getData('sku');
        // Se l'opzione non è abilitata ritorno la sku del prodotto
        if(!Mage::getStoreConfigFlag('tracker/google_tag_manager/trigger_configurator_change')) {
            return $sku;
        }
        // se il prodotto non è configurabile ritorno la sku del prodotto
        if($product->getData('image') === null) {
            $product->load($product->getId(),array('image'));
        }
        $attribute = Mage::helper('configurator/swatches')->gettingFromImage($product);
        if($product->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE || !is_array($attribute)) {
            return $sku;
        }
        // calcolo la sku del child da ritornare
        $selected_child = null;
        $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $product);
        foreach($attribute as $value) {
            $attribute_object = Mage::getModel('eav/entity_attribute')->load($value['attribute_id']);
            if ($attribute_object->getAttributeCode() != 'color') {
                continue;
            }
            $selected_child = null;
            $allChildOutOfStockForAttribute = true;
            foreach ($childProducts as $child) {
                if ($child->getData($attribute_object->getAttributeCode()) == $value['option_id']) {
                    if ($child->getStockItem()->getStockStatus() > 0) {
                        $allChildOutOfStockForAttribute = false;
                        $selected_child = $child;
                        break;
                    }
                }
            }
            if ($allChildOutOfStockForAttribute) {
                foreach ($childProducts as $child) {
                    if ($child->getStockItem()->getStockStatus() > 0) {
                        $value['option_id'] = $child->getColor();
                        $selected_child = $child;
                        break;
                    }
                }
            }
        }
        if($selected_child !== null) {
            $sku = $selected_child->getSku();
        }
        return $sku;
    }

    protected function _getTagList()
    {
        $model = $key = $id = "";
        if (Mage::registry('current_category')) {
            $model = 'category';
        } else {
            if (Mage::registry("current_product")) {
                $model = 'product';
            } else {
                return $model;
            }
        }
        $id = Mage::registry("current_" . $model)->getId();
        $key = $model . $id;
        if (isset($this->_tags[$key])) {
            return $this->_tags[$key];
        }
        if ($model !== "") {
            $name = Mage::getResourceModel("catalog/" . $model)->getAttributeRawValue(
                $id, 'name', $this->getDefaultStoreId()
            );
            $this->_tags[$key] = "-" . str_replace(" ", "_", strtolower($name));
        } else {
            $this->_tags[$key] = "";
        }
        return $this->_tags[$key];
    }

    public function useDimension6($storeId = null)
    {
        return (bool)Mage::getStoreConfig("tracker/google_tag_manager/use_dimension_six", $storeId);
    }

    protected function _getDimesion6Data($childProductId)
    {
        if (isset($this->_sizes[$childProductId])) {
            return $this->_sizes[$childProductId];
        }
        if (!is_null($childProductId)) {
            if ($this->useDimension6() && Mage::getStoreConfig("tracker/google_tag_manager/multiplefilter")
            ) {
                $sizeAttributeCode = Mage::getStoreConfig("tracker/google_tag_manager/multiplefilter");
                $sizeId = Mage::getResourceModel('catalog/product')->getAttributeRawValue(
                    $childProductId, $sizeAttributeCode, $this->getDefaultStoreId()
                );
                $this->_sizes[$childProductId] = Mage::helper('utils/product')->getAttributeValue(
                    array("code" => "size", "value" => $sizeId), $this->getDefaultStoreId()
                );
            }
        }
        return $this->_sizes[$childProductId];
    }

    /**
     * Get the brand name
     *
     * @param object $_product
     * @return string
     */
    public function getCurrentBrand($_product)
    {
        $brand = $this->_getCurrentBrandByProduct($_product);
        if(!empty($brand)) {
            return (string) $brand;
        }
        if (Mage::getStoreConfig('tracker/google_tag_manager/use_category')) {
            $brand = $this->_getCurrentBrandByCategory($_product);
        } else {
            $brand = Mage::getStoreConfig('tracker/google_tag_manager/brand');
        }
        if (empty($brand)) {
            $brand = Mage::getStoreConfig('general/store_information/name');
        }

        return (string) $brand;
    }

    protected function _getCurrentBrandByProduct($_product) {
        $brand = null;
        if ($_product instanceof Varien_Object === false) {
            return $brand;
        }
        $brand_id = (int)$_product->getGoogleTagmanagerBrand();

        if ($brand_id > 0) {
            $brand = (isset(self::$categories[$brand_id]))
                ? self::$categories[$brand_id]
                : Mage::getResourceModel('catalog/category')->getAttributeRawValue(
                    $brand_id, 'name', $this->getDefaultStoreId()
                )
            ;
        }
        return $brand;
    }

    protected function _getCurrentBrandByCategory($_product)
    {
        $brand = null;
        if ($_product instanceof Varien_Object === false) {
            return $brand;
        }

        $result = null;

        $categories = $_product->getCategoryCollection();

        foreach ($categories as $category) {
            if (!$result) {
                $result = $category;
                continue;
            }

            if ($category->getLevel() > $result->getLevel()) {
                $result = $category;
                continue;
            }

            if ($category->getLevel() < $result->getLevel()) {
                continue;
            }

            // level is equal but id is newer (take the old one)
            if ($category->getId() > $result->getId()) {
                continue;
            }

            $result = $category;
        }


        // non dovrebbe succedere, ma per sicurezza...
        if (!$result) {
            return '';
        }

        $levelSplit = $this->getLevelSplit();
        $currentCategory = $result;
        if ($currentCategory instanceof Mage_Catalog_Model_Category) {
            $paths = explode("/", $currentCategory->getPath());
            foreach ($paths as $key => $categoryLevelId) {
                if ($key < $levelSplit) {
                    continue;
                }
                if (isset(self::$categories[$categoryLevelId])) {
                    $brand = self::$categories[$categoryLevelId];
                } else {
                    $brand = Mage::getResourceModel('catalog/category')->getAttributeRawValue(
                        $categoryLevelId, 'name', $this->getDefaultStoreId()
                    );
                }
                break;
            }
        }
        return $brand;
    }

    public function getDefaultStoreId()
    {
        if (is_null($this->_default_store_id)) {
            $this->_default_store_id = Mage::getStoreConfig('tracker/google_tag_manager/translate_to_store');
        }
        return $this->_default_store_id;
    }

    public function getUserData()
    {
        $session = Mage::getSingleton('customer/session');

        if (!$session->isLoggedIn()) {
            return array(
                'logged'    => 'non-loggato',
                'userId'    => '',
                'gender'    => '',
                'city'      => '',
                'email'     => '',
                'birthYear' => ''
            );
        }

        $user = $session->getCustomer();
        $gender = $user->getGender();
        $gender = Mage::getResourceModel('customer/customer')
            ->getAttribute('gender')
            ->getSource()
            ->getOptionText($gender);

        return array(
            'logged'    => 'loggato',
            'userId'    => $user->getId(),
            'gender'    => $gender,
            'city'      => $user->getCustomerRegisterCity(),
            'email'     => $user->getEmail(),
            'birthYear' => substr($user->getDob(), 0, 4)
        );
    }

    public function getProductJson($product, $childProductId = null)
    {
        $this->resetCounter();
        $getCurrentCategory = !Mage::getStoreConfigFlag("tracker/google_tag_manager/ignore_current_category");
        $data = $this->_json($product, $getCurrentCategory, $childProductId);
        return Zend_Json::encode($data);
    }

    /**
     * @param $collection
     * @return array
     */
    public function getImpressions($collection)
    {
        $impressions = array();
        $onClick = array();
        $this->resetCounter();
        foreach ($collection as $product) {
            $data = $this->_json($product, false);
            $id = $product->getId();

            $impressions[] = $data;
            $onClick[$id] = $data;
        }
        return array(
            $impressions,
            $onClick
        );
    }

    public function getImpressionsJson($collection)
    {
        list($impressions, $onClick) = $this->getImpressions($collection);
        return array(
            Zend_Json::encode($impressions),
            $onClick
        );
    }

    public function getWishlistJson()
    {
        $collection = Mage::helper('wishlist')->getWishlistItemCollection();
        $onClick = array();
        $this->resetCounter();
        foreach ($collection as $item) {
            $id = $item->getId();
            $product = $item->getProduct();
            $data = $this->_json($product, false);

            $onClick[$id] = $data;
        }

        return Zend_Json::encode($onClick);
    }

    public function getCartJson()
    {
        $items = Mage::getModel('checkout/cart')->getQuote()->getAllVisibleItems();
        $collection = array();
        $onClick = array();
        $this->resetCounter();
        foreach ($items as $item) {
            $product = $item->getProduct();
            //$type = $item->getProductType();
            // dunque dunque... se mostriamo solo i simple, ci troviamo dei prodotti con dei
            // nomi del menga (e.g. "CINTURA") perchè il nome mostrato sul frontend è quello
            // del configurabile. quindi per ora mostriamo il configurabile e nascondiamo il
            // suo simple. poi si vedrà....
            //if ($type == 'simple') continue;

            $data = $this->_json($product, false);
            if(array_key_exists('position',$data)) {
                unset($data['position']);
            }
            $data['quantity'] = (intval($item->getQty(), 10) > 0) ? $item->getQty() : 1;
            $id = $item->getId();

            $collection[] = $data;
            if($this->useDimension6()) {
                $childProductId = Mage::getModel("catalog/product")->getIdBySku($item->getSku());
                if (!is_null($childProductId)) {
                    $size = $this->_getDimesion6Data($childProductId);
                    $data['dimension6'] = $size;
                    if (Mage::helper('tracker')->addDimension6DataOnCheckOut()) {
                        $akeys = array_keys($collection);
                        $lastKey = end($akeys); // get last key
                        unset($akeys);
                        $collection[$lastKey] = $data;
                    }
                }
            }
            $onClick[$id] = $data;
        }

        return array(
            Zend_Json::encode($collection),
            Zend_Json::encode($onClick)
        );
    }

    public function isSuccessPage()
    {
        $path = Mage::helper('tracker')->getUrlPath();
        return $path == 'checkout_onepage_success';
    }

    public function getSaleRuleActionType(Mage_SalesRule_Model_Rule $salesRule)
    {

        $actionsMap = $this->_actionMapping;

        /* $actionsMap = Mage::getStoreConfig("tracker/google_tag_manager/action_mapping");
          if (empty($actionsMap))
          return array();
          $actionsMap = unserialize($actionsMap);
         */
        $salesRuleAction = $salesRule->getSimpleAction();
        if (!array_key_exists($salesRuleAction, $actionsMap)) {
            Mage::log("SalesRule SimpleAction (".$salesRuleAction.") not mapped", Zend_Log::INFO, "googletagmanager-actionsmap.log");
            return "";
        }

        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $currencyText = strtolower(Mage::app()->getLocale()->currency($currencyCode)->getName());
        return $actionsMap[$salesRuleAction]['type'] == "" ? $currencyText : $actionsMap[$salesRuleAction]['type'];
    }

    public function getOrderData()
    {
        $lastOrderId = Mage::getSingleton('checkout/session')->getLastOrderId();
        $order = Mage::getSingleton('sales/order');
        $order->load($lastOrderId);

        list($head, $details) = $this->_thankyouJson($order);

        // se non erro questi dati devono considerare solo i prodotti. niente
        // spese di spedizioni o addizionali per contrassegno eccetera...
        // dobbiamo inoltre tenere conto di eventuali sconti
        $totalExclTax = 0;

        foreach ($head as $row) {
            $price = $row['price'];
            $price *= $row['quantity'];
            $totalExclTax += $price;
        }

        // magento memorizza le spese di spedizione senza iva, ma lo sconto
        // sulle spese di spedizione è con l'iva. quindi diamo per scontato
        // che avremo o le spese piene, oppure il freeshipping
        if ($order->getShippingDiscountAmount() > 0) {
            $shippingAmount = 0;
        } else {
            $shippingAmount = $order->getShippingAmount();
        }
        $shippingAmount += $order->getCodFee();

        $couponCode = $order->getCouponCode();
        /** @var Mage_SalesRule_Model_Coupon $coupon */
        $coupon = Mage::getModel('salesrule/coupon')->load($couponCode, 'code');
        /** @var Mage_SalesRule_Model_Rule $salesRule */
        $salesRule = Mage::getModel('salesrule/rule')->load($coupon->getRuleId());

        // taxAmount deve includere le tasse dei prodotti e delle
        // spese di spedizione. in altre parole netto prodotti più netto
        // spese di spedizione più tasse uguale totale pagato dal cliente.
        $taxAmount = $order->getGrandTotal() - $totalExclTax - $shippingAmount - $salesRule->getDiscountAmount();

        $salesRuleName = preg_replace('/\s+/', '_', $salesRule->getName());
        $discountAmount = sprintf('%.2f', $salesRule->getDiscountAmount());
        $actionType = $this->getSaleRuleActionType($salesRule);
        $couponDescription = empty($couponCode) ? '' : "{$salesRuleName}-{$discountAmount}{$actionType}";

        // Registration
        $quoteId = $order->getQuoteId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);
        $method = $quote->getCheckoutMethod(true);

        $_confermaIscrizioneVal = array();
        if ($method == Mage_Checkout_Model_Type_Onepage::METHOD_REGISTER) {
            $_confermaIscrizioneVal[] = "acc=1";
        }

        //  Subscrpition: we use cookie from Tbuy_Tracker_Model_Observer::setSubscriberSessionForGTM
        $isSubscribed = Mage::getModel('core/cookie')->get("GTMSubscriberId");
        if ($isSubscribed) {
            $_confermaIscrizioneVal[] = "nl=1";
        }
        //  clean nl=1 cookie
        Mage::getModel('core/cookie')->delete("GTMSubscriberId");

        $confermaIscrizioneVal = "";
        if (!empty($_confermaIscrizioneVal)) {
            $confermaIscrizioneVal = "?" . implode("&", $_confermaIscrizioneVal);
        }

        $paymentMethod = $this->_paymentMethod($order->getPaymentMethod());
        $shippingAmount = sprintf('%.2f', $shippingAmount);
        $totalExclTax = sprintf('%.2f', $totalExclTax);
        $taxAmount = sprintf('%.2f', $taxAmount);

        return array(
            'head'                            => Zend_Json::encode($head),
            'details'                         => Zend_Json::encode($details),
            'tag_manager_order_id'            => $order->getIncrementId(),
            'tag_manager_order_total'         => $totalExclTax,
            'tag_manager_order_shipping'      => $shippingAmount,
            'tag_manager_order_tax'           => $taxAmount,
            'tag_manager_payment_type'        => $paymentMethod,
            'tag_manager_affiliation'         => $couponCode,
            'tag_manager_coupon'              => $couponCode,
            'tag_manager_coupon_description'  => $couponDescription,
            'tag_manager_conferma_iscrizione' => $confermaIscrizioneVal
        );
    }

    public function _thankyouJson($order)
    {
        // distinguiamo i due array in questo modo giusto perchè il primo va
        // a sostituire quello presente in cima a ogni altra pagina del sito
        $items = $order->getAllVisibleItems();
        $details = array();
        $head = array();
        $this->resetCounter();
        foreach ($items as $item) {
            $product = $item->getProduct();

            $childProductId = null;
            if (Mage::helper('tracker')->addDimension6DataOnCheckOut()) {
                $childProductId = Mage::getModel("catalog/product")->getIdBySku($item->getSku());
            }
            $data = $this->_json($product, false, $childProductId);

            // ricalcoliamo il prezzo perchè se c'è uno sconto,
            // ci serve il prezzo scontato pagato dal cliente (senza iva)
            $price = $item->getPriceInclTax() - $item->getDiscountAmount();
            //$price = $this->_price($product, $price);
            $full_price = $this->_price($product, $item->getRegularPrice());
            if($full_price == 0){
	            $metric1 = 0;
            }else{
            $metric1 = round((1 - (floatval($price) / floatval($full_price))) * 100, -1);
            }
            $discountAmount = sprintf('%.2f', $item->getDiscountAmount());
            $data1 = array(
                'name'     => $data['name'],
                'sku'      => $data['id'],
                'category' => $data['category'],
                'price'    => $price,
                'currency' => $data['currency'],
                'quantity' => (int)$item->getQtyOrdered()
            );
            $data2 = array(
                'name'       => $data['name'],
                'id'         => $data['id'],
                'price'      => $price * (int)$item->getQtyOrdered(),
                'currency'   => $data['currency'],
                'brand'      => $data['brand'],
                'dimension6' => isset($data['dimension6']) ? $data['dimension6'] : '',
                'metric1'    => $metric1,
                'category'   => $data['category'],
                'variant'    => $data['variant'],
                'quantity'   => (int)$item->getQtyOrdered(),
                'coupon'     => '',
                'discountAmount' => $discountAmount
            );

            if (isset($data['dimension7'])){
                $data2['dimension7'] = $data['dimension7'];
            }
            $details[] = $data2;
            $head[] = $data1;
        }

        return array($head, $details);
    }

    protected function _category($storeId, $product, $getCurrentCategory)
    {
        $result = null;

        // per le impression dobbiamo ignorare la categoria corrente
        if ($getCurrentCategory === true) {
            $currentCategory = Mage::registry('current_category');
        } else {
            $currentCategory = null;
        }

        if ($currentCategory) {
            $result = $currentCategory;
        } else {
            $categories = $product->getCategoryCollection();

            foreach ($categories as $category) {
                if (!$result) {
                    $result = $category;
                    continue;
                }

                if ($category->getLevel() > $result->getLevel()) {
                    $result = $category;
                    continue;
                }

                if ($category->getLevel() < $result->getLevel()) {
                    continue;
                }

                // level is equal but id is newer (take the old one)
                if ($category->getId() > $result->getId()) {
                    continue;
                }

                $result = $category;
            }
        }

        // non dovrebbe succedere, ma per sicurezza...
        if (!$result) {
            return '';
        }

        // idem come sopra...
        $categoryId = $result->getId();
        if (empty($categoryId) || (int)$categoryId < 1) {
            return '';
        }

        if (!isset(self::$categories[$categoryId])) {
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

            $type = $this->getNameAttributeType();
            $attr = $this->getNameAttributeId();

            $query = "
                SELECT `value`
                FROM catalog_category_entity_varchar
                WHERE entity_id = '{$categoryId}' AND
                    store_id IN (0, {$storeId}) AND
                    entity_type_id = {$type} AND
                    attribute_id = {$attr}
                ORDER BY store_id DESC
                LIMIT 1;";

            $name = $connection->fetchOne($query);
            if (empty($name)) {
                try {
                    $name = Mage::getResourceModel('catalog/category')->getAttributeRawValue(
                        $categoryId, 'name', $this->getDefaultStoreId()
                    );
                } catch (Exception $e) {
                    Mage::logException($e);
                    $name = null;
                }
            }

            self::$categories[$categoryId] = $name;
        }

        return self::$categories[$categoryId];
    }

    protected function _variant($storeId, $product)
    {
        // per rendere le cose più semplici, magento restituisce il colore sempre nella
        // lingua dello store, fregandosene bellamente dell'emulazione di ambiente
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $colorId = $product->getData('color');
        if (empty($colorId)) {
            return $product->getAttributeText('color');
        }

        $colorId = $connection->quote($colorId, Zend_Db::INT_TYPE);
        $storeId = $connection->quote($storeId, Zend_Db::INT_TYPE);

        $query = "
            SELECT `value`
            FROM eav_attribute_option_value
            WHERE store_id  = {$storeId} AND
                  option_id = {$colorId}
            LIMIT 1;";

        $result_object = $connection->query($query);
        $result = $result_object->fetch();

        $color = (is_array($result) && isset($result['value'])) ? $result['value'] : '';
        if (empty($color)) {
            $color = $product->getAttributeText('color');
        }
        if (empty($color)) {
            return '';
        }

        return $color;
    }

    protected function _price($product, $price = null)
    {

        // http://www.magentocommerce.com/boards%20/viewreply/380870/
        $json = $this->getRatesByClass();
        $taxClasses = Mage::helper('core')->jsonDecode($json);
        $taxClassId = $product->getTaxClassId();
        if(is_null($taxClassId)) {
            $taxClassId = Mage::getResourceModel('catalog/product')->getAttributeRawValue($product->getId(), "tax_class_id", Mage::app()->getStore()->getId());
        }
        $div = 1;
        // in teoria non dovrebbe mai succedere
        $key_value = 'value_' . $taxClassId;
        $taxRate = isset($taxClasses[$key_value]) ? $taxClasses[$key_value] : null;
        if (!is_null($taxRate)) {
            $div = 1 + ($taxRate / 100);
        }

        // note to self: se hai problemi con i prezzi delle impressions in upsellcartrelated,
        // controlla che nel template tbuy/upsellcartrelated/default.phtml non ci sia:
        //
        // echo $this->getPriceHtml($related, true)
        //
        // nel caso sostituiscilo con:
        //
        // echo $this->getLayout()->createBlock('catalog/product')
        //     ->setTemplate('catalog/product/price.phtml')
        //     ->setProduct($related)
        //     ->toHtml();

        if (is_null($price)) {
            if ($product->getFinalPrice() <= 0) {
                $product = Mage::helper('utils/product')->loadProduct(
                    $product, array("price","special_price")
                );
                $product->setData('final_price', null);
            }
            $price = $product->getFinalPrice();
        }

        return sprintf('%.2f', $price / $div);
    }

    protected function _paymentMethod($paymentMethod)
    {
        if (preg_match("/GlobalCollect \[(.*)\]/", $paymentMethod, $matches)) {
            $_gcPaymentMethod = $this->_globalcCollectData();
            $paymentMethod = $matches[1];
            if (isset($_gcPaymentMethod[$paymentMethod])) {
                $paymentMethod = $_gcPaymentMethod[$paymentMethod];
            }
        }
        switch ($paymentMethod) {
            case "Cashondelivery":
                $gtmPaymentMethod = self::PAYMENT_METHOD_CASHONDELIVERY;
                break;
            case "Banktransfer":
            case "Bank Transfer":
                $gtmPaymentMethod = self::PAYMENT_METHOD_BANKTRANSFER;
                break;
            case "Real-time Bank Transfer":
                $gtmPaymentMethod = self::PAYMENT_METHOD_REALTIMEBANK;
                break;
            case "Credit Card Online":
                $gtmPaymentMethod = self::PAYMENT_METHOD_CREDITCARD;
                break;
            default:
                $gtmPaymentMethod = $paymentMethod;
                break;
        }
        return $gtmPaymentMethod;
    }

    protected function _globalcCollectData()
    {
        if ($this->_globalCollectData == null) {
            try {
                $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
                $query = "SELECT p.code AS pmethod, m.`code` AS pgroup
                    FROM `globalcollect_payment_product` p
                    JOIN `globalcollect_payment_method` m ON m.`payment_method_id` = p.`payment_method_id`;";
                $result_object = $connection->query($query);
                while ($row = $result_object->fetch()) {
                    $this->_globalCollectData[$row["pmethod"]] = $row["pgroup"];
                }
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
        return $this->_globalCollectData;
    }

    public function getPageCategory()
    {
        // http://www.magentocommerce.com/boards/viewthread/280505/#t388253
        $page = Mage::app()->getFrontController()->getAction()->getFullActionName();

        switch ($page) {
            case 'cms_index_noRoute':
            case 'cms_index_index':
                return Mage::getStoreConfig('tracker/google_tag_manager/page_type/home');

            case 'catalogsearch_result_index':
                return Mage::getStoreConfig('tracker/google_tag_manager/page_type/searchresult');

            case 'catalog_category_layered':
            case 'catalog_category_view':
                return Mage::getStoreConfig('tracker/google_tag_manager/page_type/category');

            case 'catalog_product_view':
                return Mage::getStoreConfig('tracker/google_tag_manager/page_type/product');

            case 'checkout_cart_index':
            case 'onestepcheckout_index_index':
                return Mage::getStoreConfig('tracker/google_tag_manager/page_type/cart');

            case 'checkout_onepage_success':
                return Mage::getStoreConfig('tracker/google_tag_manager/page_type/purchase');

            default:
                return Mage::getStoreConfig('tracker/google_tag_manager/page_type/other');
        }
    }

    protected function getNameAttributeType()
    {
        if (is_null($this->_name_attribute_type)) {
            $type = Mage::getSingleton('eav/entity_type')
                ->loadByCode('catalog_category');
            $this->_name_attribute_type = $type->getEntityTypeId();
        }

        return $this->_name_attribute_type;
    }

    protected function getNameAttributeId()
    {
        if (is_null($this->_name_attribute_id)) {
            $type = $this->getNameAttributeType();

            $attr = Mage::getSingleton('eav/entity_attribute')->getCollection()
                ->addFieldToFilter('attribute_code', 'name')
                ->addFieldToFilter('entity_type_id', $type)
                ->getFirstItem();
            $this->_name_attribute_id = $attr->getId();
        }

        return $this->_name_attribute_id;
    }

    protected function getRatesByClass()
    {
        if (is_null($this->_tax_rates_by_class)) {
            $this->_tax_rates_by_class = Mage::helper('tax')->getAllRatesByProductClass();
        }

        return $this->_tax_rates_by_class;
    }

    public function setCounter($counter) {
        self::$prodPosition = $counter;
    }

    public function resetCounter()
    {
        if ($this->allowResetPosition) {
            self::$prodPosition = 1;
        }
    }

    public function denyResetCounter()
    {
        $this->allowResetPosition = false;
    }

    public function allowResetCounter()
    {
        $this->allowResetPosition = true;
    }

    public function getEncryptionKey()
    {

        $cryptUserEmailEnabled = Mage::getStoreConfigFlag("tracker/google_tag_manager/crypt_user_email", Mage::app()->getStore()->getStoreId());
        if (!$cryptUserEmailEnabled) {
            return;
        }

        $encryptionKey = Mage::getStoreConfig("tracker/google_tag_manager/secure_key", Mage::app()->getStore()->getStoreId());
        $encryptionKey = hash('md5', $encryptionKey, true);

        return $encryptionKey;
    }

    /**
     * Returns an encrypted & base64_encode
     */
    public function encrypt($string)
    {
        $encryptionKey = $this->getEncryptionKey();
        if (!$encryptionKey) {
            return;
        }

        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_CAST_128, MCRYPT_MODE_ECB));
        return base64_encode(mcrypt_encrypt(MCRYPT_CAST_128, $encryptionKey, $string, MCRYPT_MODE_ECB, $iv));
    }

    /**
     * Returns decrypted original string
     */
    public function decrypt($string)
    {
        $encryptionKey = $this->getEncryptionKey();
        if (!$encryptionKey) {
            return;
        }

        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_CAST_128, MCRYPT_MODE_ECB));
        return trim(mcrypt_decrypt(MCRYPT_CAST_128, $encryptionKey, base64_decode($string), MCRYPT_MODE_ECB, $iv));
    }

    public function checkDataOnHead($config) {
        return (bool) in_array($config,explode(",",Mage::getStoreConfig('tracker/google_tag_manager/all_data_on_head')));
    }

    public function addedToCart($block)
    {
        // http://inchoo.net/ecommerce/magento/tracking-magento-add-product-to-cart-action-for-analytic-software-purpose/
        $products = Mage::getModel('core/session')->getProductsToShoppingCart();
        Mage::getModel('core/session')->unsProductsToShoppingCart();

        // se un prodotto è appena stato aggiunto al carrello lanciamo l'evento "addToCart". siccome
        // la pagina in cui atterriamo può essere quella del prodotto o quella del carrello, per non
        // duplicare il codice lo piazziamo qua dentro.

        if (!$products || !is_array($products->getIds()))
            $block->setData('addedToCart', null);
        else {
            $result = array();
            $childProductId = null;

            // dobbiamo risalire al padre
            foreach ($products->getIds() as $id) {
                $parentIds = Mage::getResourceSingleton('catalog/product_type_configurable')
                    ->getParentIdsByChild($id);

                if (count($parentIds) > 0) {
                    $product = Mage::getModel('catalog/product');
                    $product->setId($parentIds[0]);
                    $childId = $id;
                } else {
                    $product = Mage::getModel('catalog/product');
                    $product->setId($id);
                    $childId = null;
                }
                $result[] = $this->getProductJson($product, $childId);
            }
            // session_start();
            // Mage::getModel('core/session')->unsProductsToShoppingCart();
            $block->setData('addedToCart', $result);
        }
    }

    public function getLevelSplit(){
        return Mage::getStoreConfig("tracker/google_tag_manager/level_split");
    }

    public function updateProductJson($key, $jsonArray){
        if (isset($this->_product_json[$key])){
            $this->_product_json[$key] = $jsonArray;
            return true;
        }
        return false;
    }
}
