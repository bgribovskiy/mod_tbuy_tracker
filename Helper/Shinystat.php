<?php

class Tbuy_Tracker_Helper_Shinystat extends Tbuy_Tracker_Helper_Data
{
    const CONFIG_PATH = 'tracker/shinystat/';

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return (bool)Mage::getStoreConfig(self::CONFIG_PATH . 'enabled');
    }

    /**
     * Return script tag for tracker
     *
     * @return string
     */
    public function getJsFilePath()
    {
        $path = '<script type="text/javascript" src="//codiceisp.shinystat.com/cgi-bin/getcod.cgi?USER=%s&NODW=yes" ';
        $path .= 'async="async"></script>';

        return sprintf($path, $this->getUser());
    }

    /**
     * return username from config
     *
     * @return string
     */
    public function getUser()
    {
        return Mage::getStoreConfig(self::CONFIG_PATH . 'user');
    }

    /**
     * return conversion code from config
     *
     * @return string
     */
    public function getConversionCode()
    {
        return Mage::getStoreConfig(self::CONFIG_PATH . 'conversion_code');
    }
}
