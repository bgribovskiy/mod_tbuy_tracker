<?php

class Tbuy_Tracker_Helper_Mainadv extends Mage_Core_Helper_Abstract {

    public function getPageType() {
        // http://www.magentocommerce.com/boards/viewthread/280505/#t388253
        $pageIdentifier = Mage::app()->getFrontController()->getAction()->getFullActionName();

        switch ($pageIdentifier) {
            case 'cms_index_noRoute':
            case 'cms_index_index':
                return 'home';

            case 'catalog_category_layered':
            case 'catalog_category_view':
                return 'category';

            case 'catalog_product_view':
                return 'product';

            case 'checkout_cart_index':
                return 'basket';

            case 'onestepcheckout_index_index':
                return 'checkout';
            case 'checkout_onepage_success':
                return 'thankyou';

            default:
                return null;
        }
    }

    public function getCategoryList($categoryId = null) {
        if ($categoryId != null)
            $category = Mage::getModel('catalog/category')->load($categoryId);
        else
            $category = Mage::registry('current_category');

        if(!is_object($category))
            return '';

        $list        = array();
        $pathInStore = $category->getPathInStore();
        $categories  = $category->getParentCategories();
        $pathIds     = array_reverse(explode(',', $pathInStore));

        // rubato da app/code/core/Mage/Catalog/Helper/Data.php
        // che però crea il breadcrumb solo della categoria corrente
        foreach ($pathIds as $categoryId) {
            if (isset($categories[$categoryId]) && $categories[$categoryId]->getName())
                $list[] = $categories[$categoryId]->getName();
        }

        return join(',', $list);
    }

    public function getProductData($product = null) {
        if ($product == null)
            $product = Mage::registry('current_product');

        return array(
            'pdt_id'  => $product->getId(),
            'pdt_url' => $product->getProductUrl()
        );
    }

    public function getFullProductData($product = null) {
        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();

        if ($product == null)
            $product = Mage::registry('current_product');

        $description = $product->getShortDescription();
        if (empty($description))
            $description = $product->getDescription();
        $description = str_replace(array("\r", "\n", "'"), array(' ', ' ', "\'"), $description);

        $stockQty = Mage::getModel('cataloginventory/stock_item')
            ->loadByProduct($product)
            ->getQty();
        $inStock = $stockQty > 0 ? 1 : 0;

        // specifiche inesistenti: pigliamo la prima e vaffanculo
        $allIds = $product->getCategoryIds();
        $categoryId = @$allIds[0];

        return array(
            'pdt_id'               => $product->getId(),
            'pdt_sku'              => $product->getSku(),
            'pdt_url'              => $product->getProductUrl(),
            'pdt_name'             => $product->getName(),
            'pdt_price'            => $this->getPriceWithoutTax($product),
            'pdt_currency'         => $currencyCode,
            'pdt_photo'            => $product->getImageUrl(),
            'pdt_instock'          => $inStock,
            // Well these are product which will be not sold after some
            // daily, Mainly for daily deal site, keep empty
            'pdt_expdate'          => '',
            'pdt_category_list'    => $this->getCategoryList($categoryId),
            'pdt_smalldescription' => $description
        );
    }

    protected function getPriceWithoutTax($product) {
        $price      = $product->getFinalPrice();
        $taxClassId = $product->getTaxClassId();
        $store      = Mage::app()->getStore();
        $perc       = 0;

        if (!$this->productPriceIncludeTax($store->getId()))
            return $price;

        $taxes = Mage::helper('tax')->getTaxRatesByProductClass();
        if (!is_array($taxes) && $taxes != '')
            $taxes = Mage::helper('core')->jsonDecode($taxes);

        if (isset($taxes['value_' . $taxClassId]))
            $perc = $taxes['value_' . $taxClassId];

        $taxes = Mage::getSingleton('tax/calculation')
            ->calcTaxAmount($price, $perc, true, true);

        return sprintf('%.2f', $price - $taxes);
    }

    public function productPriceIncludeTax($storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {
        return (bool) Mage::getStoreConfig('tax/calculation/price_includes_tax', $storeId);
    }
}
