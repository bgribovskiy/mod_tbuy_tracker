<?php
/**
 * Created by PhpStorm.
 * User: nicola
 * Date: 05/12/16
 * Time: 11.47
 */


class Tbuy_Tracker_Helper_Pixelresponsys extends Tbuy_Tracker_Helper_Data {
    const TRACKER_CODE = 'pixel_responsys';

    public function isEnabled($module = self::TRACKER_CODE) {
        return Mage::getStoreConfig("tracker/{$module}/enabled");
    }
}