<?php

class Tbuy_Tracker_Helper_Acquisio extends Tbuy_Tracker_Helper_Data {

    const TRACKER_CODE = 'acquisio';

    public function isEnabled($module = self::TRACKER_CODE) {
        return Mage::getStoreConfig("tracker/{$module}/enabled");
    }

}
