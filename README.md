To insert advercy pixel for Newsletter TY Page, insert this code in the template

```
<?php if (Mage::getSingleton('core/session')->getTrackerSubscriberId()): ?>
    <img src="http://tb.advercy.com/Sale.ashx?CampaignId=<?php echo Mage::getStoreConfig("tracker/advercy/newsletter/accountid"); ?>&OrderId=<?php echo Mage::getSingleton('core/session')->getTrackerSubscriberId(); ?>&TotalCost=1&ProductId=richiesta&data1=<?php echo Mage::getSingleton('core/session')->getTrackerSubscriberEmail(); ?>">
    <?php 
    Mage::getSingleton('core/session')->setTrackerSubscriberId(null);
    Mage::getSingleton('core/session')->setTrackerSubscriberEmail(null);
endif; 
?>
```

and enable it from System Configuration

For facebookpixel add this code on
/namestore/app/design/frontend/namestore/default/template/onestepcheckout/payment_method.phtml
```
$$('#globalcollect_merchant_link_hosted_payment_product_id').invoke('observe', 'change', function(e) {
    var diUrl = '<?php echo $this->getUrl('tracker/ajax/changepayment', array('_secure' => true)); ?>';
    jQuery.ajax({
        url: diUrl,
        type: 'get',
        dataType: "html"
    });
});
```
where 'globalcollect_merchant_link_hosted_payment_product_id' is element to observe in this case


For ShinyStat tracker

After you enable module need add next javascript after adding the product to the cart:
```
ssxl('PAG=Prodotto_Inserito');
```

and if when clicking the customer did not choose a color/size:

```
ssxl('PAG=Errore_Taglia'); // only if the customer did not choose a size
ssxl('PAG=Errore_Colore'); // only if the customer did not choose a color
```