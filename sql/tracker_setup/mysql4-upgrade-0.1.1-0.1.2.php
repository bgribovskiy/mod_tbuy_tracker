<?php
try {
	/* important local variables */
	$localEntityTypeId = 'catalog_product';
	$localAttributesGroupName = 'General';

	$installer = $this;

	$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
	$installer->startSetup();

// get default attributes set ID
	$localSetId = $setup->getDefaultAttributeSetId($localEntityTypeId);

	/* add attributes group: START  */
// if there is no such attribute group - add new
	if (!is_array($setup->getAttributeGroup($localEntityTypeId, $localSetId, $localAttributesGroupName))) {
		$setup->addAttributeGroup('catalog_products', $localSetId, $localAttributesGroupName);
	}
	$groupData = $setup->getAttributeGroup($localEntityTypeId, $localSetId, $localAttributesGroupName);
	$groupId = $groupData['attribute_group_id'];
	/* add attributes group: END  */

	/* adding attributes: START */
// remove if exists
	if ($setup->getAttribute($localEntityTypeId, 'google_tagmanager_brand')) {
		$setup->removeAttribute($localEntityTypeId, 'google_tagmanager_brand');
	}
// add attribute
	$setup->addAttribute(
		$localEntityTypeId,
		'google_tagmanager_brand', array(
			'label' => 'Google TagManager Brand',
			'type' => 'int',
			'input' => 'select',
			'backend' => '',
			'frontend' => '',
			'source' => 'tracker/system_config_source_categories',
			'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
			'visible' => true,
			'required' => false,
			'user_defined' => true,
			'is_searchable' => false,
			'filterable' => false,
			'comparable' => false,
			'visible_on_front' => true,
			'visible_in_advanced_search' => false,
			'unique' => false,
			'used_in_product_listing' => true
		)
	);

// add attribute to set
	if ($setup->getAttribute($localEntityTypeId, 'google_tagmanager_brand')) {
		$currentAttribute = $setup->getAttribute($localEntityTypeId, 'google_tagmanager_brand');
		$setup->addAttributeToSet($localEntityTypeId, $localSetId, $setup->getDefaultAttributeGroupId($localEntityTypeId, $localSetId), $currentAttribute['attribute_id']);
	}
} catch (Exception $e) {
	Mage::logException($e);
}


$installer->endSetup();