<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();
$connection = $installer->getConnection();

$orderTable = $installer->getTable('sales/order');
if(!$connection->tableColumnExists($orderTable, 'customer_browser')){
	try{
		$connection->addColumn($orderTable, 'customer_browser', "char(255) NULL AFTER mobile_http_user_agent");
	}catch(Exception $e){
		Mage::logException($e);
		throw $e;
	}
}
if(!$connection->tableColumnExists($orderTable, 'customer_os')){
	try{
		$connection->addColumn($orderTable, 'customer_os', "char(255) NULL AFTER mobile_http_user_agent");
	}catch(Exception $e){
		Mage::logException($e);
		throw $e;
	}
}