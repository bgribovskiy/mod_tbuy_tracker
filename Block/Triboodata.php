<?php

class Tbuy_Tracker_Block_Triboodata extends Mage_Core_Block_Template {

    public function getCacheLifetime() {
        return null;
    }


	// Landing page == home page
    public function getHomeCampaignId($store = Mage_Core_Model_Store::ADMIN_CODE) {
        if( Mage::getStoreConfig("tracker/triboodata/home_campaign_id", $store)=="" ) {
			return 0;
		}
        return Mage::getStoreConfig("tracker/triboodata/home_campaign_id", $store);
    }
	public function getHomeCampaignS($store = Mage_Core_Model_Store::ADMIN_CODE) {
		if( Mage::getStoreConfig("tracker/triboodata/home_campaign_s", $store)=="" ) {
			return 0;
		}
		return Mage::getStoreConfig("tracker/triboodata/home_campaign_s", $store);
	}


	// Product view
	public function getProductCampaignId($store = Mage_Core_Model_Store::ADMIN_CODE) {
		if( Mage::getStoreConfig("tracker/triboodata/product_campaign_id", $store)=="" ) {
			return 0;
		}
		return Mage::getStoreConfig("tracker/triboodata/product_campaign_id", $store);
	}
	public function getProductCampaignS($store = Mage_Core_Model_Store::ADMIN_CODE) {
		if( Mage::getStoreConfig("tracker/triboodata/product_campaign_s", $store)=="" ) {
			return 0;
		}
		return Mage::getStoreConfig("tracker/triboodata/product_campaign_s", $store);
	}


	// Cart
	public function getCartCampaignId($store = Mage_Core_Model_Store::ADMIN_CODE) {
		if( Mage::getStoreConfig("tracker/triboodata/cart_campaign_id", $store)=="" ) {
			return 0;
		}
		return Mage::getStoreConfig("tracker/triboodata/cart_campaign_id", $store);
	}
	public function getCartCampaignS($store = Mage_Core_Model_Store::ADMIN_CODE) {
		if( Mage::getStoreConfig("tracker/triboodata/cart_campaign_s", $store)=="" ) {
			return 0;
		}
		return Mage::getStoreConfig("tracker/triboodata/cart_campaign_s", $store);
	}


	// Checkout Success
	public function getCheckoutSuccessId1($store = Mage_Core_Model_Store::ADMIN_CODE) {
		if( Mage::getStoreConfig("tracker/triboodata/checkout_success_id1", $store)=="" ) {
			return 0;
		}
		return Mage::getStoreConfig("tracker/triboodata/checkout_success_id1", $store);
	}
	public function getCheckoutSuccessId2($store = Mage_Core_Model_Store::ADMIN_CODE) {
		if( Mage::getStoreConfig("tracker/triboodata/checkout_success_id2", $store)=="" ) {
			return 0;
		}
		return Mage::getStoreConfig("tracker/triboodata/checkout_success_id2", $store);
	}
	public function getCheckoutSuccessId3($store = Mage_Core_Model_Store::ADMIN_CODE) {
		if( Mage::getStoreConfig("tracker/triboodata/checkout_success_id3", $store)=="" ) {
			return 0;
		}
		return Mage::getStoreConfig("tracker/triboodata/checkout_success_id3", $store);
	}

}