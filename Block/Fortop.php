<?php

class Tbuy_Tracker_Block_Fortop extends Mage_Core_Block_Template {

    protected $_order = null;

    /**
     * @return bool|Mage_Sales_Model_Order|Magestore_Giftwrap_Model_Ordermail|Tbuy_CustomerCare_Model_Order|Tbuy_Flow_Model_Sales_Order
     */
    public function getCurrentOrder() {
        $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
        if (!$orderId)
            return false;
        return Mage::getModel("sales/order")->load($orderId);
    }

    /**
     * @param null $element
     * @return float|int|string
     * @throws Exception
     */
    public function getOrderTotal($element = null)
    {
        $this->_order = $element;
        $totalPrice = 0;
        foreach ($element->getAllItems() as $item) {
            if ($item->getParentItemId()) {
                continue;
            }


            //$taxPercent = 1 + $item->getTaxPercent() / 100;
            $basePrice = $item->getBasePrice() ;// $taxPercent; // - $item->getBaseDiscountAmount();
            $baseDiscountAmount = ($item->getBaseDiscountAmount()) ;// $taxPercent); // - $baseShippingDiscountAmount;

            if($element->getStoreCurrencyCode() !== 'EUR'){
                $basePrice = round(Mage::helper('directory')->currencyConvert($basePrice, Mage::app()->getStore()->getCurrentCurrencyCode(),'EUR'),2);
            }

            if ($basePrice == 0)
                $basePrice = number_format($basePrice, 2);

            $totalPrice += $basePrice * (is_null($item->getQtyOrdered())?$item->getQty():$item->getQtyOrdered()) - Mage::helper("tbflow")->fromBaseToEuroValue($element, $baseDiscountAmount);
        }
        return $totalPrice;
    }
}
