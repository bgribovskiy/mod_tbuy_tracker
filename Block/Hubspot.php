<?php
/**
 * Created by PhpStorm.
 * User: nicola
 * Date: 30/01/17
 * Time: 12.32
 */


class Tbuy_Tracker_Block_Hubspot extends Mage_Core_Block_Template{
    public function getTrackingCode() {
        return Mage::getStoreConfig("tracker/hubspot/trackingcode");
    }
}