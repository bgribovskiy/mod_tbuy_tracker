<?php

class Tbuy_Tracker_Block_Mainadv extends Mage_Core_Block_Template {

    protected function _toHtml() {
        if (!Mage::getStoreConfig('tracker/mainadv/enabled'))
            return '';

        $helper   = Mage::helper('tracker/mainadv');
        $pageType = $helper->getPageType();

        if ($pageType == null)
            return '';

        // FIXME le specifiche sono incomprensibili
        switch ($pageType) {
            case 'category':
                $collection = $this->getCategoryProducts();
                $result     = array(
                    'pdt_url' => array()
                );

                // All multiple product and category can be separated by any of these separator ”, ; \”
                foreach ($collection as $product) {
                    $data = $helper->getProductData($product);
                    $result['pdt_url'][] = $data['pdt_url'];
                }
                foreach ($result as $key => $val)
                    $this->setData($key, join(';', $val));

                $this->setData('pdt_category_list', $helper->getCategoryList());
                break;

            case 'product':
                $data = $helper->getFullProductData();
                foreach ($data as $key => $val)
                    $this->setData($key, $val);
                break;

            case 'basket':
                $quote  = Mage::getSingleton('checkout/session')->getQuote();
                $result = array(
                    'pdt_url' => array(),
                    'pdt_id'  => array()
                );

                foreach ($quote->getAllVisibleItems() as $item) {
                    $data = $helper->getProductData($item->getProduct(), $item);
                    $result['pdt_url'][] = $data['pdt_url'];
                    $result['pdt_id'][]  = $data['pdt_id'];
                }
                foreach ($result as $key => $val)
                    $this->setData($key, join(';', $val));

                $taxes = $quote->getShippingAddress()->getData('tax_amount');
                $total = $quote->getGrandTotal();
                $total = sprintf('%.2f', $total - $taxes);

                $this->setData('order_amount', $total);
                break;

            case 'checkout':
                return '';

            case 'thankyou':
                // dobbiamo metterlo solo nella ty page, ma come codice riportiamo checkout
                $pageType = 'checkout';

                $lastOrderId = Mage::getSingleton('checkout/session')->getLastOrderId();
                $order       = Mage::getSingleton('sales/order');
                $order->load($lastOrderId);

                $result = array(
                    'pdt_url' => array(),
                    'pdt_id'  => array()
                );

                foreach ($order->getAllVisibleItems() as $item) {
                    $data = $helper->getProductData($item->getProduct(), $item);
                    $result['pdt_url'][] = $data['pdt_url'];
                    $result['pdt_id'][]  = $data['pdt_id'];
                }
                foreach ($result as $key => $val)
                    $this->setData($key, join(';', $val));

                $customerName = "{$order->getCustomerFirstname()} {$order->getCustomerLastname()}";
                $customerName = trim($customerName);

                $taxes = $order->getTaxAmount();
                $total = $order->getGrandTotal();
                $total = sprintf('%.2f', $total - $taxes);

                // http://jira.service.triboo.it/browse/TREM-840?focusedCommentId=156637#comment-156637
                // basta settare 1 per ordine completato o 0 per pagamanto pending o ordine con
                // pagamento ritardato.
                $check  = array('Cashondelivery', 'Banktransfer');
                $method = $order->getPaymentMethod();
                if (in_array($method, $check))
                    $this->setData('order_status', '0');
                else
                    $this->setData('order_status', '1');

                $this->setData('order_id', $order->getIncrementId());
                $this->setData('order_amount', $total);
                $this->setData('order_date', $order->getCreatedAt());
                $this->setData('customer_name', $customerName);
                $this->setData('customer_id', $order->getCustomerId());
                $this->setData('customer_type', '');
                break;
        }

        $token = Mage::getStoreConfig('tracker/mainadv/token');
        $key   = Mage::getStoreConfig('tracker/mainadv/key');
        $id    = Mage::getStoreConfig('tracker/mainadv/id');

        $this->setTemplate('tbuy/tracker/mainadv.phtml');
        $this->setData('pageType', $pageType);
        $this->setData('token', $token);
        $this->setData('key', $key);
        $this->setData('id', $id);

        return parent::_toHtml();
    }

    protected function getCategoryProducts() {
        // macello pazzesco
        $dir = Mage::getStoreConfig('tracker/mainadv/sort_direction');

        // ci tocca replicare buona parte del codice del metodo _beforeToHtml
        // del blocco app/code/core/Mage/Catalog/Block/Product/List.php
        $block      = $this->getLayout()->getBlockSingleton('catalog/product_list');
        $toolbar    = $block->getToolbarBlock();
        $collection = $block->getLoadedProductCollection();

        // importante!! se no va tutto a donnine di facili costumi
        $toolbar->setDefaultDirection($dir);

        if ($orders = $block->getAvailableOrders())
            $toolbar->setAvailableOrders($orders);
        if ($sort = $block->getSortBy())
            $toolbar->setDefaultOrder($sort);
        if ($dir = $block->getDefaultDirection())
            $toolbar->setDefaultDirection($dir);
        if ($modes = $block->getModes())
            $toolbar->setModes($modes);

        $toolbar->setCollection($collection);
        $collection->load();

        return $collection;
    }
}
