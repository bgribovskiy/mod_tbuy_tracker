<?php

class Tbuy_Tracker_Block_Publicidees extends Mage_Core_Block_Template {

    protected $_template = '';

    protected function _toHtml() {
        if (!Mage::getStoreConfig('publicidees/settings/enabled'))
            return "";

        //$elementId = Mage::getStoreConfig("publicidees/settings/element_id");
        $cid = Mage::getStoreConfig("publicidees/settings/cid");
        if (empty($cid))
            return "";

        $this->setData('cid', $cid);

        return parent::_toHtml();
    }

    public function getJavascript() {
        if (!Mage::getStoreConfig('publicidees/settings/enabled'))
            return "";
        $layout = Mage::getSingleton('core/layout');
        $block = $layout->createBlock(
            'tracker/publicidees', 'tracker_publicidees_script', array(
                'template' => 'tbuy/tracker/publicidees/script.phtml'
            )
        );
        return $block->toHtml();
    }

}
