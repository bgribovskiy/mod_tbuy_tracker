<?php

class Tbuy_Tracker_Block_Adminhtml_Form_Field_Category extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract {

    protected $_categoryRenderer;
    protected $_useFilter;

    public function _getCategoryRenderer() {
        if (!$this->_categoryRenderer) {
            $this->_categoryRenderer = $this->getLayout()->createBlock(
                    'tracker/adminhtml_form_options_category', '', array('is_render_to_js_template' => true)
            );
            $this->_categoryRenderer->setExtraParams('style="width:auto;"');
        }
        return $this->_categoryRenderer;
    }

    /**
     * Prepare to render
     */
    protected function _prepareToRender() {
        $this->setHtmlId('tracker_configuration_general_category');
        $this->addColumn('category', array(
            'label' => Mage::helper('tracker')->__('Category'),
            'renderer' => $this->_getCategoryRenderer()
        ));

        $this->addColumn('code', array(
            'label' => Mage::helper('tracker')->__('Code')
        ));

        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('tracker')->__('Add Category Code');
    }

    /**
     * Prepare existing row data object
     *
     * @param Varien_Object
     */
    protected function _prepareArrayRow(Varien_Object $row) {
        $row->setData(
                'option_extra_attr_' . $this->_getCategoryRenderer()->calcOptionHash($row->getData('category')), 'selected="selected"'
        );
    }

}
