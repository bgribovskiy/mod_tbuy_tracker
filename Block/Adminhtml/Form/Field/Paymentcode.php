<?php

class Tbuy_Tracker_Block_Adminhtml_Form_Field_Paymentcode extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract {

    protected $_codeRenderer;
    protected $_labelRenderer;
    protected $_addButton = false;
    protected $_deleteButton = false;

    public function __construct() {
        parent::__construct();
        $this->setTemplate('system/config/form/field/array_enanched.phtml');
    }

    protected function _getCodeRenderer() {
        if (!$this->_codeRenderer) {
            $this->_codeRenderer = $this->getLayout()->createBlock(
                    'tracker/adminhtml_form_field_paymentcodes', '', array('is_render_to_js_template' => true)
            );
            $this->_codeRenderer->setClass('customer_group_select');
            $this->_codeRenderer->setExtraParams('style="width:200px"');
        }
        return $this->_codeRenderer;
    }

    protected function _getLabelRenderer() {
        if (!$this->_labelRenderer) {
            $this->_labelRenderer = $this->getLayout()->createBlock(
                    'tracker/adminhtml_form_field_label', '', array()
            );
        }
        $this->_labelRenderer->setExtraParams('style="width:250px"');
        return $this->_labelRenderer;
    }

    /**
     * Prepare to render
     */
    protected function _prepareToRender() {
        $this->addColumn('method', array(
            'label' => Mage::helper('tracker')->__('Method'),
            'renderer' => $this->_getLabelRenderer()
        ));
        $this->addColumn('code', array(
            'label' => Mage::helper('tracker')->__('Code'),
            'renderer' => $this->_getCodeRenderer()
        ));
        $this->_addAfter = false;
        //$this->_addButtonLabel = Mage::helper('tracker')->__('Add Sezionale');
    }

    /**
     * Prepare existing row data object
     *
     * @param Varien_Object
     */
    protected function _prepareArrayRow(Varien_Object $row) {
        $row->setData(
                'option_extra_attr_' . $this->_getCodeRenderer()->calcOptionHash($row->getData('code')), 'selected="selected"'
        );
    }

}
