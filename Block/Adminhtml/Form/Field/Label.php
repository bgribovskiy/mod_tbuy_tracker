<?php

class Tbuy_Tracker_Block_Adminhtml_Form_Field_Label extends Mage_Adminhtml_Block_Abstract implements Varien_Data_Form_Element_Renderer_Interface {

    protected $name = "";

    /**
     * Render element html
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element) {
        return sprintf('<span id="%s">%s</span>', $element->getHtmlId(), $element->getLabel());
    }

    public function setColumn() {
        return $this;
    }

    public function setInputName() {
        return $this;
    }

    public function setColumnName($name) {
        $this->name = $name;
        return $this;
    }

    public function _toHtml() {
        return '#{' . $this->name . '}';
    }

}

?>