<?php

class Tbuy_Tracker_Block_Adminhtml_Form_Field_Section extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract {

    protected $_categoryRenderer;
    protected $_useFilter;

    public function _getSectionRenderer() {
        if (!$this->_categoryRenderer) {
            $this->_categoryRenderer = $this->getLayout()->createBlock(
                    'tracker/adminhtml_form_options_section', '', array('is_render_to_js_template' => true)
            );
            $this->_categoryRenderer->setExtraParams('style="width:auto;"');
        }
        return $this->_categoryRenderer;
    }

    /**
     * Prepare to render
     */
    protected function _prepareToRender() {
        $this->setHtmlId('tracker_configuration_general_category');
        $this->addColumn('section', array(
            'label' => Mage::helper('tracker')->__('Section'),
            'renderer' => $this->_getSectionRenderer()
        ));

        $this->addColumn('code', array(
            'label' => Mage::helper('tracker')->__('Code')
        ));

        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('tracker')->__('Add Section Code');
    }

    /**
     * Prepare existing row data object
     *
     * @param Varien_Object
     */
    protected function _prepareArrayRow(Varien_Object $row) {
        $row->setData(
                'option_extra_attr_' . $this->_getSectionRenderer()->calcOptionHash($row->getData('section')), 'selected="selected"'
        );
    }

}
