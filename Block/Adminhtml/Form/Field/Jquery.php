<?php

class Tbuy_Tracker_Block_Adminhtml_Form_Field_Jquery extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract {

    protected function _prepareToRender() {
        $this->addColumn('selector', array(
            'label' => Mage::helper('tracker')->__('jQuery selector'),
            'style' => 'width: 200px'
        ));
        $this->addColumn(
            'attribute', array('label' => Mage::helper('tracker')->__('Attribute'),
            'style' => 'width: 50px'
        ));
        $this->addColumn(
            'regexp', array('label' => Mage::helper('tracker')->__('Regular Expr'),
            'style' => 'width: 150px'
        ));

        $this->_addButtonLabel = Mage::helper('tracker')->__('Add');
        $this->_addAfter = false;
    }
}
