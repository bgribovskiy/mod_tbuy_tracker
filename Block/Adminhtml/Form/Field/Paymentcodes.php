<?php

class Tbuy_Tracker_Block_Adminhtml_Form_Field_Paymentcodes extends Mage_Core_Block_Html_Select {
    /*
     * Array for the payment methods/fatturone's codes conversion.
     * Default values: CC[credit card];PP[paypal];CON[cash on delivery];BON[bank transaction]
     */

    private $_codes = array(
        '' => '--',
        'CC' => 'Credit Card (CC)',
        'BON' => 'Bank Transfer (BON)',
        'PAY' => 'PayPal (PAY)',
        'CON' => 'Cash on delivery (CON)',
        'CMO' => 'Check Money Order(CMO)',
        'FREE' => 'No Payment Information (FREE)',
        'PUR' => 'Purchase Order  (PUR)'
    );

    protected function _getCodes($code = null) {
        if (!is_null($code)) {
            return isset($this->_codes[$code]) ? $this->_codes[$code] : null;
        }
        return $this->_codes;
    }

    public function setInputName($value) {
        return $this->setName($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml() {
        if (!$this->getOptions()) {
            foreach ($this->_getCodes() as $code => $codeLabel) {
                $this->addOption($code, $codeLabel);
            }
        }
        return parent::_toHtml();
    }

}
