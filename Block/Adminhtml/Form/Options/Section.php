<?php

class Tbuy_Tracker_Block_Adminhtml_Form_Options_Section extends Mage_Core_Block_Html_Select {

	public function _toHtml() {
		$this->addOption('', Mage::helper('tracker')->__('-- Select a Section --'));

		$sections = array(
			"Home" => "cms_index_index",
			"Category" => "catalog_category_view",
			"Search Result" => "catalogsearch_result_index",
			"Product" => "catalog_product_view",
			"Cart" => "checkout_cart_index",
			"Success Page" => "checkout_onepage_success"
		);
		foreach ($sections as $label => $value)
			$this->addOption($value, $label);
		return parent::_toHtml();
	}

	public function setInputName($value) {
		return $this->setName($value);
	}

}

//End of class