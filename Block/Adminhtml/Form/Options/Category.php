<?php

class Tbuy_Tracker_Block_Adminhtml_Form_Options_Category extends Mage_Core_Block_Html_Select {

    public function _toHtml() {
        $this->addOption('', Mage::helper('tracker')->__('-- Select a Category --'));
        $collection = Mage::helper("tracker")->load_tree();
        foreach ($collection as $attr) {
            $style = $attr['is_active'] ? "color:#000;" : "color:grey;font-style:italic;";
            $params = array("style" => $style);
            $this->addOption($attr['value'], str_replace("'","\'",$attr['label']), $params);
        }
        return parent::_toHtml();
    }

    public function setInputName($value) {
        return $this->setName($value);
    }

}

//End of class