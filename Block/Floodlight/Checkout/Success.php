<?php

class Tbuy_Tracker_Block_Floodlight_Checkout_Success extends Tbuy_Tracker_Block_Floodlight_Tag
{

    protected $_cat = null;
    protected $_type = null;

    public function _construct()
    {
        $order = Mage::getSingleton("sales/order")->load(Mage::getSingleton("checkout/session")->getLastOrderId());
        if (is_object($order)) {
            $this->setData($order->getData());
            $this->setObjectItems($order->getAllVisibleItems());
        }
    }

    public function setClickTagVars()
    {
        $productsCounter = 0;
        $products = array();
        foreach($this->getObjectItems() as $_item) {
            $products[] = preg_replace('/[^[:print:]]/', '', $_item->getProduct()->getName());
            $productsCounter += $_item->getQtyOrdered();
        }
        $data = array(
            'qty' => (int)$productsCounter,
            //'qty' => count($products),
            'cost' => $this->getGrandTotal(),
            'u1' => Mage::app()->getWebsite()->getCode(),
            'u2' => implode(',', $products),
            'u3' => Mage::app()->getStore()->getCurrentCurrencyCode(),
            'ord' => $this->getIncrementId()
        );
        $this->setData('click_tag_vars',$data);
    }

}
