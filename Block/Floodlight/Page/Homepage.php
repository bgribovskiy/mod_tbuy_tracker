<?php

class Tbuy_Tracker_Block_Floodlight_Page_Homepage extends Tbuy_Tracker_Block_Floodlight_Tag
{

    protected $_cat = null;
    protected $_type = null;

    public function _construct() {
    }

    public function setClickTagVars()
    {
        $data = array(
            'u1' => Mage::app()->getWebsite()->getCode(),
        );

        $this->setData('click_tag_vars',$data);
    }
}
