<?php

class Tbuy_Tracker_Block_Floodlight_OneStepCheckout_Checkout extends Tbuy_Tracker_Block_Floodlight_Tag
{

    protected $_cat = null;
    protected $_type = null;

    public function _construct() {
        $quote = Mage::getModel('checkout/cart')->getQuote();
        if (is_object($quote)) {
            $this->setData($quote->getData());
            $this->setObjectItems($quote->getAllVisibleItems());
        }
    }

    public function setClickTagVars()
    {
        $products = array();

        foreach($this->getObjectItems() as $_item) {
            $products[] = preg_replace('/[^[:print:]]/', '', $_item->getProduct()->getName());
        }

        $data = array(
            'u1' => Mage::app()->getWebsite()->getCode(),
            'u2' => implode(',', $products),
            'u3' => Mage::app()->getStore()->getCurrentCurrencyCode()
        );

        $this->setData('click_tag_vars',$data);
    }
}
