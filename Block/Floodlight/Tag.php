<?php

class Tbuy_Tracker_Block_Floodlight_Tag extends Mage_Core_Block_Template
{

    const PRODUCTION_URL = 'fls.doubleclick.net';

    protected $_cat = null;
    protected $_type = null;

    public function _construct()
    {
        $this->setTemplate('tbuy/tracker/floodlight/tag.phtml');
    }

    /**
     * The url (is there a testing site??)
     *
     * @return string
     */
    public function getProdUrl()
    {
        return self::PRODUCTION_URL;
    }

    /**
     * Get The account Id from Config
     * @return string
     */
    public function getEnabled()
    {
        return Mage::getStoreConfig('floodlight_section/options/enabled_' . $this->getEnabledCheck());
    }

    /**
     * Get The account Id from Config
     * @return string
     */
    public function getMerchantCode()
    {
        return $this->escapeHtml(Mage::getStoreConfig('floodlight_section/options/merchant'));
    }

    public function getType()
    {
        if (is_null($this->_type)) {
            return $this->getData('type');
        }
        return $this->_type;
    }

    public function getCat()
    {
        if (is_null($this->_cat)) {
            return $this->getData('cat');
        }
        return $this->_cat;
    }

    public function getUrlParams()
    {
        $data = $this->getClickTagVars();

        if(is_null($data)) {
            $data = array();
        }
        $params = array();
        if (is_array($data)) {
            if (!array_key_exists('ord', $data)) {
                $data['ord'] = rand(0, 10000000000000);
            }
            $params = http_build_query(
                array_merge(array(
                    'src' => $this->getMerchantCode(),
                    'type' => $this->getType(),
                    'cat' => $this->getCat(),
                    'dc_lat' => '',
                    'dc_rdid' => '',
                    'tag_for_child_directed_treatment' => ''), $data)
            , '', ';');
        }
        return $params . '?';
    }

}
