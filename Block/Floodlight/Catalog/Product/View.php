<?php

class Tbuy_Tracker_Block_Floodlight_Catalog_Product_View extends Tbuy_Tracker_Block_Floodlight_Tag
{

    protected $_cat = null;
    protected $_type = null;

    public function _construct()
    {
        $product = Mage::registry('current_product');
        if (is_object($product)) {
            $this->setData($product->getData());
            $this->setObjectItems($product->getAllVisibleItems());
        }
    }

    public function setClickTagVars()
    {
        $data = array(
            'u1' => Mage::app()->getWebsite()->getCode(),
            'u2' => $this->getName(),
        );

        $this->setData('click_tag_vars',$data);
    }
}
