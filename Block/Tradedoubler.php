<?php

class Tbuy_Tracker_Block_Tradedoubler extends Mage_Core_Block_Template {

    protected $_canTrackOrder = false;
    protected $organization;
    protected $event;
    protected $leadNumber;
    protected $orderId;
    protected $baseGrandTotal;
    protected $incrementId;

    public function _construct() {
        parent::_construct();

        if (!Mage::getStoreConfig("tradedoubler/settings/enabled"))
            return;

        $this->_canTrackOrder = true;
        $this->getTracker();
    }

    public function canTrackOrder() {
        return $this->_canTrackOrder;
    }

    public function getTracker() {
        $this->organization = Mage::getStoreConfig("tradedoubler/settings/organization");
        $this->event = Mage::getStoreConfig("tradedoubler/settings/event");
        $this->leadNumber = Mage::getStoreConfig("tradedoubler/settings/leadNumber");
        $this->orderId = $this->getOrderId();
        $order = Mage::getModel('sales/order')->load($this->orderId);
        $this->incrementId = $order->getIncrementId();
        $this->baseGrandTotal = $order->getGrandTotal();
    }

}
