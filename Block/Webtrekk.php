<?php

class Tbuy_Tracker_Block_Webtrekk extends Mage_Core_Block_Template {

	const PRODUCT_STATUS_VIEW = "view";
	const PRODUCT_STATUS_ADD = "add";
	const PRODUCT_STATUS_CONF = "conf";

	protected $_template = 'tbuy/tracker/webtrekk.phtml';
	protected $_products = array();
	protected $_categories = array();
	protected $_brands = array();
	protected $_qty = array();
	protected $_price = array();
	protected $_status = null;
	protected $_order = null;
	protected $_attribues = array();
	protected $_order_total = array();
	protected $_trackingProduct = null;
	protected static $_prodCounter = array();

	public function _construct() {
		parent::_construct();
		$this->helper = Mage::helper("tracker/webtrekk");
	}

	public function getUrlPath() {
		$path = Mage::helper('tracker')->getUrlPath();
		return $path;
	}

	public function isHome() {
		return $this->getUrlPath() == 'cms_index_index';
	}

	public function isProductPage() {
		return $this->getUrlPath() == 'catalog_product_view';
	}

	public function isCategoryPage() {
		return $this->getUrlPath() == 'catalog_category_view';
	}

	public function isCartPage() {
		return $this->getUrlPath() == 'checkout_cart_index';
	}

	public function isSuccessPage() {
		return $this->getUrlPath() == 'checkout_onepage_success';
	}

	public function showProductParams() {
		if (!$this->isProductPage() && !$this->isCartPage())
			return false;
		return true;
	}

	public function showCategoryParam() {
		return $this->getProductStatus() == self::PRODUCT_STATUS_VIEW;
	}

	public function getProductStatus() {
		$this->helper->log("[TRACKER|WEBTREKK] getProductStatus => {$this->_status}");
		return $this->_status;
	}

	public function getTrackId() {
		return Mage::getStoreConfig("webtrekk/settings/track_id");
	}

	public function getTrackDomain() {
		return Mage::getStoreConfig("webtrekk/settings/track_domain");
	}

	public function getDomain() {
		return Mage::getStoreConfig("webtrekk/settings/domain");
	}

	public function getCookie() {
		return Mage::getStoreConfig("webtrekk/settings/cookie");
	}

	public function getMediaCode() {
		return Mage::getStoreConfig("webtrekk/settings/media_code");
	}

	public function getLinkTrack() {
		if ($this->isHome())
			return Mage::getStoreConfig("webtrekk/settings/link_track");
		return "";
	}

	public function getHeatmap() {
		if ($this->isHome())
			return Mage::getStoreConfig("webtrekk/settings/heatmap");
		return "";
	}

	public function getContentGroup() {
		$cg = array();
		$countryCode = Mage::getStoreConfig('general/country/default');
		$country = Mage::getModel('directory/country')->loadByCode($countryCode);
		$cg[1] = Mage::getStoreConfig("webtrekk/settings/content_group_key1");
		$cg[2] = Mage::getStoreConfig("webtrekk/settings/content_group_key2");
		$cg[3] = "{$country->getName()}";
		if (Mage::registry("current_category")) {
			$currentStore = Mage::app()->getStore()->getId();
			Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
			$category = Mage::getModel('catalog/category')->load(Mage::registry("current_category")->getId());
			$coll = $category->getResourceCollection();
			$pathIds = $category->getPathIds();
			$coll->addAttributeToSelect('name');
			$coll->addAttributeToFilter('entity_id', array('in' => $pathIds));
			$coll->addAttributeToFilter('level', array('gt' => 1));
			foreach ($coll as $cat)
				$cg[] = $cat->getName();
			Mage::app()->setCurrentStore($currentStore);
		}
		$this->helper->log("[TRACKER|WEBTREKK] getContentGroup => " . print_r($cg, true));
		$json = $this->helper->myJsonEncode($cg);
		return $json;
	}

	public function setTrackingProduct($product) {
		$this->_trackingProduct = $product;
		return $this;
	}

	public function setAddQty($qty) {
		$this->_qty[] = $qty;
		return $this;
	}

	public function getTrackingProduct() {
		if (Mage::registry("current_product"))
			return Mage::registry("current_product");
		return $this->_trackingProduct;
	}

	public function getProductAttributes($product) {
		$attributes = explode(",", Mage::getStoreConfig("webtrekk/settings/attributes"));
		$i = 0;

		foreach ($attributes as $attr) {
			$_attr = array();
			$value = $product->getAttributeText($attr);
			$this->helper->log("[TRACKER|WEBTREKK] \t \t {$attr} => {$value}");
			if (empty($value))
				$value = "Non disponibile";
			$_attr[] = $value;
			if (count($_attr) > 0) {
				$i++;
				$this->_attribues[$i] = implode(";", $_attr);
			}
		}
		return $this;
	}

	public function loadProduct() {
		$product = Mage::getModel("catalog/product")
				->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
				->load($this->getTrackingProduct()->getId());
		$this->_products[] = str_replace("'", "\'", $product->getName());
		$this->_categories[] = Mage::helper("tracker/webtrekk")->getProductCategory($product);
		$this->_brands[] = $product->getData(Mage::getStoreConfig("webtrekk/settings/brand_attribute")) != "" ? $product->getData(Mage::getStoreConfig("webtrekk/settings/brand_attribute")) : Mage::getStoreConfig("webtrekk/settings/custom_brand_attribute");
		$this->_status = self::PRODUCT_STATUS_VIEW;
		$productPrice = Mage::getModel("catalog/product")
				->setStoreId($this->helper->getBaseStoreId())
				->load($this->getTrackingProduct()->getId());
		$this->_price[] = number_format($productPrice->getFinalPrice(), 2, ".", "");
		return $product;
	}

	public function initProducts() {
		//$currency = Mage::app()->getStore()->getCurrentCurrencyCode();
		//$baseCurrency = $this->helper->getBaseCurrency();
		if ($this->isProductPage()) {
			$this->helper->log("[TRACKER|WEBTREKK] initProducts | ProductPage");
			$this->loadProduct();

			$this->helper->log("[TRACKER|WEBTREKK] initProducts ---------------------------------------");
			$this->helper->log("[TRACKER|WEBTREKK] \t Products: " . print_r($this->_products, true));
			$this->helper->log("[TRACKER|WEBTREKK] \t Price: " . print_r($this->_price, true));
			$this->helper->log("[TRACKER|WEBTREKK] \t Status: " . print_r($this->_status, true));
			$this->helper->log("[TRACKER|WEBTREKK] \t Categories: " . print_r($this->_categories, true));
			$this->helper->log("[TRACKER|WEBTREKK] \t Brands: " . print_r($this->_brands, true));
			$this->helper->log("[TRACKER|WEBTREKK] initProducts END -----------------------------------");
			return true;
		}

		/* if ($this->isCartPage()) {
		  $this->helper->log("[TRACKER|WEBTREKK] initProducts | CartPage");
		  $quote = Mage::getSingleton('checkout/session')->getQuote();
		  $cartItems = $quote->getAllVisibleItems();
		  foreach ($cartItems as $item) {
		  $product = Mage::getModel("catalog/product")
		  ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
		  ->load($item->getProductId());
		  $this->_products[] = str_replace("'", "\'", $product->getName());
		  $this->_qty[] = $item->getQty();
		  $price = Mage::helper('directory')->currencyConvert($item->getPriceInclTax(), $currency, $baseCurrency);
		  $this->_price[] = number_format($price, 2, ".", "");
		  $this->_categories[] = Mage::helper("tracker/webtrekk")->getProductCategory($product);
		  $this->_brands[] = $product->getData(Mage::getStoreConfig("webtrekk/settings/brand_attribute")) != "" ? $product->getData(Mage::getStoreConfig("webtrekk/settings/brand_attribute")) : Mage::getStoreConfig("webtrekk/settings/custom_brand_attribute");
		  }
		  $this->_status = null;
		  $this->helper->log("[TRACKER|WEBTREKK] initCart ---------------------------------------");
		  $this->helper->log("[TRACKER|WEBTREKK] \t Products: " . print_r($this->_products, true));
		  $this->helper->log("[TRACKER|WEBTREKK] \t Status: " . print_r($this->_status, true));
		  $this->helper->log("[TRACKER|WEBTREKK] \t Qty: " . print_r($this->_qty, true));
		  $this->helper->log("[TRACKER|WEBTREKK] \t Price: " . print_r($this->_price, true));
		  $this->helper->log("[TRACKER|WEBTREKK] \t Categories: " . print_r($this->_categories, true));
		  $this->helper->log("[TRACKER|WEBTREKK] \t Brands: " . print_r($this->_brands, true));
		  $this->helper->log("[TRACKER|WEBTREKK] initCart END -----------------------------------");
		  return true;
		  }
		 * 
		 */
		return true;
	}

	public function initOrder() {
		$orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
		if (!$orderId)
			return false;
		$this->_order = Mage::getModel("sales/order")->load($orderId);
		$this->_incrementId = $this->_order->getIncrementId();
		$this->_status = self::PRODUCT_STATUS_CONF;
		if (!$this->_order)
			return false;
		$currency = Mage::app()->getStore()->getCurrentCurrencyCode();
		$baseCurrency = $this->helper->getBaseCurrency();
		$this->_order_total = Mage::helper('directory')->currencyConvert($this->_order->getGrandTotal(), $currency, $baseCurrency);
		$attributes = explode(",", Mage::getStoreConfig("webtrekk/settings/attributes"));
		$allItems = $this->_order->getAllVisibleItems();
		$i = 0;
		foreach ($attributes as $attr) {
			$_attr = array();
			foreach ($allItems as $item) {
				//if ($item->getParentItemId() != null)
				//	continue;
				$product = Mage::getModel("catalog/product")
						->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
						->load($item->getProductId());
				if (!in_array($item->getProductId(), self::$_prodCounter)) {
					$this->_products[] = str_replace("'", "\'", $product->getName());
					$price = Mage::helper('directory')->currencyConvert($item->getPriceInclTax(), $currency, $baseCurrency);
					$this->_price[] = number_format($item->getQtyOrdered() * $price, 2, ".", "");
					$this->_qty[] = number_format($item->getQtyOrdered(), 0, ".", "");
					self::$_prodCounter[] = $item->getProductId();
				}
				$value = $product->getAttributeText($attr);
				if (empty($value))
					$value = "Non disponibile";
				$_attr[] = $value;
			}
			//$this->helper->log("[TRACKER|WEBTREKK] \t Attributes: " . print_r($_attr, true));
			if (count($_attr) > 0) {
				$i++;
				$this->_attribues[$i] = implode(";", $_attr);
			}
		}
		$this->helper->log("[TRACKER|WEBTREKK] initOrder ---------------------------------------");
		$this->helper->log("[TRACKER|WEBTREKK] \t IncrementId: " . print_r($this->_incrementId, true));
		$this->helper->log("[TRACKER|WEBTREKK] \t Products: " . print_r($this->_products, true));
		$this->helper->log("[TRACKER|WEBTREKK] \t Status: " . print_r($this->_status, true));
		$this->helper->log("[TRACKER|WEBTREKK] \t Qty: " . print_r($this->_qty, true));
		$this->helper->log("[TRACKER|WEBTREKK] \t Price: " . print_r($this->_price, true));
		$this->helper->log("[TRACKER|WEBTREKK] \t All Attributes: " . print_r($this->_attribues, true));
		$this->helper->log("[TRACKER|WEBTREKK] initOrder END -----------------------------------");
		$i++;
		$this->_attribues[$i] = $this->getPaymentMethod();
	}

	public function getPaymentMethod() {
		if (!$this->_order)
			return;
		$paymentModel = $this->_order->getPayment();
		$payment = $paymentModel->getMethod();
		if ($payment == Tbuy_Tracker_Helper_Webtrekk::GLOBAL_COLLECT_PAYMENT_METHOD) {
			$additionalInfo = $paymentModel->getAdditionalInformation();
			$productId = $additionalInfo['payment_product_id'];
			$paymentProduct = Mage::getModel("globalcollect/payment_product")->load($productId);
			if (!$paymentProduct)
				return "Non Disponibile";
			return $paymentProduct->getCode();
		} else {
			return $payment;
		}
	}

	public function getOrderIncrementId() {
		return $this->_incrementId;
	}

	public function getOrderTotal() {
		return $this->_order_total;
	}

	public function getCustomEcommerceParameter() {
		$json = $this->helper->myJsonEncode($this->_attribues);
		return $json;
	}

	public function getProductsName() {
		return implode(";", $this->_products);
	}

	public function getProductsCategory() {
		$categoryBrand = array();
		$categoryBrand[1] = implode(";", $this->_brands);
		$categoryBrand[2] = implode(";", $this->_categories);
		$json = $this->helper->myJsonEncode($categoryBrand);
		return $json;
	}

	public function getProductsQty() {
		return implode(";", $this->_qty);
	}

	public function getProductsCost() {
		return implode(";", $this->_price);
	}

	protected function _toHtml() {
		if (!Mage::getStoreConfig("webtrekk/settings/enabled"))
			return '';
		return parent::_toHtml();
	}

}
