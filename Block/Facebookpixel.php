<?php

class Tbuy_Tracker_Block_Facebookpixel extends Mage_Core_Block_Template {

    protected $_globalPageType;

    public function _construct(){
        parent::_construct();
        $this->_globalPageType = null;
    }
    /**
     * @return int|mixed
     */
    public function getCampaignId() {
        if (Mage::getStoreConfig("tracker/facebookpixel/campaignid") == "")
            return 0;
        return Mage::getStoreConfig("tracker/facebookpixel/campaignid");
    }

    /**
     * @return null|string
     */
    public function getScriptType(){
        $requestedRouteName = Mage::app()->getFrontController()->getRequest()->getRequestedRouteName();
        $cmsPageUrlKey = Mage::getSingleton('cms/page')->getIdentifier();
        $controllerName = Mage::app()->getFrontController()->getRequest()->getControllerName();
        $pageName = Mage::helper('tracker/facebookpixel')->viewContent();
        $this->_globalPageType = $pageName;
        $scriptType = null;
        $urlPath = Mage::helper('tracker')->getUrlPath();
        if($urlPath == "customer_account_index"){
            $messagesCollection = Mage::getSingleton('customer/session')->getMessages()->toString();
            if(preg_match('/success/i',$messagesCollection)){
                $controllerName = "registration";
            }
        }
        if($urlPath == "cms_index_index"){
            $messagesCollection = Mage::getSingleton('core/session')->getCustomEnv();
            if($messagesCollection == "newsletter_confirmation"){
                $controllerName = "subscriber";
            }
        }
        switch ($controllerName) {
            case "index":
                if ($requestedRouteName == "onestepcheckout")
                    break;
                if(Mage::helper("tracker/facebookpixel")->isEnabledForViewContent() && $cmsPageUrlKey=="home") {
                    $scriptType = "fbq('track', '{$pageName}');";
                }
                break;

            case "page":
                if(!Mage::helper("tracker/facebookpixel")->isEnabledForViewContent())
                    break;
                $scriptType = "fbq('track', '{$pageName}');";
                break;

            case "category":
                if(!Mage::helper("tracker/facebookpixel")->isEnabledForViewContent())
                    break;
                $scriptType = "fbq('track', '{$pageName}');";
                break;

            case "product":
                if (!Mage::helper("tracker/facebookpixel")->isEnabledForViewContent())
                    break;
                if (!Mage::helper('tracker/facebookpixel')->isEnabledDpa()) {
                    $scriptType = "fbq('track', '{$pageName}');";
                } else {
                    $productSku = Mage::registry('current_product')->getId().'_'.Mage::app()->getStore()->getStoreId();
                    $scriptType = sprintf("fbq('track', '%s', {content_type : 'product', content_ids : ['%s']})",
                        $pageName,
                        $productSku
                    );
                }
                break;

            case "subscriber":
                if(!Mage::helper("tracker/facebookpixel")->isEnabledForLead())
                    break;
                $pageName = Mage::helper('tracker/facebookpixel')->lead();
                $this->_globalPageType = $pageName;
                $scriptType = "fbq('track', '{$pageName}');";
                break;
            case "registration":
                if(!Mage::helper("tracker/facebookpixel")->isEnabledForCompleteRegistration())
                    break;
                $pageName = Mage::helper('tracker/facebookpixel')->completeRegistration();
                $this->_globalPageType = $pageName;
                $scriptType = "fbq('track', '{$pageName}');";
                break;
        }

        return $scriptType;
    }

    public function getGlobalPageType(){
        return $this->_globalPageType;
    }
}
