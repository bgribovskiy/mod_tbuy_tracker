<?php

class Tbuy_Tracker_Block_Yahoo extends Mage_Core_Block_Template {

	public function getCacheLifetime() {
		return null;
	}

	public function getTagId() {
		return Mage::helper("tracker/yahoo")->getTag();
	}

	public function getProductId() {
		if (Mage::getStoreConfig('tracker/yahoo/use_skus')) {
			return Mage::registry('current_product')->getSku();
		}
		return Mage::registry('current_product')->getId();
	}
	
	public function getCategoryId() {
		return Mage::registry('current_category')->getId();
	}

	public function getOrder() {
		$orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
		$order = Mage::getModel('sales/order')->load($orderId);
		return $order;
	}


}
