<?php

class Tbuy_Tracker_Block_Googleads extends Mage_Core_Block_Template {

	protected $_pageType = '';
	
	public function setPageType($pageType){
		$this->_pageType = $pageType;
	}
	
	public function getPageType(){
		return $this->_pageType;
	}
	
	public function getTagParams(){
		$h = Mage::helper('tracker');
		
		switch($this->_pageType){
			case $h->getGoogleAdsPageTypeHome():
				$params = $this->getHomeParams();
				break;
			case $h->getGoogleAdsPageTypeSearchResults():
				$params = $this->getSearchResultsParams();
				break;
			case $h->getGoogleAdsPageTypeCategory():
				$params = $this->getCategoryParams();
				break;
			case $h->getGoogleAdsPageTypeProduct():
				$params = $this->getProductParams();
				break;
			case $h->getGoogleAdsPageTypeOther():
				$params = $this->getOtherParams();
				break;
			case $h->getGoogleAdsPageTypeCart():
				$params = $this->getCartParams();
				break;
			case $h->getGoogleAdsPageTypePurchase():
				$params = $this->getPurchaseParams();
				break;
		}
		
		$customerParams = $this->getCustomerParams();
		if(!empty($customerParams)){
			$params = array_merge($params, $customerParams);
		}
		return json_encode($params,JSON_NUMERIC_CHECK);
	}
	
	protected function getHomeParams(){
		$h = Mage::helper('tracker');
		return array('ecomm_prodid'=>'','ecomm_pagetype'=>$h->getGoogleAdsPageTypeHome(),'ecomm_totalvalue'=>'');
	}
	
	protected function getOtherParams(){
		$h = Mage::helper('tracker');
		return array('ecomm_prodid'=>'','ecomm_pagetype'=>$h->getGoogleAdsPageTypeOther(),'ecomm_totalvalue'=>'');
	}
	
	protected function getSearchResultsParams(){
		$h = Mage::helper('tracker');
		$items = Mage::getSingleton('catalogsearch/layer')->getProductCollection();
		$prodIds = array(); 
		//$totalValue = 0;
		foreach($items as $item){
			$prodIds[]= $item->getId(). "_".Mage::app()->getStore()->getId();
			//$totalValue+=$item->getFinalPrice();
		}
		return array('ecomm_prodid'=>$prodIds,'ecomm_pagetype'=>$h->getGoogleAdsPageTypeSearchResults(),'ecomm_totalvalue'=>'');
	}
	
	protected function getCategoryParams(){
		$h = Mage::helper('tracker');
		
		$items = Mage::getSingleton('catalog/layer')->getProductCollection();
		$prodIds = array();
		//$totalValue = 0;
		foreach($items as $item){
			$prodIds[]= $item->getId(). "_".Mage::app()->getStore()->getId();
			//$totalValue+=$item->getFinalPrice();
		}
		$categoryParams = array('ecomm_prodid'=>$prodIds,'ecomm_pagetype'=>$h->getGoogleAdsPageTypeCategory(),'ecomm_totalvalue'=>'');
		if($h->getGoogleAdsIsCategoryEnabled()){
			$categoryParams['ecomm_category']=Mage::registry('current_category')->getName();
		}
		return $categoryParams; 
	}
	
	protected function getProductParams(){
		$h = Mage::helper('tracker');
		$storeId = Mage::app()->getStore()->getId();
	
		$prod = Mage::registry('current_product');
		$prodId = $prod->getId()."_".Mage::app()->getStore()->getId();
		$productParams = array('ecomm_prodid'=>$prodId,'ecomm_pagetype'=>$h->getGoogleAdsPageTypeProduct(),'ecomm_totalvalue'=>$prod->getFinalPrice());
		
		if($h->getGoogleAdsIsReccomendedEnabled($storeId)){
			$relProds = array();
			foreach($prod->getRelatedProductCollection() as $relProd){
				$relProds[]=$relProd->getId()."_".$storeId;
			}
			if(!empty($relProds)){
				$productParams['ecomm_rec_prodid']=$relProds;
			}
		}
		
		if($h->getGoogleAdsIsPValueEnabled($storeId)){
			$productParams['ecomm_pvalue']=$prod->getFinalPrice();
		}
		
		return $productParams;
	}
	
	protected function getCartParams(){
		$h = Mage::helper('tracker');
		$storeId = Mage::app()->getStore()->getId();
		$quote = Mage::getSingleton('checkout/session')->getQuote();
		$items = $quote->getItemsCollection();
		
		$prodIds = array();
		$prices = array();
		$qty = array();
		$totalValue = 0;
		foreach($items as $item){
			$prodIds[]= $item->getProductId(). "_".$storeId;
			$qty[]=$item->getQty();
			$prices[] = $item->getPriceInclTax();
			$totalValue+=$item->getRowTotalInclTax();
		}
		
		$cartParams = array('ecomm_prodid'=>$prodIds,'ecomm_pagetype'=>$h->getGoogleAdsPageTypeCart(),'ecomm_totalvalue'=>$totalValue); 
		if($h->getGoogleAdsIsQuantityEnabled($storeId)){
			$cartParams['ecomm_quantity']=$qty;
		}
		
		if($h->getGoogleAdsIsPValueEnabled($storeId)){
			$cartParams['ecomm_pvalue']=$prices;
		}
		
		return $cartParams;
	}
	
	protected function getPurchaseParams(){
		$h = Mage::helper('tracker');
		$storeId = Mage::app()->getStore()->getId();
	
		$orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
		$order = Mage::getModel('sales/order')->load($orderId);
		$items = $order->getItemsCollection();
		
		$prodIds = array();
		$prices = array();
		$qty = array();
		$totalValue = 0;
		foreach($items as $item){
			$qty[]=$item->getQtyOrdered();
			$prodIds[]= $item->getProductId(). "_".$storeId;
			$totalValue+=$item->getRowTotalInclTax();
			$prices[] = $item->getPriceInclTax();
		}
		$purchParams = array('ecomm_prodid'=>$prodIds,'ecomm_pagetype'=>$h->getGoogleAdsPageTypePurchase(),'ecomm_totalvalue'=>$totalValue);
		if($h->getGoogleAdsIsQuantityEnabled($storeId)){
			$purchParams['ecomm_quantity']=$qty;
		}
		
		if($h->getGoogleAdsIsPValueEnabled($storeId)){
			$purchParams['ecomm_pvalue']=$prices;
		}
		
		return $purchParams; 
	}
	
	protected function getCustomerParams(){
		$h = Mage::helper('tracker');
		$storeId = Mage::app()->getStore()->getId();
		$customerParams = array();
		
		if(!Mage::getSingleton('customer/session')->isLoggedIn()) {
			if($h->getGoogleAdsIsCustomerHasAccountEnabled($storeId)){
				$customerParams['hasaccount']='n';
			}
			return $customerParams;
		}
		
		if($h->getGoogleAdsIsCustomerHasAccountEnabled($storeId)){
			$customerParams['hasaccount']='y';
		}
		
		$h = Mage::helper('tracker');
		$storeId = Mage::app()->getStore()->getId();
		$customer = Mage::getSingleton('customer/session')->getCustomer();
		
		$dob = $customer->getDob();
		if($h->getGoogleAdsIsCustomerAgeEnabled($storeId) && !empty($dob)){
			$customerParams['a']=$h->getAgeFromDob($dob);
		}
		
		$gender = $customer->getGender();
		if($h->getGoogleAdsIsCustomerGenderEnabled($storeId) && !empty($gender)){
			$customerParams['g']=strtolower(substr($gender,0,1));
		}
		
		if($h->getGoogleAdsIsCustomerRpEnabled($storeId)){
			
			$validStatuses = $h->getGoogleAdsValidOrderStatuses();
			$validStatusesArr = explode(',',$validStatuses);
			$orders = Mage::getModel('sales/order')->getCollection()
				->addFieldToFilter('customer_id', $customer->getId())
				->addFieldToFilter('status',array('in'=>$validStatusesArr));
			$ordersCount = $orders->count();
			if($ordersCount > 0){
				$customerParams['rp']='y';
			}else{
				$customerParams['rp']='n';
			}
		}
			
		return $customerParams;
	}
	
	
	
}
