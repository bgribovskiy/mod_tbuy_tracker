<?php

class Tbuy_Tracker_Block_Rocketfuel extends Mage_Core_Block_Template {

    public function canTrack() {
        return Mage::getStoreConfig("rocketfuel/settings/enabled");
    }

    public function getPageType() {
        $pageIdentifier = Mage::app()->getFrontController()->getAction()->getFullActionName();

        switch ($pageIdentifier) {
            case 'cms_index_noRoute':
            case 'cms_index_index':
                return 'home';

            case 'catalog_category_layered':
            case 'catalog_category_view':
                return 'category';

            case 'catalog_product_view':
                return 'product';

            case 'checkout_cart_index':
                return 'cart';

            case 'onestepcheckout_index_index':
                return 'checkout';

            case 'checkout_onepage_success':
                return 'purchase';

            default:
                return null;
        }
    }

}
