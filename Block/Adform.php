<?php

class Tbuy_Tracker_Block_Adform extends Mage_Core_Block_Template {

    protected $_quote = null;
    protected $_order = null;

    public function getCacheLifetime() {
        return null;
    }

    public function getCampaignId() {
        if (Mage::getStoreConfig("tracker/adform/campaignid") == "")
            return 0;
        return Mage::getStoreConfig("tracker/adform/campaignid");
    }

    public function getPointId() {
        return Mage::helper("tracker/adform")->getTag();
    }

    public function getScriptCode() {
        return <<<SCRIPTCODE
		(function(){var s=document.createElement('script');
			s.type='text/javascript';
			s.async=true;
			s.src='//track.adform.net/serving/scripts/trackpoint/async/';
			var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);})();

SCRIPTCODE;
    }

    public function getNoScriptCode() {
        $websiteName = Mage::app()->getStore()->getFrontendName();
        $categories = Mage::helper("tracker/adform")->getCategories();
        $product = Mage::helper("tracker/adform")->getProduct();
        $sections = "";
        $sections .= implode("|", $categories);
        if ($product)
            $sections .= "|{$product->getName()}";
        $path = Mage::helper("tracker/adform")->getCurrentUrlPath();
        return <<<NOSCRIPTCODE
			<noscript>
				<p style="margin:0;padding:0;border:0;">
					<img src="//track.adform.net/Serving/TrackPoint/?pm={$this->getCampaignId()}&amp;ADFPageName={$this->getAdfName()}&amp;ADFdivider=|" width="1" height="1" alt="" />
				</p>
			</noscript>

NOSCRIPTCODE;
    }

    public function getAdfName() {
        $adfname = Mage::getStoreConfig('tracker/adform/adfname');
        if (empty($adfname))
            $adfname = Mage::app()->getStore()->getFrontendName();
        $adfname .= " " . Mage::getStoreConfig('general/country/default');
        $adfname = strtoupper($adfname);

        $path = Mage::helper("tracker/adform")->getCurrentUrlPath();

        $adfname .= "/{$path}";
        return $adfname;
    }

    public function getCurrentQuote() {
        return Mage::getModel('checkout/cart')->getQuote();
    }

    public function getItemsArray($items, $type = null) {
        $itms = array();
        $i = 0;
        foreach ($items as $item) {

            if (is_null($type)) {
                $product = Mage::getModel("catalog/product")->load($item->getProductId());
                $itms[$i]['productname'] = $product->getName();
                $itms[$i]['productid'] = $product->getId();
                //$taxPercent = 1 + $item->getTaxPercent() / 100;
                $itemPrice = $item->getBasePrice();// $taxPercent;
                $itms[$i]['productsales'] = round(Mage::helper('directory')->currencyConvert($itemPrice, Mage::app()->getStore()->getCurrentCurrencyCode(), 'EUR'),2);
                $itms[$i]['productcount'] = $item->getQty();
                $itms[$i]['categoryname'] = Mage::helper("tracker/adform")->getProductCategory($product);
                $itms[$i]['step'] = 2;
                $itms[$i]['weight'] = $product->getWeight() > 0 ? $product->getWeight() : 1;
            } else {
                $itms[$i]['productid'] = $item->getProductId();
                //$taxPercent = 1 + $item->getTaxPercent() / 100;
                $itemPrice = $item->getBasePrice(); // $taxPercent;
                $itms[$i]['svn1'] = round(Mage::helper('directory')->currencyConvert($itemPrice, Mage::app()->getStore()->getCurrentCurrencyCode(),'EUR'),2);

                if ($type == 'quote') {
                    $itms[$i]['svn2'] = round($item->getQty());
                    $itms[$i]['step'] = 2;
                } elseif ($type == 'order') {
                    $itms[$i]['svn2'] = round($item->getQtyOrdered());
                    $itms[$i]['step'] = 3;
                }
            }

            $i++;
        }
        return $itms;
    }

    public function getQuoteItems($quote = null) {
        $this->_quote = $quote;
        if (!$this->_quote)
            $this->_quote = $this->getCurrentQuote();
        $cartItems = $this->_quote->getAllVisibleItems();
        $itms = $this->getItemsArray($cartItems, 'quote');
        return json_encode($itms);
    }

    public function getCurrentOrder() {
        $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
        if (!$orderId)
            return false;
        return Mage::getModel("sales/order")->load($orderId);
    }

    public function getCurrentOrderId() {
        return Mage::getSingleton('checkout/session')->getLastOrderId();
    }

    public function getCurrentOrderIncrementId() {
        $order = $this->getCurrentOrder();
        if (!$order)
            return 0;
        return $order->getIncrementId();
    }

    public function getOrderItems($order = null) {
        $this->_order = $order;
        if (!$this->_order)
            $this->_order = $this->getCurrentOrder();
        $orderItems = $this->_order->getAllVisibleItems();
        $itms = $this->getItemsArray($orderItems, 'order');
        return json_encode($itms);
    }


    public function getOrderTotal($element = null)
    {
        $this->_order = $element;
        $totalPrice = 0;
        foreach ($element->getAllItems() as $item) {
            if ($item->getParentItemId()) {
                continue;
            }


            //$taxPercent = 1 + $item->getTaxPercent() / 100;
            $basePrice = $item->getBasePrice() ;// $taxPercent; // - $item->getBaseDiscountAmount();
            $baseDiscountAmount = ($item->getBaseDiscountAmount()) ;// $taxPercent); // - $baseShippingDiscountAmount;

            if($element->getStoreCurrencyCode() !== 'EUR'){
                $basePrice = round(Mage::helper('directory')->currencyConvert($basePrice, Mage::app()->getStore()->getCurrentCurrencyCode(),'EUR'),2);
            }

            if ($basePrice == 0)
                $basePrice = number_format($basePrice, 2);

            $totalPrice += $basePrice * (is_null($item->getQtyOrdered())?$item->getQty():$item->getQtyOrdered()) - Mage::helper("tbflow")->fromBaseToEuroValue($element, $baseDiscountAmount);
        }
        return $totalPrice;
    }



    public function getCategoryTreeName($_current_category) {

        if (!is_object($_current_category)) {
            return;
        }

        $categoryName = null;
        $categoryTreeName = null;
        $categoryPath = $_current_category->getPath();
        $categoryIds = explode("/", $categoryPath);
        foreach ($categoryIds as $key => $categoryId) {
            if ($key < 2) {
                continue;
            }

            $categoryName = Mage::getResourceModel('catalog/category')->getAttributeRawValue($categoryId, 'name', Mage::app()->getStore()->getStoreId());
            if (is_null($categoryTreeName)) {
                $categoryTreeName .= $categoryName;
            } else {
                $categoryTreeName .= "/" . $categoryName;
            }
        }

        return $categoryTreeName;
    }

    public function getCategoryItems(Mage_Catalog_Model_Category $_current_category = null) {

        if (!is_object($_current_category)) {
            return;
        }

        if (!is_a($_current_category, 'Mage_Catalog_Model_Category')) {
            return;
        }

        $collection = $_current_category->getProductCollection()
                ->addFieldToFilter( 'status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED )
                ->addFieldToFilter( 'visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH )
                ->setPageSize(3);
        
        $itms = [];
        $i = 0;
        $categoryTreeName = $this->getCategoryTreeName($_current_category);
        foreach ($collection as $product) {
            $itms[$i]['categoryname'] = $categoryTreeName;
            $itms[$i]['productid'] = $product->getId();
            $i++;
        }

        return json_encode($itms);
    }

    public function getPaymentMethod() {
        if (!$this->_order)
            $this->_order = $this->getCurrentOrder();
        $paymentModel = $this->_order->getPayment();
        $payment = $paymentModel->getMethod();
        if ($payment == Tbuy_Tracker_Helper_Webtrekk::GLOBAL_COLLECT_PAYMENT_METHOD) {
            $additionalInfo = $paymentModel->getAdditionalInformation();
            $productId = $additionalInfo['payment_product_id'];
            $paymentProduct = Mage::getModel("globalcollect/payment_product")->load($productId);
            if (!$paymentProduct)
                return "Non Disponibile";
            return $paymentProduct->getCode();
        } else {
            return $payment;
        }
    }

    public function getCurrentCategoryCode(){
    	return Mage::helper('tracker/adform')->getCurrentCategoryCode();
    }

}
