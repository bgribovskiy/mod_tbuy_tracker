<?php

class Tbuy_Tracker_Block_Shinystat_Thankyou extends Mage_Core_Block_Template {

    protected $_template = 'tbuy/tracker/shinistat_thankyou.phtml';

    /**
     * @return string
     */
    protected function _toHtml() {
        if (!Mage::helper('tracker/shinystat')->isEnabled()) {
            return '';
        }

        $lastOrderId = Mage::getSingleton('checkout/session')->getLastOrderId();
        $lastOrder = Mage::getSingleton('sales/order')->load($lastOrderId);

        if ($lastOrder->getIncrementId()) {
            $items = $lastOrder->getAllVisibleItems();
            $ssProducts = $this->getProductsData($items);

            $this->setData('order_increment_id', $lastOrder->getIncrementId());
            $this->setData('order_grand_total', $lastOrder->getGrandTotal());
            $this->setData('order_tax_amount', $lastOrder->getTaxAmount());
            $this->setData('order_shipping_incl_tax', $lastOrder->getShippingInclTax());
            $this->setData('order_country_code', $lastOrder->getBillingAddress()->getCountry());
            $this->setData('order_region_name', $lastOrder->getBillingAddress()->getRegion());
            $this->setData('order_city', $lastOrder->getBillingAddress()->getCity());
            $this->setData('ss_prods', $ssProducts);
        }

        return parent::_toHtml();
    }

    /**
     * @param Mage_Sales_Model_Order_Item[] $items
     *
     * @return array
     */
    private function getProductsData(array $items)
    {
        $ssProducts = [];

        foreach ($items as $item) {
            $productData = [];
            $product = $item->getProduct();
            $productData[] = $product->getSku();
            $productData[] = (string)$item->getQtyOrdered();
            $productData[] = (string)$item->getPriceInclTax() - $item->getDiscountAmount();
            $productData[] = substr($product->getName(), 0, 100);
            $categoryIds = $product->getCategoryIds();

            if ($categoryIds) {
                $firstCategoryId = reset($categoryIds);
                if ($firstCategoryId) {
                    $firstCategory = Mage::getResourceModel('catalog/category_collection')
                        ->addIdFilter($firstCategoryId)
                        ->addAttributeToSelect('name')->getFirstItem();
                    $productData[] = substr($firstCategory->getName(), 0, 60);
                }
            }

            $ssProducts[] = Mage::helper('core')->jsonEncode($productData);
        }

        return $ssProducts;
    }
}
