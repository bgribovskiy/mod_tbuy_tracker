<?php

class Tbuy_Tracker_Block_Googletagmanager_Product extends Mage_Core_Block_Template {

	protected $_template = 'tbuy/tracker/googletagmanager_product.phtml';
    protected function _toHtml() {

        if (!Mage::getStoreConfig('tracker/google_tag_manager/enabled'))
            return '';

        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $helper       = Mage::helper('tracker/googletagmanager');
        $product      = Mage::registry('current_product');

        $product = $helper->getProductJson($product);
        $helper->addedToCart($this);

        //$this->setTemplate('tbuy/tracker/googletagmanager_product.phtml');
        $this->setData('currencyCode', $currencyCode);
        $this->setData('product', $product);
        $this->setData('onHead',Mage::helper('tracker/googletagmanager')->checkDataOnHead(Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_PROD));

        return parent::_toHtml();
    }
}
