<?php

class Tbuy_Tracker_Block_Googletagmanager_Resultimpressions extends Mage_Core_Block_Template {

	protected $_template = 'tbuy/tracker/googletagmanager_impressions.phtml';

    protected function _toHtml() {
        if (!Mage::getStoreConfig('tracker/google_tag_manager/enabled'))
            return '';

        # blocco per la pagina dei risultati della ricerca

        $collection   = $this->getLayout()->getBlock('search_result_list')->getLoadedProductCollection();
        if ($collection->getSize() == 0)
            return '';

        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $helper       = Mage::helper('tracker/googletagmanager');

        list($impressions, $onClick) = $helper->getImpressionsJson($collection);

        //$this->setTemplate('tbuy/tracker/googletagmanager_impressions.phtml');
        $this->setData('currencyCode', $currencyCode);
        $this->setData('impressions', $impressions);
        $this->setData('onClick', $onClick);
        $this->setData('onHead', Mage::helper('tracker/googletagmanager')->checkDataOnHead(Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_SEARCH));
        $this->setData('impressionChunk', (int) Mage::getStoreConfig('tracker/google_tag_manager/max_num_impression'));

        return parent::_toHtml();
    }
}
