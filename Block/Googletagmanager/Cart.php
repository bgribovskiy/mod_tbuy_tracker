<?php

class Tbuy_Tracker_Block_Googletagmanager_Cart extends Mage_Core_Block_Template {

	protected $_template = 'tbuy/tracker/googletagmanager_cart.phtml';

    protected function _toHtml() {
        if (!Mage::getStoreConfig('tracker/google_tag_manager/enabled'))
            return '';

        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $helper       = Mage::helper('tracker/googletagmanager');

        list($collection, $cartClick) = $helper->getCartJson();

        $jqHead = Mage::getStoreConfig('tracker/google_tag_manager/remove_from_cart_header');
        // se abilitato troviamo già questi dati in pagina
        if (!$jqHead)
            $this->setData('cartClick', $cartClick);

        //$this->setTemplate('tbuy/tracker/googletagmanager_cart.phtml');
        $this->setData('currencyCode', $currencyCode);
        $this->setData('collection', $collection);

        return parent::_toHtml();
    }
}
