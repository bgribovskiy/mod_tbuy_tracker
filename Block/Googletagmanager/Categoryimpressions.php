<?php

class Tbuy_Tracker_Block_Googletagmanager_Categoryimpressions extends Mage_Core_Block_Template {

	protected $_template = 'tbuy/tracker/googletagmanager_impressions.phtml';

    protected function _toHtml() {
        if (!Mage::getStoreConfig('tracker/google_tag_manager/enabled'))
            return '';

        # blocco per la pagina della categoria. mostriamo le
        # impressions dei prodotti presenti in questa categoria

        $layer    = Mage::getSingleton('catalog/layer');
        $category = $layer->getCurrentCategory();

        // static block only, non ci sono prodotti
        if ($category->getDisplayMode() == 'PAGE')
            return '';

        $designs = Mage::getStoreConfig('tracker/google_tag_manager/custom_designs');
        $design  = $category->getCustomDesign();
        $designs = explode(',', $designs);
        if (!empty($design) && in_array($design, $designs))
            return '';

        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $helper       = Mage::helper('tracker/googletagmanager');
        $collection   = $layer->getProductCollection();

        //  DUC-2424, problem with limit in category list
        $list         = $this->getLayout()->createBlock('catalog/product_list_toolbar');
        $limit        = isset($params['limit']) ? $params['limit'] : $list->getLimit();
        $startCounter = $limit * ($this->getRequest()->getParam('p', 1)-1);
        $this->getLayout()->createBlock('page/html_pager')->setLimit($limit)->setCollection($collection);
        if($startCounter > 0) {
            $helper->setCounter((int) $startCounter + 1);
        }
        $helper->denyResetCounter();
        list($impressions, $onClick) = $helper->getImpressionsJson($collection);
        $helper->allowResetCounter();

        //$this->setTemplate('tbuy/tracker/googletagmanager_impressions.phtml');
        $this->setData('currencyCode', $currencyCode);
        $this->setData('impressions', $impressions);
        $this->setData('onClick', $onClick);
        $this->setData('onHead', Mage::helper('tracker/googletagmanager')->checkDataOnHead(Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_CATEGORY));
        $this->setData('impressionChunk', (int) Mage::getStoreConfig('tracker/google_tag_manager/impression_chunk_size'));

        return parent::_toHtml();
    }
}
