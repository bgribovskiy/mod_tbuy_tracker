<?php

class Tbuy_Tracker_Block_Googletagmanager_Thankyou extends Mage_Core_Block_Template {

	protected $_template = 'tbuy/tracker/googletagmanager_thankyou.phtml';

    protected function _toHtml() {
        if (!Mage::getStoreConfig('tracker/google_tag_manager/enabled'))
            return '';

        $tagManagerId = Mage::getStoreConfig('tracker/google_tag_manager/tag_manager_id');
        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $helper       = Mage::helper('tracker/googletagmanager');
        $data         = $helper->getOrderData();

        //$this->setTemplate('tbuy/tracker/googletagmanager_thankyou.phtml');
        $this->setData('tag_manager_id', $tagManagerId);
        $this->setData('currencyCode', $currencyCode);
        $this->setData('details', $data['details']);
        $this->setData('head', $data['head']);

        $this->setData('tag_manager_order_id', $data['tag_manager_order_id']);
        $this->setData('tag_manager_order_total', $data['tag_manager_order_total']);
        $this->setData('tag_manager_order_shipping', $data['tag_manager_order_shipping']);
        $this->setData('tag_manager_order_tax', $data['tag_manager_order_tax']);
        $this->setData('tag_manager_payment_type', $data['tag_manager_payment_type']);
        $this->setData('tag_manager_affiliation', $data['tag_manager_affiliation']);
        $this->setData('tag_manager_coupon', $data['tag_manager_coupon']);
        $this->setData('tag_manager_coupon_description', $data['tag_manager_coupon_description']);

        $this->setData('all_data_on_head', Mage::helper('tracker/googletagmanager')->checkDataOnHead(Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_THANKYOU));

        return parent::_toHtml();
    }
}
