<?php

class Tbuy_Tracker_Block_Googletagmanager_Wishlist extends Mage_Core_Block_Template {

	protected $_template = 'tbuy/tracker/googletagmanager_wishlist.phtml';

    protected function _toHtml() {
        if (!Mage::getStoreConfig('tracker/google_tag_manager/enabled'))
            return '';

        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $helper       = Mage::helper('tracker/googletagmanager');
        $onClick      = $helper->getWishlistJson();

        //$this->setTemplate('tbuy/tracker/googletagmanager_wishlist.phtml');
        $this->setData('currencyCode', $currencyCode);
        $this->setData('onClick', $onClick);

        return parent::_toHtml();
    }
}
