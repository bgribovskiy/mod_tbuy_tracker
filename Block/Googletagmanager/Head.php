<?php

class Tbuy_Tracker_Block_Googletagmanager_Head extends Mage_Core_Block_Template
{

	protected $_template = 'tbuy/tracker/googletagmanager_head.phtml';

    protected function _toHtml()
    {
        if (!Mage::getStoreConfig('tracker/google_tag_manager/enabled'))
            return '';

        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $language = Mage::app()->getLocale()->getLocaleCode();
        $helper = Mage::helper('tracker/googletagmanager');
        $pageCategory = $helper->getPageCategory();
        $data = $helper->getUserData();

        //$this->setTemplate('tbuy/tracker/googletagmanager_head.phtml');
        $this->setData('tag_manager_lang', strtolower(substr($language, 0, 2)));
        $this->setData('currencyCode', $currencyCode);

        $this->setData('tag_manager_page_category', $pageCategory);
        $this->setData('tag_manager_logged', $data['logged']);
        $this->setData('tag_manager_userId', $data['userId']);
        $this->setData('tag_manager_user', $data);
        $this->setData('tag_manager_gender', $data['gender']);
        $this->setData('tag_manager_city', $data['city']);
        $this->setData('tag_manager_birthYear', $data['birthYear']);

        $orderData = null;
        // questi dati extra servono solo a benetton. non sono presenti dentro
        // _additional_structure perché sono dentro i template locali di benetton
        if (!$helper->isSuccessPage()) {
            $this->setData('extended', false);
        } else {
            $orderData = $helper->getOrderData();
            $this->setData('extended', true);
            $this->setData('head', $orderData['head']);
            $this->setData('tag_manager_order_id', $orderData['tag_manager_order_id']);
            $this->setData('tag_manager_order_total', $orderData['tag_manager_order_total']);
            $this->setData('tag_manager_order_shipping', $orderData['tag_manager_order_shipping']);
            $this->setData('tag_manager_order_tax', $orderData['tag_manager_order_tax']);
            $this->setData('tag_manager_payment_type', $orderData['tag_manager_payment_type']);
            $this->setData('tag_manager_affiliation', $orderData['tag_manager_affiliation']);
        }

        $helper->addedToCart($this);
        if (count(explode(",",Mage::getStoreConfig('tracker/google_tag_manager/all_data_on_head'))) > 0) {

            $impressions = $impressionsCategory = $impressionsSearch = array();
            if($helper->checkDataOnHead(Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_PROD)) {
                $product = Mage::registry('current_product');
                if ($product) {
                    $product = $helper->getProductJson($product);
                    $this->setData('product', $product);
                }
            }
            if($helper->checkDataOnHead(Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_SEARCH)) {
                $block = $this->getLayout()->getBlock('search_result_list');
                if (is_object($block)) {
                    /** @var Mage_Catalog_Block_Product_List_Toolbar $toolbar */
                    $toolbar = Mage::getBlockSingleton("catalog/product_list_toolbar");
                    $page_size = $toolbar->getDefaultPerPageValue();
                    $curr_page = $toolbar->getCurrentPage();
                    $collectionSearch = $block->getLoadedProductCollection();
                    $collectionSearch->setCurPage($curr_page);
                    $collectionSearch->setPageSize($page_size);
                    if ($collectionSearch->getSize() > 0) {
                        list($impressionsSearch, $onClick) = $helper->getImpressions($collectionSearch);
                        $collectionSearch->clear();
                    }
                }
            }
            if($helper->checkDataOnHead(Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_CATEGORY) &&
                Mage::app()->getFrontController()->getRequest()->getControllerName() == "category") {
                    /** @var Mage_Catalog_Block_Product_List_Toolbar $toolbar */
                    $toolbar = Mage::app()->getLayout()->createBlock("catalog/product_list_toolbar");

                    if (Mage::getSingleton('catalog/layer')->getCurrentCategory()->getId() != Mage::app()->getStore()->getRootCategoryId()) {
                        $originalCollection = Mage::getSingleton('catalog/layer')->getProductCollection();
                        $collectionCategory = $this->prepareCollectionCategory($originalCollection);
                        // apply collection order (SORT BY in MySQL) on the product collection.
                        // If this is not done we cannot guarantee that the product collection is ordered
                        // like the one on the listing page
                        $toolbar->setCollection($collectionCategory);

                        if ($collectionCategory->getSize() > 0) {
                            list($impressionsCategory, $onClick) = $helper->getImpressions($collectionCategory);
                            // clear the collection to prevent the loaded that was done in getImpressions
                            $collectionCategory->clear();
                            // reset the the collection sort order applied form the $toolbar->setCollection($collectionCategory);
                            $collectionCategory->getSelect()->reset(Zend_Db_Select::ORDER);
                        }
                    }
            }
            if($helper->checkDataOnHead(Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_CHECKOUT) &&
                Mage::app()->getFrontController()->getRequest()->getRequestedRouteName() == "onestepcheckout") {

                list($collection, $onClick) = $helper->getCartJson();

                $this->setData('collection', $collection);
                if(is_object(Mage::getSingleton('checkout/session')->getQuote())) {
                    if (Mage::getSingleton('checkout/session')->getQuote()->getIsVirtual()) {
                        $value = Mage::getSingleton('checkout/session')->getQuote()->getBillingAddress()->getSubtotal();
                    } else {
                        $value = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getSubtotal();
                    }
                } else {
                    $value = 0;
                }

                $this->setData('metric3', $value);
                $this->setData('onClick', $onClick);
                $this->setData('checkout', true);

            }

            unset($onClick);
            $impressions = array_merge($impressions, $impressionsCategory, $impressionsSearch);
            if (count($impressions) > 0) {
                $this->setData('impressions', Zend_Json::encode($impressions));
	            if (Mage::getStoreConfig('tracker/google_tag_manager/impression_chunk_size') > 0) {
		            $this->setData('impressionsSliced', Zend_Json::encode(array_slice($impressions, 0, Mage::getStoreConfig('tracker/google_tag_manager/impression_chunk_size'))));
	            } else {
		            $this->setData('impressionsSliced', Zend_Json::encode($impressions));
	            }
                $this->setData('impressionChunk', (int)Mage::getStoreConfig('tracker/google_tag_manager/impression_chunk_size'));
            }
        }
        // END
        if ($helper->isSuccessPage()) {
            if (is_null($orderData)) {
                $orderData = $helper->getOrderData();
            }
            $this->setData('details', $orderData['details']);
            $this->setData('tag_manager_conferma_iscrizione', $orderData['tag_manager_conferma_iscrizione']);
            foreach ($orderData as $key => $value) {
                if (strrpos($key, 'tag_manager_', -strlen($key)) !== false) {
                    $this->setData($key, $orderData[$key]);
                }
            }


        }
        return parent::_toHtml();
    }

    /**
     * @param $originalCollection
     * @return Tbuy_Tracker_Model_Catalog_Collection
     */
    public function prepareCollectionCategory($originalCollection){
        $select = $originalCollection->getSelect();
        $newSelect = clone $select;
        $newCollection = Mage::getModel("tracker/Catalog_Collection");
        $newCollection->setSelect($newSelect);
        return $newCollection;
    }
}
