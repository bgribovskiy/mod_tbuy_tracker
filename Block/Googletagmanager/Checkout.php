<?php

class Tbuy_Tracker_Block_Googletagmanager_Checkout extends Mage_Core_Block_Template {

	protected $_template='tbuy/tracker/googletagmanager_checkout.phtml';

    protected function _toHtml() {
        if (!Mage::getStoreConfig('tracker/google_tag_manager/enabled'))
            return '';
        
        $helper       = Mage::helper('tracker/googletagmanager');

        list($collection, $onClick) = $helper->getCartJson();

        //$this->setTemplate('tbuy/tracker/googletagmanager_checkout.phtml');
        $this->setData('collection', $collection);
        if(is_object(Mage::getSingleton('checkout/session')->getQuote())) {
            if (Mage::getSingleton('checkout/session')->getQuote()->getIsVirtual()) {
                $value = Mage::getSingleton('checkout/session')->getQuote()->getBillingAddress()->getSubtotal();
            } else {
                $value = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getSubtotal();
            }
        } else {
            $value = 0;
        }
        $this->setData('metric3',$value);
        $this->setData('onClick', $onClick);
        $this->setData('onHead',Mage::helper('tracker/googletagmanager')->checkDataOnHead(Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_CHECKOUT));

        return parent::_toHtml();
    }
}
