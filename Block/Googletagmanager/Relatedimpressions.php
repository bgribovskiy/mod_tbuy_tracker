<?php

class Tbuy_Tracker_Block_Googletagmanager_Relatedimpressions extends Mage_Core_Block_Template {

	protected $_template = 'tbuy/tracker/googletagmanager_impressions.phtml';

    protected function _toHtml() {
        if (!Mage::getStoreConfig('tracker/google_tag_manager/enabled'))
            return '';

        # blocco per la pagina del prodotto (o eventualmente anche altre).
        # mostriamo le impressions dei prodotti correlati

        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $helper       = Mage::helper('tracker/googletagmanager');
        // settato dal nostro observer
        $collection   = $this->getData('collection');

        list($impressions, $onClick) = $helper->getImpressions($collection);

        if((string) $this->getData('list_type') !== "" && is_array($impressions)) {
            foreach($impressions as $key => $impr) {
                $impr['list'] = $impr['list']."-".$this->getData('list_type');
                $impressions[$key] = $impr;
            }
        }

        //$this->setTemplate('tbuy/tracker/googletagmanager_impressions.phtml');
        $this->setData('currencyCode', $currencyCode);
        $this->setData('impressions', Zend_Json::encode($impressions));
        $this->setData('onClick', $onClick);
        $this->setData('impressionChunk', (int) Mage::getStoreConfig('tracker/google_tag_manager/impression_chunk_size'));

        return parent::_toHtml();
    }
}
