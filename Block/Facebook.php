<?php

class Tbuy_Tracker_Block_Facebook extends Mage_Core_Block_Template {

    protected $_canTrackOrder = false;

    public function _construct() {
        parent::_construct();
        $this->pixel_id = Mage::getStoreConfig("facebook/settings/pixelid");
        $this->helper = Mage::helper("tracker");
        $this->order = Mage::getModel("sales/order")->load($this->getOrderId());
        if ($this->order->getId() && Mage::getStoreConfig("facebook/settings/enabled")) {
            $this->_canTrackOrder = true;
            $this->getTracker();
        }
    }

    public function canTrackOrder() {

        if(!$this->_canTrackOrder){
            return false;
        }
        if(!Mage::getStoreConfig("facebook/settings/enabled_for_successpage")){
            return false;
        }
        return true;
    }

    public function getTracker() {

        $this->currency = Mage::app()->getStore()->getCurrentCurrencyCode();
        $this->baseCurrency = Mage::app()->getBaseCurrencyCode();
        //  FIXED
        $baseGT = $this->order->getBaseGrandTotal();
        $baseTaxAmount = $this->order->getBaseTaxAmount();
        $baseShippingAmount = $this->order->getBaseShippingAmount();
        $baseCodFee = $this->order->getBaseCodFee();
        $baseShippingDiscountAmount = $this->order->getBaseShippingDiscountAmount();

        $this->baseGrandTotal = $baseGT - $baseTaxAmount - $baseShippingAmount - $baseCodFee + $baseShippingDiscountAmount;
        //$this->baseGrandTotal = Mage::helper('directory')->currencyConvert($this->baseGrandTotal, $currency, $baseCurrency);
    }

}
