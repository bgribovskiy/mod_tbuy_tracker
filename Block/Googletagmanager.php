<?php

class Tbuy_Tracker_Block_Googletagmanager extends Mage_Core_Block_Template {

	protected $_template = 'tbuy/tracker/googletagmanager.phtml';

    protected function _toHtml() {
        if (!Mage::getStoreConfig('tracker/google_tag_manager/enabled'))
            return '';

        $tagManagerId = Mage::getStoreConfig('tracker/google_tag_manager/tag_manager_id');
        $helper       = Mage::helper('tracker/googletagmanager');

        $jqHead = Mage::getStoreConfig('tracker/google_tag_manager/remove_from_cart_header');
        if ($jqHead) {
            $currencyCode                 = Mage::app()->getStore()->getCurrentCurrencyCode();
            list($collection, $cartClick) = $helper->getCartJson();

            $this->setData('currencyCode', $currencyCode);
            $this->setData('collection', $collection);
            $this->setData('cartClick', $cartClick);
        }

        //$this->setTemplate('tbuy/tracker/googletagmanager.phtml');
        $this->setData('tag_manager_id', $tagManagerId);

        return parent::_toHtml();
    }
}
