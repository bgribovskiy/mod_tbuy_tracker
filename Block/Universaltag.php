<?php

class Tbuy_Tracker_Block_Universaltag extends Mage_Core_Block_Template {

    public function getUniversalTagId() {

        return Mage::getStoreConfig("tracker/universaltag/universal_tag_id");
    }

    public function getElementQtyItems($element, $type = 'quote') {

        if (!is_object($element)) {
            return;
        }
        $qty = 0;
        foreach ($element->getAllItems() as $item) {
            if ('simple' != $item->getProductType())
                continue;

            if ($type == 'quote')
                $qty += $item->getQty();
            else
                $qty += $item->getQtyOrdered();
        }

        return $qty;
    }

    public function getTotal($element) {

        if (!is_object($element)) {
            return;
        }
        $baseGrandTotal = $element->getBaseGrandTotal();

        return $baseGrandTotal;
    }

    public function getElementProductsSku($element) {

        if (!is_object($element)) {
            return;
        }
        $cartItems = $element->getAllVisibleItems();
        $itms = array();
        $i = 0;
        foreach ($cartItems as $item) {
            $itms[$i]['sku'] = $item->getSku();
            $i++;
        }

        return json_encode($itms);
    }

    public function getElementProductsName($element) {

        if (!is_object($element)) {
            return;
        }
        $cartItems = $element->getAllVisibleItems();
        $itms = array();
        $i = 0;
        foreach ($cartItems as $item) {
            $itms[$i]['name'] = $item->getName();
            $i++;
        }

        return json_encode($itms);
    }

    public function getElementCategoryName($element) {

        if (!is_object($element)) {
            return;
        }
        $cartItems = $element->getAllVisibleItems();
        $itms = array();
        $i = 0;
        foreach ($cartItems as $item) {
            $product = Mage::getModel('catalog/product')->load($item->getProductId());
            $itms[$i]['category'] = $this->getProductCategory($product);
            $i++;
        }

        return json_encode($itms);
    }

    public function getProductCategory($product) {
        $category = null;
        $currentStore = Mage::app()->getStore()->getId();
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        if (Mage::registry("current_category"))
            $category = Mage::getModel('catalog/category')->load(Mage::registry("current_category")->getId());

        if (!$category) {
            $categories = $product->getCategoryCollection();
            foreach ($categories as $cat) {
                //	level is lower
                if (!is_null($category) && $cat->getLevel() < $category->getLevel())
                    continue;

                //	level is equal but id is newer (take the old one)
                if (!is_null($category) && ($cat->getLevel() == $category->getLevel() && $cat->getId() > $category->getId()))
                    continue;
                $category = $cat;
            }
        }

        if ($category) {
            $category = Mage::getModel('catalog/category')->load($category->getId());
        }

        Mage::app()->setCurrentStore($currentStore);
        return !$category ? "" : $category->getName();
    }

}
