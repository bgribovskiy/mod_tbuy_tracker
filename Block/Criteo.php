<?php

class Tbuy_Tracker_Block_Criteo extends Mage_Core_Block_Template {

	public function getCacheLifetime() {
		return null;
	}

	public function getCustomerId() {
		if (Mage::getSingleton('customer/session')->isLoggedIn()) {
			$customerData = Mage::getSingleton('customer/session')->getCustomer();
			return $customerData->getId();
		}
		return false;
	}

	/**
	 * returns the { event: "setCustomerId", id: "<<<ID>>>"}, for criteo tracking.
	 * If hole punching is enabled it adds the nocache key tags
	 *
	 * @return string | boolean
	 */
	public function getCustomerIdEvent() {
		if (Mage::getSingleton('customer/session')->isLoggedIn()) {
			$customerData = Mage::getSingleton('customer/session')->getCustomer();
			/**
			 * check is the hole punching module is enable and if it's add the
			 * necessary tags to punch the block. The holes controller will check for the key added here
			 */
			if (Mage::helper('core')->isModuleEnabled('Tbuy_Holes')) {
				$customerIdEvent = '<!-- NOCACHE key="' . Tbuy_Tracker_Helper_Data::HOLE_PUNCHING_TRACKER_KEY . '" -->
                    { event: "setCustomerId", id: "' . $customerData->getId() . '"},
                    { event: "setHashedEmail", email: "' . md5(strtolower($customerData->getEmail())) . '"},
                    <!-- ENDNOCACHE -->';
			} else {
				$customerIdEvent = '{ event: "setCustomerId", id: "' . $customerData->getId() . '"},
                { event: "setHashedEmail", email: "' . md5(strtolower($customerData->getEmail())) . '"},';
			}
			return $customerIdEvent;
		} elseif (Mage::helper('core')->isModuleEnabled('Tbuy_Holes')) {
			return '<!-- NOCACHE key="' . Tbuy_Tracker_Helper_Data::HOLE_PUNCHING_TRACKER_KEY . '" --><!-- ENDNOCACHE -->';
		} else {
			return false;
		}
	}

	/**
	 * Edit for UNI-835
	 *
	 * @return string
	 */
	public function getSiteType() {
		if (Mage::helper('tracker')->isCriteoDetectDeviceEnabled(Mage::app()->getStore()->getId())
			&& class_exists('Tbuy_MobileOrders_Helper_Data', false)
			&& file_exists(Mage::getBaseDir() . DS . 'js' . DS . Tbuy_MobileOrders_Helper_Data::DETECTOR_JS)
		) {
			return "(DeviceDetector.isMobile()?'m':(DeviceDetector.isTablet()?'t':'d'))";

		}
		return Mage::helper('tracker')->getCriteoDefaultDevice(Mage::app()->getStore()->getId());
	}

	public function getAccount() {
		return Mage::helper('tracker')->getCriteoAccount(Mage::app()->getStore()->getId());
	}

	public function getLoader() {
		return Mage::helper('tracker')->getCriteoLoader(Mage::app()->getStore()->getId());
	}

	public function getProductId() {
		if (Mage::helper('tracker')->getCriteoUseSku(Mage::app()->getStore()->getId())) {
			return Mage::registry('current_product')->getSku();
		}
        return Mage::registry('current_product')->getId() . (Mage::helper('tracker')->getAppendStoreId(Mage::app()->getStore()->getId()) ? '_' . Mage::app()->getStore()->getId() : '' );
	}

	public function getCategoryProductIds() {
		$select = clone Mage::getSingleton('catalog/layer')->getProductCollection()->getSelect();
		//WHLP-818
		$orderBy = $this->getCurrentOrder();
		$table = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $orderBy)->getBackend()->getTable();
		$attributeId = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $orderBy)->getAttributeId();

		if ($attributeId) {
			$orderBy = "k" . $orderBy;
			$select->join(array('attributeTable' => $table), 'e.entity_id = attributeTable.entity_id', array($orderBy => 'attributeTable.value'))
				->where("attributeTable.attribute_id = ?", $attributeId);
			$select->limit($this->getPageSize(), $this->getCurrentPage());
		}

		//Mage 1.6 Unknown column 'cat_index_position' fix
		//	Stupid Patch

		if ($orderBy == "position")
			$orderBy = "cat_index.{$orderBy}";

		$products = array();

		try {
			$select->order($orderBy . ' ' . $this->getCurrentDirection());
			$conn = Mage::getSingleton('core/resource')->getConnection('core_read');
			$query_object = $conn->query($select);
			$products = array();
			while ($fetched_product = $query_object->fetch()) {
				$products[] = $fetched_product;
			}

		} catch (Exception $ex) {
			Mage::logException($ex);
		}


		$limit = Mage::helper('tracker')->getCriteoListingLimit(Mage::app()->getStore()->getId());
		$pIds = array();
		$i = 0;

		foreach ($products as $product) {
			$i++;
			if (!empty($limit) && $i > $limit) {
				break;
			}
			$pIds[] = $product['entity_id'];
		}

		if (Mage::helper('tracker')->getCriteoUseSku(Mage::app()->getStore()->getId())) {
			$skus = Mage::getResourceModel('catalog/product')->getProductsSku($pIds);
			$pIds = array();
			foreach ($skus as $sku) {
				$pIds[] = $sku['sku'];
			}
		}

        if (Mage::helper('tracker')->getAppendStoreid(Mage::app()->getStore()->getId())) {
            $productIds = $pIds;
            $pIds = array();
            foreach($productIds as $productId) {
                $pIds[] = $productId . '_' . Mage::app()->getStore()->getId();
            }
        }
		return '"' . implode('","', $pIds) . '"';
	}

	protected function getPageSize() {
		$layout = Mage::getSingleton('core/layout');
		$toolBarBlock = $layout->getBlock('product_list_toolbar');
		if (!empty($toolBarBlock)) {
			$limit = $toolBarBlock->getLimit();
		} else {
			$limit = Mage::getStoreConfig('catalog/frontend/list_per_page');
		}

		return $limit;
	}

	protected function getCurrentOrder() {
		$layout = Mage::getSingleton('core/layout');
		$toolBarBlock = $layout->getBlock('product_list_toolbar');
		if (!empty($toolBarBlock)) {
			/** @var Mage_Catalog_Block_Product_List_Toolbar $toolBarBlock */
			$order = $toolBarBlock->getCurrentOrder();
			// refresh the cached order in the toolbar block
			$toolBarBlock->unsetData('_current_grid_order');
		} else {
			$availableOrders = Mage::getSingleton('catalog/config')->getAttributeUsedForSortByArray();
			$order = Mage::getStoreConfig(Mage_Catalog_Model_Config::XML_PATH_LIST_DEFAULT_SORT_BY);

			if (!isset($availableOrders[$order])) {
				$keys = array_keys($availableOrders);
				$order = $keys[0];
			}
		}
		//JIRA:: MASEMAG-497
		if ($order == 'price') {
			$order = 'min_price';
		}

		return $order;
	}

	protected function getCurrentDirection() {
		$layout = Mage::getSingleton('core/layout');
		$toolBarBlock = $layout->getBlock('product_list_toolbar');

		if (!empty($toolBarBlock)) {
			$direction = $toolBarBlock->getCurrentDirection();
		} else {
			$directions = array('asc', 'desc');
			$dir = strtolower($this->getRequest()->getParam($this->getDirectionVarName()));
			if ($dir && in_array($dir, $directions)) {
				$direction = $dir;
			} else {
				$direction = Mage::getSingleton('catalog/session')->getSortDirection();
			}
			// validate direction
			if (!$dir || !in_array($dir, $directions)) {
				$direction = 'desc';
			}
		}

		return $direction;
	}

	protected function getCurrentPage() {
		$layout = Mage::getSingleton('core/layout');

		$toolBarBlock = $layout->getBlock('product_list_toolbar');

		if (!empty($toolBarBlock)) {
			$currentPage = $toolBarBlock->getCurrentPage();
		} else {
			if ($page = (int)$this->getRequest()->getParam($this->getPageVarName())) {
				$currentPage = $page;
			} else {
				$currentPage = 1;
			}
		}
		return $currentPage;
	}

	public function getSearchResultProductIds() {


		$layout = Mage::getSingleton('core/layout');
		$resultBlock = $layout->getBlock('product_list_toolbar');

		$products = $resultBlock->getCollection();

		$limit = Mage::helper('tracker')->getCriteoListingLimit(Mage::app()->getStore()->getId());
		$pIds = array();
		$i = 0;
		//  Avoid warning in catalog_search... ?!
		if ($products) {
			foreach ($products as $product) {
				$i++;
				if (!empty($limit) && $i > $limit) {
					break;
				}
				$pIds[] = $product->getId();
			}
		}

		if (Mage::helper('tracker')->getCriteoUseSku(Mage::app()->getStore()->getId())) {
			$skus = Mage::getResourceModel('catalog/product')->getProductsSku($pIds);
			$pIds = array();
			foreach ($skus as $sku) {
				$pIds[] = $sku['sku'];
			}
		}

        if (Mage::helper('tracker')->getAppendStoreid(Mage::app()->getStore()->getId())) {
            $productIds = $pIds;
            $pIds = array();
            foreach($productIds as $productId) {
                $pIds[] = $productId . '_' . Mage::app()->getStore()->getId();
            }
        }
		return '"' . implode('","', $pIds) . '"';
	}

	public function getSearchTerm() {
		return Mage::helper('catalogsearch')->getQuery()->getQueryText();
	}

	public function getCartArrayString() {
		$pIds = array();
		$quote = Mage::getSingleton('checkout/cart')->getQuote();
		$items = $quote->getAllVisibleItems();

		foreach ($items as $item) {
			$itemRow = $this->_getCartRow($item);
			if ($itemRow) {
				$pIds[] = $itemRow;
			}
		}
		return implode(',', $pIds);
	}

	public function getTranscationId() {
		$orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
		$order = Mage::getModel('sales/order')->load($orderId);
		return $order->getIncrementId();
	}

	public function getIsFirstOrder() {
		if ($customerId = $this->getCustomerId()) {
			$orders = Mage::getResourceModel('sales/order_collection')
				->addFieldToSelect('*')
				->addFieldToFilter('customer_id', $customerId);
			return (int)!($orders->count() > 1);
		}
		return 1;
	}

	public function getOrderArrayString() {
		$pIds = array();
		$orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
		$order = Mage::getModel('sales/order')->load($orderId);
		$items = $order->getItemsCollection();

		foreach ($items as $item) {
			$itemRow = $this->_getCartRow($item);
			if ($itemRow) {
				$pIds[] = $itemRow;
			}
		}

		return implode(',', $pIds);
	}

	protected function _getCartRow($item) {
		$price = $item->getPriceInclTax();
		$sku = $item->getSku();
		$id = $item->getProductId();
		$qty = $item->getQty();
		// if it is an order
		if (!$qty >= 1) {
			$qty = $item->getQtyOrdered();
		}

		if ($parent = $item->getParentItem()) {
			return false;
		}

		if ($item->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {

			if (Mage::helper('tracker')->getCriteoUseConfigurable(Mage::app()->getStore()->getId())) {
				$skus = Mage::getResourceModel('catalog/product')->getProductsSku(array($id));
				$sku = $skus[0]['sku'];
			} else {
				$id = Mage::getResourceModel('catalog/product')->getIdBySku($sku);
			}
		}

        if (Mage::helper('tracker')->getAppendStoreId(Mage::app()->getStore()->getId())) {
            $id .= '_' . Mage::app()->getStore()->getId();
        }

		if (Mage::helper('tracker')->getCriteoUseSku(Mage::app()->getStore()->getId())) {
			return '{ id: "' . $sku . '", price: ' . number_format($price, 2, '.', '') . ', quantity: ' . $qty . ' }';
		} else {
			return '{ id: "' . $id . '", price: ' . number_format($price, 2, '.', '') . ', quantity: ' . $qty . ' }';
		}
	}
}
