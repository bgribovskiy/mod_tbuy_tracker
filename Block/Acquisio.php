<?php

class Tbuy_Tracker_Block_Acquisio extends Mage_Core_Block_Template {

    protected $_quote = null;
    protected $_order = null;

    public function getCacheLifetime() {
        return null;
    }

    public function getCampaignId() {
        if (Mage::getStoreConfig("tracker/acquisio/campaignid") == "")
            return 0;
        return Mage::getStoreConfig("tracker/acquisio/campaignid");
    }

    public function getCurrentOrder() {
        $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
        if (!$orderId)
            return false;
        return Mage::getModel("sales/order")->load($orderId);
    }

    public function getCurrentOrderId() {
        return Mage::getSingleton('checkout/session')->getLastOrderId();
    }

    public function getCurrentOrderIncrementId() {
        $order = $this->getCurrentOrder();
        if (!$order)
            return 0;
        return $order->getIncrementId();
    }

    public function getItemsCount($order = null) {
        $this->_order = $order;
        if (!$this->_order)
            $this->_order = $this->getCurrentOrder();
        $orderItems = $this->_order->getAllVisibleItems();
        return count($orderItems);
    }

    public function getOrderItems($order = null) {
        $this->_order = $order;
        if (!$this->_order)
            $this->_order = $this->getCurrentOrder();
        $orderItems = $this->_order->getAllVisibleItems();
        $itms = $this->getItemsArray($orderItems);
        return json_encode($itms);
    }

}
