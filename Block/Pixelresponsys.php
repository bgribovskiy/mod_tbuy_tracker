<?php

/**
 * Created by PhpStorm.
 * User: nicola
 * Date: 05/12/16
 * Time: 11.51
 */
class Tbuy_Tracker_Block_Pixelresponsys extends Mage_Core_Block_Template
{

    private $lastOrderId;

    public function setLastOrderId($lastOrderId)
    {
        $this->lastOrderId = $lastOrderId;
    }

    protected function _toHtml()
    {
        if (!Mage::helper("tracker/pixelresponsys")->isEnabled() || !Mage::getStoreConfig("tracker/pixel_responsys/enabled_for_successpage")) {
            return "";
        }

        $url = Mage::getStoreConfig("tracker/pixel_responsys/url");
        $params = array();

        $order = Mage::getModel("sales/order")->load($this->lastOrderId);


        if (Mage::getStoreConfigFlag("tracker/pixel_responsys/action")) {
            $params["action"] = "once";
        }

        if (Mage::getStoreConfigFlag("tracker/pixel_responsys/orderid")) {
            $params["orderid"] = $this->lastOrderId;
        }

        if (Mage::getStoreConfigFlag("tracker/pixel_responsys/ordertotal")) {
            $params["ordertotal"] = $order->getGrandTotal();
        }

        if (Mage::getStoreConfigFlag("tracker/pixel_responsys/numitems")) {
            $params["numitems"] = (int)$order->getTotalQtyOrdered();
        }

        if (Mage::getStoreConfigFlag("tracker/pixel_responsys/customerid")) {
            $params["customerid"] = $order->getCustomerId();
        }

        $type = Mage::getStoreConfig("tracker/pixel_responsys/type");
        if (!empty ($type)) {
            $params["type"] = $type;
        }

        if (Mage::getStoreConfigFlag("tracker/pixel_responsys/item")) {
            $arItem = array();
            foreach ($order->getAllVisibleItems() as $item) {
                $arItem[] = $item->getName();
            }
            $params["item"] = implode(",", $arItem);
        }

        if (Mage::getStoreConfigFlag("tracker/pixel_responsys/color")) {
            $arColor = array();
            $attributeInfo = $item->getProductOptionByCode("attributes_info");
            foreach ($attributeInfo as $att) {
                if (strtolower($att["label"]) == "color") {
                    $arColor[] = $att["value"];
                }
            }
            if (count($arColor)) {
                $params["color"] = implode(",", $arColor);
            }
        }
        $url .= (count($params)) ? '?' . http_build_query($params) : "";
        return '<img src="' . $url . '" WIDTH="1" HEIGHT="1">';
    }
}