<?php

class Tbuy_Tracker_Block_Bingyahoo extends Mage_Core_Block_Template {

    protected $_canTrackOrder = false;

    public function _construct() {
        parent::_construct();

        if (!Mage::getStoreConfig("bingyahoo/settings/enabled")) 
            return;

        $this->_canTrackOrder = true;
        $this->getTracker();
    }

    public function canTrackOrder() {
        return $this->_canTrackOrder;
    }

    public function getTracker() {
        $this->dedup = Mage::getStoreConfig("bingyahoo/settings/dedup");
        $this->domainid = Mage::getStoreConfig("bingyahoo/settings/domainid");
        $this->trackerType = Mage::getStoreConfig("bingyahoo/settings/trackerType");
        $this->revenue = Mage::getStoreConfig("bingyahoo/settings/revenue");
        $this->actionid = Mage::getStoreConfig("bingyahoo/settings/actionid");
    }

}
