<?php

class Tbuy_Tracker_Block_Turn extends Mage_Core_Block_Template {

    public function getCacheLifetime() {
        return null;
    }


    public function _toHtml()
    {
		$h = Mage::helper('tracker/turn');
    	if($h->isModuleEnabled()){
    		if($h->getTurnCode())
		    $http = Mage::app()->getStore()->isFrontUrlSecure()?"https":'http';
		    return '<script type="text/javascript" src="'.$http.'://d.turn.com/r/dd/id/'.$h->getTurnCode().'/cat/'.$h->getCurrentCategoryCode().'"> </script>';
	    }

		return '';
    }
}
