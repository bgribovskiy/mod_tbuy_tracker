<?php

class Tbuy_Tracker_Block_Publicidees_Category extends Tbuy_Tracker_Block_Publicidees {

    protected $_template = "tbuy/tracker/publicidees/category.phtml";

    public function getElementId() {
        $elementId = Mage::helper('tracker/publicidees')->getTagSubcategory();
        if (Mage::registry("current_category")->getLevel() <= 2) {
            $elementId = Mage::helper('tracker/publicidees')->getTagCategory();
        }
        return $elementId;
    }
}
