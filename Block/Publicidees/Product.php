<?php

class Tbuy_Tracker_Block_Publicidees_Product extends Tbuy_Tracker_Block_Publicidees {

    protected $_template = "tbuy/tracker/publicidees/product.phtml";

    public function getElementId() {
        return Mage::helper('tracker/publicidees')->getTagProduct();
    }
}
