<?php

class Tbuy_Tracker_Block_Publicidees_Home extends Tbuy_Tracker_Block_Publicidees {

    protected $_template = "tbuy/tracker/publicidees/home.phtml";

    public function getElementId() {
        return Mage::helper('tracker/publicidees')->getTagHome();
    }
}
