<?php

class Tbuy_Tracker_Block_Publicidees_Cart extends Tbuy_Tracker_Block_Publicidees {

    protected $_template = "tbuy/tracker/publicidees/cart.phtml";

    public function getElementId() {
        return Mage::helper('tracker/publicidees')->getTagCart();
    }

    public function getCurrentQuoteId() {
        $cart = Mage::getModel('checkout/cart')->getQuote();
        return $cart->getId();
    }
}
