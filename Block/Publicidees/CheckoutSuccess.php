<?php

class Tbuy_Tracker_Block_Publicidees_CheckoutSuccess extends Tbuy_Tracker_Block_Publicidees {

    protected $_template = "tbuy/tracker/publicidees/checkout_success.phtml";

    public function getElementId() {
        return Mage::helper('tracker/publicidees')->getTagThankyouPage();
    }
}
