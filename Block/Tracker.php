<?php

class Tbuy_Tracker_Block_Tracker extends Mage_Core_Block_Template {

    protected $_canTrackOrder = false;

    /** @var  Tbuy_Tracker_Helper_Data */
    protected $helper;
    /** @var  Mage_Sales_Model_Order */
    protected $order;
    /** @var  int */
    protected $incrementId;
    /** @var  string */
    protected $proto;
    /** @var  string */
    protected $accountid;
    /** @var  float */
    protected $baseGrandTotal;
    /** @var  int */
    protected $countryID;
    /** @var  string */
    protected $isFirstOrder;
    /** @var  Mage_Sales_Model_Order_Address */
    protected $billingAddress;
    /** @var  Mage_Sales_Model_Order_Address */
    protected $shippingAddress;

    protected $paymentMethodCode;

    /** @var  string */
    protected $data3;
    /** @var  int */
    protected $qty;

    public function _construct() {
        parent::_construct();
        $this->helper = Mage::helper("tracker");
        $this->helper->log("BLOCK: received OrderId: {$this->getOrderId()}");
        $this->order = Mage::getModel("sales/order")->load($this->getOrderId());
        if ($this->order->getId() && Mage::getStoreConfig("tracker/advercy/enabled")) {
            $this->_canTrackOrder = true;
            $this->getAdvTracker();
            $this->helper->log("Can Track on OrderId: {$this->order->getId()}");
        }
        else
            $this->helper->log("Can't Track: orderId: {$this->getLastOrderId()}");
    }

    public function canTrackOrder() {
        return $this->_canTrackOrder;
    }

    public function getAdvTracker($mobile = false) {
        if (!$this->canTrackOrder())
            return;

        $this->helper->log("Tracker start on OrderId: {$this->order->getId()}");
        /* @var $this->order Tbuy_CustomerCare_Model_Order */

        /* $order = Mage::getModel("sales/order")
          ->load($this->order->getId()); */

        $this->incrementId = $this->order->getIncrementId();
        $this->proto = @$_SERVER['HTTPS'] != "" ? "https" : "http";
        //$campaign = Mage::getStoreConfig("tracker/adv/campaign");
        $this->accountid = Mage::getStoreConfig('tracker/advercy/accountid');

        if ($this->accountid == "") {
            $this->helper->log("Tracker disabilitato, accountid non configurato", Zend_Log::WARN);
            return "";
        }

        $storeCode = strtoupper(Mage::app()->getStore()->getCode());
        $storeId = Mage::app()->getStore()->getId();
        $currency = Mage::app()->getStore()->getCurrentCurrencyCode();
        $baseCurrency = Mage::app()->getBaseCurrencyCode();

        /*$baseGT = $this->order->getBaseGrandTotal();
        $shipping = ($this->order->getBaseShippingAmount() + $this->order->getBaseShippingTaxAmount()) - $this->order->getBaseShippingDiscountAmount();
        // (base_shipping_amount + base_shipping_tax_amount) - base_shipping_discount_amount
        $this->baseGrandTotal = $baseGT - $shipping - $this->order->getBaseTaxAmount() - $this->order->getBaseCodFee();
         * 
         */

        //  FIXED
        $baseGT = $this->order->getBaseGrandTotal();
        $baseTaxAmount = $this->order->getBaseTaxAmount();
        $baseShippingAmount = $this->order->getBaseShippingAmount();
        $baseCodFee = $this->order->getBaseCodFee();
        $baseShippingDiscountAmount = $this->order->getBaseShippingDiscountAmount();
        
        $this->baseGrandTotal = $baseGT - $baseTaxAmount - $baseShippingAmount - $baseCodFee + $baseShippingDiscountAmount;
        $this->baseGrandTotal = Mage::helper('directory')->currencyConvert($this->baseGrandTotal, $currency, $baseCurrency);

        $this->countryID = $this->order->getBillingAddress()->getData('country_id');

        $payment = $this->order->getPayment(); //->getMethod();
        $paymentMethod = Mage::helper("tracker")->getPaymentProductCode($payment);

        //  Customer FirstOrder/More orders
        $this->isFirstOrder = $this->isFirstOrder($this->order->getCustomerId()) ? "" : Mage::getStoreConfig('tracker/moreordersparams');
        if (Mage::getStoreConfig('tracker/moreorder'))
            $this->incrementId = $this->incrementId . $this->isFirstOrder;


        $paymentCodes = unserialize(Mage::getStoreConfig('tracker/advercy/paymentcode'));
        $this->paymentMethodCode = isset($paymentCodes[$paymentMethod]) ? $paymentCodes[$paymentMethod] : $paymentMethod;
        $this->helper->log("[{$storeCode}|{$storeId}|{$this->accountid}] OrderId: {$this->incrementId}");
        $this->helper->log("[{$storeCode}|{$storeId}|{$this->accountid}] BaseGT: {$baseGT}");
        $this->helper->log("[{$storeCode}|{$storeId}|{$this->accountid}] BaseTaxAmount: {$baseTaxAmount}");
        $this->helper->log("[{$storeCode}|{$storeId}|{$this->accountid}] BaseShippingAmount: {$baseShippingAmount}");
        $this->helper->log("[{$storeCode}|{$storeId}|{$this->accountid}] CodFee: {$baseCodFee}");
        $this->helper->log("[{$storeCode}|{$storeId}|{$this->accountid}] BaseShippingDiscountAmount: {$baseShippingDiscountAmount}");
        $this->helper->log("[{$storeCode}|{$storeId}|{$this->accountid}] TotalCost: {$this->baseGrandTotal}");
        $this->helper->log("[{$storeCode}|{$storeId}|{$this->accountid}] CountryID: {$this->countryID}");
        $this->helper->log("[{$storeCode}|{$storeId}|{$this->accountid}] paymentMethod: {$paymentMethod}");
        $this->helper->log("[{$storeCode}|{$storeId}|{$this->accountid}] paymentMethodCode: {$this->paymentMethodCode}");
        $this->helper->log("[{$storeCode}|{$storeId}|{$this->accountid}] firstOrder?: {$this->isFirstOrder}");

        $this->billingAddress = $this->order->getBillingAddress();
        if ($this->order->getIsVirtual()) {
            $this->shippingAddress = $this->order->getBillingAddress();
        } else {
            $this->shippingAddress = $this->order->getShippingAddress();
        }

        $this->data3 = "";
        if ($mobile)
            $this->data3 = "&data3=Mobile";

        //  Get quantity
        $this->qty = 0;
        foreach ($this->order->getAllVisibleItems() as $item) {
            $this->helper->log("Qty: {$item->getQtyOrdered()}");
            $this->qty += $item->getQtyOrdered();
        }
    }

    private function isFirstOrder($customerId) {
        $orders = Mage::getModel('sales/order')
                ->getCollection()
                ->addFieldToSelect('increment_id')
                ->addFieldToFilter('customer_id', array('eq' => $customerId));
        if ($orders->getSize() == 1) {
            return true;
        }
        return false;
    }

}
