_dla = function (currency, obj) {
    var prodData = {
        'name': obj.name,
        'id': obj.id,
        'price': obj.price,
        'brand': obj.brand,
        'category': obj.category,
        'variant': obj.variant,
        'quantity': (( typeof obj.quantity === "undefined") ? 1 : obj.quantity)
    };
    if (typeof obj.dimension6 === "undefined") {
        prodData = jQuery.extend(prodData, {'dimension6': obj.dimension6});
    }
    if (typeof obj.dimension7 !== "undefined"){
        prodData.dimension7 = obj.dimension7;
    }

    dataLayer.push({
        'event': 'addToCart',
        'ecommerce': {
            'currencyCode': currency,
            'add': {
                'products': [
                    prodData
                ]
            }
        }
    });
};
_dlr = function (currency, obj) {
    var prodData = {
        'name': obj.name,
        'id': obj.id,
        'price': obj.price,
        'brand': obj.brand,
        'category': obj.category,
        'variant': obj.variant,
        'quantity': (( typeof obj.quantity === "undefined") ? 1 : obj.quantity)
    };
    if (typeof obj.dimension6 === "undefined") {
        prodData = jQuery.extend(prodData, {'dimension6': obj.dimension6});
    }
    dataLayer.push({
        'event': 'removeFromCart',
        'ecommerce': {
            'currencyCode': currency,
            'remove': {
                'products': [
                    prodData
                ]
            }
        }
    });
};

dataLayerImpression = function (element, attr, regexp) {
    var url = jQuery(element).attr('href');
    var str = jQuery(element).attr(attr);
    if (!url) return;
    var obj;
    if (str) {
        if (regexp == null) {
            obj = onClick[str];
        } else {
            var mch = str.match(regexp);
            if (mch) {
                obj = onClick[mch[1]];
            }
        }
    }
    if (!obj) {
        document.location = url;
        return;
    }
    dataLayer.push({
        'event': 'productClick',
        'ecommerce': {
            'click': {
                'products': [obj]
            }
        },
        'callback': function () {
            document.location = url;
        }
    });
};
dataLayerSocial = function (name, action, url) {
    dataLayer.push({
        'event': 'social',
        'socialNetwork': name,
        'socialAction': action,
        'socialTarget': url
    });
};

// wishlist

dataLayerAllFromWish = function (currency, qty_selector) {
    var qty, obj;

    for (var key in wishClick) {
        if (qty_selector && qty_selector != '') {
            // mi aspetto sia lo standard
            qty = jQuery('input[name="qty[' + key + ']"]').val();
            qty = parseInt(qty);
        } else {
            qty = 1;
        }
        obj = wishClick[key];
        while (qty--)
            _dla(currency, obj);
    }
};
dataLayerAddFromWish = function (element, currency, attr, regexp, qty_selector) {
    var str = jQuery(element).attr(attr);
    var name, obj;
    var qty = 1;
    if (str) {
        if (regexp == null) {
            obj = wishClick[str];
        }
        else {
            var mch = str.match(regexp);
            if (mch) {
                obj = wishClick[mch[1]];
            }
        }
    }
    if (!obj)
        return;
    if (qty_selector && qty_selector != '') {
        jQuery(qty_selector).each(function () {
            // mi aspetto sia lo standard
            name = jQuery(this).attr('name');
            if (name.match('^qty\\[' + mch[1] + '\\]$')) {
                qty = parseInt(jQuery(this).val());
                return false;
            }
        })
    }
    while (qty--)
        _dla(currency, obj);
};
dataLayerWAdd = function (productObj) {
    // yes, it's whishlist, with an extra "h". we need
    // to replicate the error made server-side.
    dataLayer.push({
        'event': 'whishlist-in',
        'item': productObj.id
    });
};
dataLayerWRemove = function (element) {
    var url = jQuery(element).attr('href');
    var obj;
    if (url) {
        // mi aspetto sia lo standard
        var mch = url.match(/\/remove\/item\/(\d+)\//);
        if (mch) obj = wishClick[mch[1]];
    }
    if (!obj) {
        return;
    }
    dataLayer.push({
        'event': 'whishlist-out',
        'item': obj.id
    });
};

// cart

dataLayerAdd = function (currency, productObj) {
    _dla(currency, productObj);
};
dataLayerRemove = function (element, currency) {
    var url = jQuery(element).attr('href');
    var obj;
    if (url) {
        // mi aspetto sia lo standard
        var mch = url.match(/\/delete\/id\/(\d+)\//);
        if (mch) obj = cartClick[mch[1]];
    }
    if (!obj) {
        return;
    }
    qty = obj.quantity;
    if (qty < 1) {
        return;
    }
    while (qty--)
        _dlr(currency, obj);
};
dataLayerUpdate = function (element, currency, attr, regexp) {
    var qty = parseInt(jQuery(element).val());
    var attr = jQuery(element).attr(attr);
    var diff;
    var obj;
    if (attr) {
        // renderlo configurabile?
        var mch = attr.match(regexp);
        if (mch) obj = cartClick[mch[1]];
    }
    if (!obj) {
        return;
    }
    if (qty == obj.quantity)
        return;
    if (qty > obj.quantity) {
        diff = qty - obj.quantity;
        while (diff--)
            _dla(currency, obj);
    }
    if (qty < obj.quantity) {
        diff = obj.quantity - qty;
        while (diff--)
            _dlr(currency, obj);
    }
};

// checkout

function dataLayerCheckout(action, next) {
    if (dataLayerCheckoutFired) {
        return;
    }
    var dataToPush = {
        'event': 'checkout',
        'ecommerce': {
            'checkout': {
                'actionField': { 'step': 2 },
                'products': dataLayerCO
            }
        },
        'callback': function () {
            document.location = next;
        }
    };
    dataLayer.push(dataToPush);
    dataLayerCheckoutFired = true;
    return true;
}

// social

function dataLayerLike(url) {
    dataLayer.push({
        'event': 'social',
        'socialNetwork': 'facebook',
        'socialAction': 'like',
        'socialTarget': url
    });
}
function dataLayerUnlike(url) {
    dataLayer.push({
        'event': 'social',
        'socialNetwork': 'facebook',
        'socialAction': 'unlike',
        'socialTarget': url
    });
}
function dataLayerTweet() {
    dataLayer.push({
        'event': 'social',
        'socialNetwork': 'twitter',
        'socialAction': 'tweet',
        'socialTarget': document.URL
    });
}
function dataLayerPinIt() {
    dataLayer.push({
        'event': 'social',
        'socialNetwork': 'pinterest',
        'socialAction': 'pin it',
        'socialTarget': document.URL
    });
}
function dataLayerEmail() {
    dataLayer.push({
        'event': 'social',
        'socialNetwork': 'email',
        'socialAction': 'send',
        'socialTarget': document.URL
    });
}
