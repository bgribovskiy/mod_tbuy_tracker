<?php

class Tbuy_Tracker_Model_Nextperformance {

	public function getHomeTracker() {
		if (!Mage::helper("tracker/nextperformance")->isHomeEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/nextperformance', 'tracker_nextperformance', array(
			'template' => 'tbuy/tracker/nextperformance_home.phtml'
		));
		return $block->toHtml();
	}

	public function getProductTracker() {
		if (!Mage::helper("tracker/nextperformance")->isProductEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/nextperformance', 'tracker_nextperformance', array(
			'template' => 'tbuy/tracker/nextperformance_product.phtml'
		));
		return $block->toHtml();
	}

	public function getCategoryTracker() {
		if (!Mage::helper("tracker/nextperformance")->isListingEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/nextperformance', 'tracker_nextperformance', array(
			'template' => 'tbuy/tracker/nextperformance_category.phtml'
		));
		return $block->toHtml();
	}

	public function getSearchResultTracker() {
		if (!Mage::helper("tracker/nextperformance")->isListingEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/nextperformance', 'tracker_nextperformance', array(
			'template' => 'tbuy/tracker/nextperformance_searchresult.phtml'
		));
		return $block->toHtml();
	}

	public function getCartTracker() {
		if (!Mage::helper("tracker/nextperformance")->isBasketEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/nextperformance', 'tracker_nextperformance', array(
			'template' => 'tbuy/tracker/nextperformance_cart.phtml'
		));
		return $block->toHtml();
	}

	public function getCheckoutSuccessTracker() {
		if (!Mage::helper("tracker/nextperformance")->isSuccessPageEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/nextperformance', 'tracker_nextperformance', array(
			'template' => 'tbuy/tracker/nextperformance_checkout_success.phtml'
		));
		return $block->toHtml();
	}

}
