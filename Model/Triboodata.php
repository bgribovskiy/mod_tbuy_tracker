<?php

class Tbuy_Tracker_Model_Triboodata extends Mage_Core_Block_Template {

    public function getHomeTracker() {

		if( !Mage::helper("tracker/triboodata")->isEnabled() || !Mage::getStoreConfig("tracker/triboodata/home_enabled") ) {
			return "";
		}
        $layout = Mage::getSingleton('core/layout');
        $block = $layout->createBlock(
            'tracker/triboodata', 'tracker_triboodata', array(
                'template' => 'tbuy/tracker/triboodata/home.phtml'
            )
        )->toHtml();
        return $block;
    }

	public function getProductTracker(){
		if( !Mage::helper("tracker/triboodata")->isEnabled() || !Mage::getStoreConfig("tracker/triboodata/product_enabled") ) {
			return "";
		}
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
			'tracker/triboodata', 'tracker_triboodata', array(
				'template' => 'tbuy/tracker/triboodata/product.phtml'
			)
		)->toHtml();
		return $block;
	}

	public function getCartTracker(){
		if( !Mage::helper("tracker/triboodata")->isEnabled() || !Mage::getStoreConfig("tracker/triboodata/cart_enabled") ) {
			return "";
		}
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
			'tracker/triboodata', 'tracker_triboodata', array(
				'template' => 'tbuy/tracker/triboodata/cart.phtml'
			)
		)->toHtml();
		return $block;
	}

	public function getCheckoutSuccessTracker(){
		if( !Mage::helper("tracker/triboodata")->isEnabled() || !Mage::getStoreConfig("tracker/triboodata/checkout_success_enabled") ) {
			return "";
		}
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
			'tracker/triboodata', 'tracker_triboodata', array(
				'template' => 'tbuy/tracker/triboodata/checkout_success.phtml'
			)
		)->toHtml();
		return $block;
	}

}