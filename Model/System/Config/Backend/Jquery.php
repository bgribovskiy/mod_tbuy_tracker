<?php

class Tbuy_Tracker_Model_System_Config_Backend_Jquery extends Mage_Core_Model_Config_Data {

    protected function _afterLoad(){
        $columns = unserialize($this->getValue());
        $values  = array();

        if (!is_array($columns))
            $columns = array();

        foreach ($columns as $column => $data) {
            if (!is_array($data))
                continue;

            $_id          = Mage::helper('core')->uniqHash('_');
            $values[$_id] = array(
                'selector'  => $data['selector'],
                'attribute' => @$data['attribute'],
                'regexp'    => @$data['regexp']
            );
        }

        $this->setValue($values);
    }

    protected function _beforeSave(){
        $values = $this->getValue();
        $result = array();

        foreach ($values as $value) {
            if (!is_array($value))
                continue;

            $result[] = array(
                'selector'  => $value['selector'],
                'attribute' => $value['attribute'],
                'regexp'    => $value['regexp']
            );
        }

        $this->setValue(serialize($result));
    }
}
