<?php

class Tbuy_Tracker_Model_System_Config_Backend_Paymentcode extends Mage_Core_Model_Config_Data {

    const GLOBAL_COLLECT_PAYMENT_METHOD = "globalcollect_merchant_link";
    const GLOBAL_COLLECT_PAYMENT_METHOD_HOSTED = "globalcollect_merchant_link_hosted";
    const GESTPAYS2S_PAYMENT_METHOD = "gestpays2s";

    /**
     * Process data after load
     */
    protected function _afterLoad() {
        $value = array();

        $paymentcode = unserialize($this->getValue());
        if (!is_array($paymentcode))
            $paymentcode = array();

        $websiteCode = Mage::app()->getRequest()->getParam('website');
        $website_id = Mage::getModel('core/website')->load($websiteCode)->getId();
        $storeCode = Mage::app()->getRequest()->getParam('store');
        if (empty($storeCode))
            $store_id = Mage::app()->getWebsite($website_id)->getDefaultStore();
        else
            $store_id = Mage::getModel('core/store')->load($storeCode);

        $payments = Mage::getSingleton('payment/config')->getActiveMethods($store_id);
        //$payments = Mage::getSingleton('payment/config')->getAllMethods();

        foreach ($payments as $k => $v) {
            if (!$v)
                continue;

            if ($k == self::GESTPAYS2S_PAYMENT_METHOD) {
                $ccTypes = Mage::getSingleton('payment/config')->getCcTypes();
                foreach ($ccTypes as $cck => $ccType) {
                    $_id = Mage::helper('core')->uniqHash("gp_{$cck}__");
                    $value[$_id] = array(
                        'method_id' => "gp_{$cck}",
                        'method' => "GestPayS2S - {$ccType} [gp_{$cck}]",
                        'code' => isset($paymentcode["gp_{$cck}"]) ? $paymentcode["gp_{$cck}"] : '--'
                    );
                }
                continue;
            }

            //  Global Collect
            if ($k == self::GLOBAL_COLLECT_PAYMENT_METHOD)
                continue;
            if ($k == self::GLOBAL_COLLECT_PAYMENT_METHOD_HOSTED) {
                $gcp = Mage::getSingleton('globalcollect/payment');
                $gpcProducts = $gcp->getPaymentProductCollection();
                $gcpMethods = $gcp->getPaymentMethodCollection();

                foreach ($gcpMethods as $gcpMethod) {
                    $productsByMethod = $gpcProducts->getItemsByColumnValue('payment_method_id', $gcpMethod->getId());
                    if (count($productsByMethod)) {
                        foreach ($productsByMethod as $paymentProduct) {
                            $_id = Mage::helper('core')->uniqHash("gc_{$paymentProduct->getId()}__");
                            $value[$_id] = array(
                                'method_id' => "gc_{$paymentProduct->getId()}",
                                'method' => "Global Collect - {$paymentProduct->getCode()} [gc_{$paymentProduct->getId()}]",
                                'code' => isset($paymentcode["gc_{$paymentProduct->getId()}"]) ? $paymentcode["gc_{$paymentProduct->getId()}"] : '--'
                            );
                        }
                    }
                }
                continue;
            }

            $_id = Mage::helper('core')->uniqHash("{$k}__");
            $value[$_id] = array(
                'method_id' => $k,
                'method' => "{$v->getTitle()} [{$k}]",
                'code' => isset($paymentcode[$k]) ? $paymentcode[$k] : '--'
            );
        }


        $this->setValue($value);
    }

    /**
     * Prepare data before save
     */
    protected function _beforeSave() {
        $values = $this->getValue();
        $paymentcode = array();
        foreach ($values as $k => $v) {
            if (!is_array($v))
                continue;

            $k = explode('__', $k);

            $paymentcode[$k[0]] = $v['code'];
        }

        $value = serialize($paymentcode);
        $this->setValue($value);
    }

}
