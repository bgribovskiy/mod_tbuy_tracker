<?php

class Tbuy_Tracker_Model_System_Config_Backend_Category extends Mage_Core_Model_Config_Data {

    /**
     * Process data after load
     */
    protected function _afterLoad() {
        $attributes = unserialize($this->getValue());
        if (!is_array($attributes))
            $attributes = array();

        $value = array();
        foreach ($attributes as $category => $data) {
            if (!is_array($data))
                continue;

            if (!isset($data["code"]) || $data["code"] == "")
                $data["code"] = "";
            
            $code = $data['code'];

            $_id = Mage::helper('core')->uniqHash('_');
            $value[$_id] = array(
                'category' => $category,
                'code' => $code
            );
        }

        $this->setValue($value);
    }

    /**
     * Prepare data before save
     */
    protected function _beforeSave() {
        $values = $this->getValue();
        $attributes = array();
        foreach ($values as $v) {
            if (!is_array($v))
                continue;
            if (!isset($v["code"]) || $v["code"] == "")
                $v["code"] = array();

            $attributes[$v['category']] = array(
                'code' => $v["code"]
            );
        }

        $value = serialize($attributes);
        $this->setValue($value);
    }

}

//End of class
