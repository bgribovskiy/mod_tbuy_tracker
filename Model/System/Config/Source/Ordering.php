<?php

class Tbuy_Tracker_Model_System_Config_Source_Ordering {

  public function toOptionArray() {
    return array(
      array('value' => 'asc', 'label' => Mage::helper('adminhtml')->__('Ascending')),
      array('value' => 'desc', 'label' => Mage::helper('adminhtml')->__('Descending'))
    );
  }
}
