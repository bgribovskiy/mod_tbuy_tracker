<?php

class Tbuy_Tracker_Model_System_Config_Source_Dataonhead
{

    protected $_data;

    public function toOptionArray()
    {
        if (is_null($this->_data)) {
            $this->_data = array();
            $data = [
                Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_PROD     => "Product Page",
                Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_CATEGORY => "Category Page",
                Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_SEARCH   => "Search Page",
                Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_CHECKOUT => "Checkout",
                Tbuy_Tracker_Helper_Googletagmanager::HEAD_DATA_THANKYOU => "Thank you Page"                
            ];

            foreach ($data as $key => $value) {
                $this->_data[] = ['value' => $key, 'label' => $value];
            }

        }
        return $this->_data;
    }
}
