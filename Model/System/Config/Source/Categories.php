<?php

class Tbuy_Tracker_Model_System_Config_Source_Categories extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

	public function getAllOptions() {
		$values[0]['value'] = "";
		$values[0]['label'] = "-- none ---";
		$values[0]['is_active'] = 0;
		$parent = Mage::helper('utils/category')->load_tree();
		return array_merge($values, $parent);
	}


}

//End of class