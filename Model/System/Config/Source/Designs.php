<?php

class Tbuy_Tracker_Model_System_Config_Source_Designs extends Mage_Core_Model_Design_Source_Design {

    public function toOptionArray() {
        return $this->getAllOptions(false);
    }
}
