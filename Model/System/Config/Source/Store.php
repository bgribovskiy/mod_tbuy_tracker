<?php

class Tbuy_Tracker_Model_System_Config_Source_Store {

    protected $_stores;

    public function toOptionArray() {
        if (is_null($this->_stores)) {
            $this->_stores = array();
            $this->_stores[''] = '-- choose a store --';

            foreach (Mage::app()->getStores() as $store)
                $this->_stores[$store->getId()] = $store->getName();
        }

        return $this->_stores;
    }
}
