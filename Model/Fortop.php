<?php

class Tbuy_Tracker_Model_Fortop extends Mage_Core_Block_Template {

	/**
	 * @return string
     */
	public function getCheckoutSuccessTracker() {
		if (!Mage::helper("tracker/fortop")->isEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/fortop', 'tracker_fortop', array(
			'template' => 'tbuy/tracker/fortop/success.phtml'
		));
		return $block->toHtml();
	}

}
