<?php

class Tbuy_Tracker_Model_Turn extends Mage_Core_Block_Template {

    public function getHomeTracker() {
	    if( !Mage::helper("tracker/turn")->isEnabled() ) {
		    return "";
	    }
	    $layout = Mage::getSingleton('core/layout');
	    $block = $layout->createBlock(
		    'tracker/turn', 'tracker_turn'
	    );

	    return $block->toHtml();

    }

	public function getProductTracker(){
		return '';
	}

	public function getCartTracker(){
		return '';
	}

	public function getCategoryTracker(){
		if( !Mage::helper("tracker/turn")->isEnabled() ) {
			return "";
		}
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
			'tracker/turn', 'tracker_turn'
		);

		return $block->toHtml();
	}


	public function getCheckoutSuccessTracker(){
		return '';
	}

}
