<?php

class Tbuy_Tracker_Model_Publicidees extends Mage_Core_Block_Template {

    public function getHomeTracker() {
        if (!Mage::getStoreConfig('publicidees/settings/enabled'))
            return "";
        $layout = Mage::getSingleton('core/layout');
        $block = $layout->createBlock(
            'tracker/publicidees_home', 'tracker_publicidees', array(
                'template' => 'tbuy/tracker/publicidees/home.phtml'
            )
        );
        return $block->toHtml();
    }

    public function getCategoryTracker() {
        if (!Mage::getStoreConfig('publicidees/settings/enabled'))
            return "";
        $layout = Mage::getSingleton('core/layout');
        $block = $layout->createBlock(
            'tracker/publicidees_category', 'tracker_publicidees', array(
                'template' => 'tbuy/tracker/publicidees/category.phtml'
            )
        );
        return $block->toHtml();
    }

    public function getProductTracker() {
        if (!Mage::getStoreConfig('publicidees/settings/enabled'))
            return "";
        $layout = Mage::getSingleton('core/layout');
        $block = $layout->createBlock(
            'tracker/publicidees_product', 'tracker_publicidees', array(
                'template' => 'tbuy/tracker/publicidees/product.phtml'
            )
        );
        return $block->toHtml();
    }

    public function getCartTracker() {
        if (!Mage::getStoreConfig('publicidees/settings/enabled'))
            return "";
        $layout = Mage::getSingleton('core/layout');
        $block = $layout->createBlock(
            'tracker/publicidees_cart', 'tracker_publicidees', array(
                'template' => 'tbuy/tracker/publicidees/cart.phtml'
            )
        );
        return $block->toHtml();
    }

    public function getCheckoutSuccessTracker() {
        if (!Mage::getStoreConfig('publicidees/settings/enabled'))
            return "";
        $layout = Mage::getSingleton('core/layout');
        $block = $layout->createBlock(
            'tracker/publicidees_checkoutSuccess', 'tracker_publicidees', array(
                'template' => 'tbuy/tracker/publicidees/checkout_success.phtml'
            )
        );
        return $block->toHtml();
    }

}
