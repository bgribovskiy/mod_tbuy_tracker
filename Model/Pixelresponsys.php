<?php
/**
 * Created by PhpStorm.
 * User: nicola
 * Date: 05/12/16
 * Time: 11.46
 */


class Tbuy_Tracker_Model_Pixelresponsys extends Mage_Core_Block_Template {

    public function getCheckoutSuccessTracker($lastOrderId){
        if( !Mage::helper("tracker/pixelresponsys")->isEnabled() || !Mage::getStoreConfig("tracker/pixel_responsys/enabled_for_successpage") ) {
            return "";
        }
        $layout = Mage::getSingleton('core/layout');
        $block = $layout->createBlock(
            'tracker/pixelresponsys', 'tracker_pixelresponsys', array(
                //'template' => 'tbuy/tracker/triboodata/checkout_success.phtml'
            )
        );
        $block->setLastOrderId($lastOrderId);
        return $block;
    }
}