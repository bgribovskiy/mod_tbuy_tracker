<?php

class Tbuy_Tracker_Model_Yahoo {

	public function getHomeTracker() {
		if (!Mage::helper("tracker/yahoo")->isHomeEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/yahoo', 'tracker_yahoo', array(
			'template' => 'tbuy/tracker/yahoo_home.phtml'
		));
		return $block->toHtml();
	}

	public function getProductTracker() {
		if (!Mage::helper("tracker/yahoo")->isProductEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/yahoo', 'tracker_yahoo', array(
			'template' => 'tbuy/tracker/yahoo_product.phtml'
		));
		return $block->toHtml();
	}

	public function getCategoryTracker() {
		if (!Mage::helper("tracker/yahoo")->isListingEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/yahoo', 'tracker_yahoo', array(
			'template' => 'tbuy/tracker/yahoo_category.phtml'
		));
		return $block->toHtml();
	}

	public function getSearchResultTracker() {
		if (!Mage::helper("tracker/yahoo")->isListingEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/yahoo', 'tracker_yahoo', array(
			'template' => 'tbuy/tracker/yahoo_searchresult.phtml'
		));
		return $block->toHtml();
	}

	public function getCartTracker() {
		if (!Mage::helper("tracker/yahoo")->isBasketEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/yahoo', 'tracker_yahoo', array(
			'template' => 'tbuy/tracker/yahoo_cart.phtml'
		));
		return $block->toHtml();
	}
        
	public function getWishlistTracker() {
		if (!Mage::helper("tracker/yahoo")->isWishlistEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/yahoo', 'tracker_yahoo', array(
			'template' => 'tbuy/tracker/yahoo_wishlist.phtml'
		));
		return $block->toHtml();
	}

	public function getCheckoutSuccessTracker() {
		if (!Mage::helper("tracker/yahoo")->isSuccessPageEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/yahoo', 'tracker_yahoo', array(
			'template' => 'tbuy/tracker/yahoo_checkout_success.phtml'
		));
		return $block->toHtml();
	}

}
