<?php

class Tbuy_Tracker_Model_Universaltag extends Mage_Core_Block_Template {

	public function getProductTracker() {
		if (!Mage::helper("tracker/universaltag")->isEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/universaltag', 'tracker_universaltag', array(
			'template' => 'tbuy/tracker/universaltag/product.phtml'
		));
		$layout->getBlock('after_body_start')->append($block);
	}

	public function getCartTracker() {
		if (!Mage::helper("tracker/universaltag")->isEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/universaltag', 'tracker_universaltag', array(
			'template' => 'tbuy/tracker/universaltag/cart.phtml'
		));
		$layout->getBlock('after_body_start')->append($block);
	}
        
	public function getOnestepchecoutTracker() {
		if (!Mage::helper("tracker/universaltag")->isEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/universaltag', 'tracker_universaltag', array(
			'template' => 'tbuy/tracker/universaltag/onestepcheckout.phtml'
		));
                $layout->getBlock('after_body_start')->append($block);
	}

	public function getCheckoutSuccessTracker() {
		if (!Mage::helper("tracker/universaltag")->isEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/universaltag', 'tracker_universaltag', array(
			'template' => 'tbuy/tracker/universaltag/success.phtml'
		));
		$layout->getBlock('after_body_start')->append($block);
	}

}
