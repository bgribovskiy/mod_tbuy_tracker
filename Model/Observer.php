<?php

class Tbuy_Tracker_Model_Observer {

    public function setTrackers($observer = null) {
        $event = $observer->getEvent();
        $this->helper = Mage::helper("tracker");
        if (!$event->hasData("order_ids")) {
            $this->lastOrderId = Mage::getSingleton('checkout/session')->getLastOrderId();
        } else {
            $orderIds = $event->getOrderIds();
            $this->lastOrderId = $orderIds[0];
        }

        $this->setAdvTracker($observer);
        $this->setTradeDoublerTracker($observer);
        $this->setFacebookTracker($observer);
        $this->setPixelresponsysTracker($observer);
        //$this->setGoogleAdsTracker($observer);
    }

    public function setAdvTracker($observer = null) {
        $layout = Mage::getSingleton('core/layout');
        $layout->getUpdate()->addHandle("tbuy_tracker");

        $block = $layout->createBlock(
            'Tbuy_Tracker_Block_Tracker', 'tracker', array(
                'template' => 'tbuy/tracker/tracker.phtml',
                'order_id' => $this->lastOrderId
            )
        );
        //$block->setOrderId($lastOrderId);

        $data3 = "";
        if (Mage::getConfig()->getModuleConfig("TinyBrick_LightSpeed")->is('active', 'true'))
            if (TinyBrick_LightSpeed_Block_Page_Html::is_mobile())
                $data3 = "&data3=Mobile";
        $block->setData("data3", $data3);

        $layout->getBlock('content')->append($block);
    }

    public function setTradeDoublerTracker($observer = null) {
        if (!Mage::getStoreConfig("tradedoubler/settings/enabled"))
            return;
        $layout = Mage::getSingleton('core/layout');
        $layout->getUpdate()->addHandle("tbuy_tracker_tradedoubler");

        $block = $layout->createBlock(
            'Tbuy_Tracker_Block_Tradedoubler', 'tracker_td', array(
                'template' => 'tbuy/tracker/tradedoubler.phtml',
                'order_id' => $this->lastOrderId
            )
        );
        $layout->getBlock('content')->append($block);
    }

    public function setFacebookTracker($observer = null) {
        if (!Mage::getStoreConfig("facebook/settings/enabled"))
            return;
        $layout = Mage::getSingleton('core/layout');
        $layout->getUpdate()->addHandle("tbuy_tracker_facebook");

        $block = $layout->createBlock(
            'Tbuy_Tracker_Block_Facebook', 'tracker_fb', array(
                'template' => 'tbuy/tracker/facebook.phtml',
                'order_id' => $this->lastOrderId
            )
        );
        $layout->getBlock('head')->append($block);
    }

    public function setPixelresponsysTracker($observer = null){
        $addFooterBlock = Mage::getModel("tracker/pixelresponsys")->getCheckoutSuccessTracker($this->lastOrderId);
        if (!empty($addFooterBlock)) {
            $layout = Mage::getSingleton('core/layout');
            if (!$layout)
                return $this;
            $content = $layout->getBlock('content');
            if (!$content)
                return $this;
            $content->append($addFooterBlock);
        }
    }

    public function setSubscriberSessionForGTM($observer) {
        $event = $observer->getEvent();
        $subscriber = $event->getSubscriber();
        $lifetime = 300;
        Mage::getModel('core/cookie')->set("GTMSubscriberId", $subscriber->getId(), $lifetime);
    }

    public function setSubscriberSession($observer) {
        if (!Mage::getStoreConfig("tracker/advercy/newsletter/enabled"))
            return;
        $event = $observer->getEvent();
        $subscriber = $event->getSubscriber();
        $countryCode = Mage::getStoreConfig('general/country/default');
        // fix per unilever: la sessione nel template non era riconosciuta. Workaround utilizzando i cookie
        // cookie lifetime = 15min
        $lifetime = 900;
        Mage::getModel('core/cookie')->set("TrackerSubscriberId", $subscriber->getId(), $lifetime);
        Mage::getModel('core/cookie')->set("TrackerSubscriberEmail", $subscriber->getEmail(), $lifetime);
        Mage::getModel('core/cookie')->set("TrackerSubscriberCountry", $countryCode, $lifetime);
        Mage::getModel('core/cookie')->set("TrackerSubscriberCluster", $subscriber->getCluster(), $lifetime);
        Mage::getSingleton('core/session')->setTrackerSubscriberId($subscriber->getId());
        Mage::getSingleton('core/session')->setTrackerSubscriberEmail($subscriber->getEmail());
        Mage::getSingleton('core/session')->setTrackerSubscriberCountry($countryCode);
        if (Mage::getStoreConfig("tracker/advercy/newsletter/log")) {
            $remote_addr = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "N/A";
            Mage::log("[{$remote_addr}] setSubscriberSession: {$subscriber->getCluster()}|{$subscriber->getId()}|{$subscriber->getEmail()}|{$countryCode}", Zend_Log::DEBUG, "nl_cluster.log");
        }
    }

    public function addTrackerJs($observer) {

        $path = Mage::helper('tracker')->getUrlPath();

        $addJs = "";
        //$addAdFormJs = "";
        $addFooterBlocks = array();

        //  Add in all pages
        if (Mage::getDesign()->getArea() != 'adminhtml') {
            $addFooterBlocks[] = Mage::getModel("tracker/yandex")->getGlobalTracker();
            $addFooterBlocks[] = Mage::getModel("tracker/hubspot")->getGlobalTracker();
            $addJs .= Mage::getModel("tracker/acquisio")->getGlobalTracker();
            $addJs .= Mage::getModel("tracker/facebook")->getGlobalTracker();
            $addJs .= Mage::getModel("tracker/adform")->getGlobalTracker();
            $addJs .= Mage::getModel("tracker/facebookpixel")->getGlobalTracker();
	        switch ($path) {
		        case 'catalog_category_view':
			        $addJs .= Mage::getModel("tracker/turn")->getCategoryTracker();
			        break;
	        }

        }
        switch ($path) {
            case 'cms_index_index':
                $addJs .= $this->_getHomeTracker();
                $addJs .= Mage::getModel("tracker/nextperformance")->getHomeTracker();
                $addJs .= Mage::getModel("tracker/yahoo")->getHomeTracker();
	            $addJs .= Mage::getModel("tracker/turn")->getHomeTracker();
                //$addJs .= Mage::getModel("tracker/adform")->getGlobalTracker();
                $addJs .= Mage::getModel("tracker/publicidees")->getHomeTracker();
				$addJs .= Mage::getModel("tracker/triboodata")->getHomeTracker();
                break;
            case 'wishlist_index_index':
                $addJs .= Mage::getModel("tracker/yahoo")->getWishlistTracker();
                $addJs .= Mage::getModel("tracker/facebookpixel")->getWishlistTracker();
                break;
            case 'cms_index_noRoute':
                $addJs .= $this->_getHomeTracker();
                $addJs .= Mage::getModel("tracker/nextperformance")->getHomeTracker();
                $addJs .= Mage::getModel("tracker/yahoo")->getHomeTracker();
                //$addJs .= Mage::getModel("tracker/adform")->getGlobalTracker();
                $addJs .= Mage::getModel("tracker/publicidees")->getHomeTracker();
                break;
            case 'catalog_category_view':
                $addJs .= $this->_getCategoryTracker();
                $addJs .= Mage::getModel("tracker/nextperformance")->getCategoryTracker();
                $addJs .= Mage::getModel("tracker/yahoo")->getCategoryTracker();
                //$addJs .= Mage::getModel("tracker/adform")->getGlobalTracker();
                $addJs .= Mage::getModel("tracker/publicidees")->getCategoryTracker();
                break;
            case 'catalogsearch_result_index':
                $addJs .= Mage::getModel("tracker/facebookpixel")->getSearchTracker();
                $addJs .= $this->_getSearchresultTracker();
                break;
            case 'catalog_product_view':
                $addJs .= $this->_getProductTracker();
                $addJs .= Mage::getModel("tracker/nextperformance")->getProductTracker();
                $addJs .= Mage::getModel("tracker/yahoo")->getProductTracker();
                //$addJs .= Mage::getModel("tracker/adform")->getGlobalTracker();
                $addJs .= Mage::getModel("tracker/universaltag")->getProductTracker();
                $addJs .= Mage::getModel("tracker/publicidees")->getProductTracker();
				$addJs .= Mage::getModel("tracker/triboodata")->getProductTracker();
                break;
            case 'checkout_cart_index':
                $addJs .= $this->_getCartTracker();
                $addJs .= Mage::getModel("tracker/nextperformance")->getCartTracker();
                $addJs .= Mage::getModel("tracker/yahoo")->getCartTracker();
                $addJs .= Mage::getModel("tracker/adform")->getCartTracker();
                $addJs .= Mage::getModel("tracker/facebookpixel")->getCartTracker();
                $addJs .= Mage::getModel("tracker/universaltag")->getCartTracker();
                $addJs .= Mage::getModel("tracker/publicidees")->getCartTracker();
				$addJs .= Mage::getModel("tracker/triboodata")->getCartTracker();
                break;
            case 'checkout_onepage_success':
                $addJs .= $this->_getCheckoutSuccessTracker();
                $addJs .= Mage::getModel("tracker/nextperformance")->getCheckoutSuccessTracker();
                $addJs .= Mage::getModel("tracker/yahoo")->getCheckoutSuccessTracker();
                $addJs .= Mage::getModel("tracker/adform")->getCheckoutSuccessTracker();
                $addJs .= Mage::getModel("tracker/fortop")->getCheckoutSuccessTracker();
                $addJs .= Mage::getModel("tracker/facebookpixel")->getCheckoutSuccessTracker();
                $addJs .= Mage::getModel("tracker/universaltag")->getCheckoutSuccessTracker();
                $addJs .= Mage::getModel("tracker/acquisio")->getCheckoutSuccessTracker();
                $addJs .= Mage::getModel("tracker/publicidees")->getCheckoutSuccessTracker();
				$addJs .= Mage::getModel("tracker/triboodata")->getCheckoutSuccessTracker();
                break;
            case 'onestepcheckout_index_index':
                $addJs .= Mage::getModel("tracker/universaltag")->getOnestepchecoutTracker();
                $addJs .= Mage::getModel("tracker/adform")->getOnestepchekoutTracker();
                $addJs .= Mage::getModel("tracker/facebookpixel")->getOnestepchekoutTracker();
                break;
        }
        //$addJs .= $addAdFormJs;
        if (!empty($addJs)) {
            $layout = Mage::getSingleton('core/layout');
            if (!$layout)
                return $this;
            $headBlock = $layout->getBlock('head');
            if (!$headBlock)
                return $this;
            $includes = $headBlock->getIncludes();
            $includes .= $addJs;
            $layout->getBlock('head')->setIncludes($includes);
        }

        if (!empty($addFooterBlocks)) {
            $layout = Mage::getSingleton('core/layout');
            if (!$layout)
                return $this;
            $footBlock = $layout->getBlock('before_body_end');
            if (!$footBlock)
                return $this;
            foreach ($addFooterBlocks as $block) {
                $footBlock->append($block);
            }
        }

        return $this;
    }

    protected function _getHomeTracker() {
        $helper = Mage::helper('tracker');
        $homeJs = '';
        if ($helper->isCriteoHomeEnabled(Mage::app()->getStore()->getId())) {
            $homeJs .= Mage::getModel('tracker/criteo')->getHomeTracker();
        }
        return empty($homeJs) ? false : $homeJs;
    }

    protected function _getProductTracker() {
        $helper = Mage::helper('tracker');
        $productJs = '';
        if ($helper->isCriteoEnabled(Mage::app()->getStore()->getId())) {
            $productJs .= Mage::getModel('tracker/criteo')->getProductTracker();
        }
        return empty($productJs) ? false : $productJs;
    }

    protected function _getCategoryTracker() {
        $helper = Mage::helper('tracker');
        $productJs = '';
        if ($helper->isCriteoListingEnabled(Mage::app()->getStore()->getId())) {
            $productJs .= Mage::getModel('tracker/criteo')->getCategoryTracker();
        }
        return empty($productJs) ? false : $productJs;
    }

    protected function _getSearchresultTracker() {
        $helper = Mage::helper('tracker');
        $productJs = '';
        if ($helper->isCriteoListingEnabled(Mage::app()->getStore()->getId())) {
            $productJs .= Mage::getModel('tracker/criteo')->getSearchResultTracker();
        }
        return empty($productJs) ? false : $productJs;
    }

    protected function _getCartTracker() {
        $helper = Mage::helper('tracker');
        $productJs = '';
        if ($helper->isCriteoBasketEnabled(Mage::app()->getStore()->getId())) {
            $productJs .= Mage::getModel('tracker/criteo')->getCartTracker();
        }
        return empty($productJs) ? false : $productJs;
    }

    protected function _getCheckoutSuccessTracker() {
        $helper = Mage::helper('tracker');
        $productJs = '';
        if ($helper->isCriteoEnabled(Mage::app()->getStore()->getId())) {
            $productJs .= Mage::getModel('tracker/criteo')->getCheckoutSuccessTracker();
        }
        return empty($productJs) ? false : $productJs;
    }

    /**
     * @param $observer Varien_Event_Observer
     */
    public function googletagmanagerRelatedImpressions($observer) {
        if (!Mage::getStoreConfig('tracker/google_tag_manager/enabled'))
            return;

        // http://www.atwix.com/magento/insert-blocks/
        $supported = array(
            'cartupsells/checkout_cart_upsells',
            'catalog/product_list_crosssell',
            'catalog/product_list_related',
            'catalog/product_list_bestsellers',
            'upsellcartrelated/related',
            'looks/completeLook',
            'utils/widget_Showcase'
        );
        $transport = $observer->getTransport();
        $block = $observer->getBlock();
        $type = $block->getType();
        $array = array();
        $list_type = "";

        if (!in_array($type, $supported))
            return;

        switch ($type) {
            case 'upsellcartrelated/related':
                $count = (int)Mage::getStoreConfig('upsellcartrelated/general/maxrelatedtoshow');
                $collection = $block->getItems();
                break;
            case 'cartupsells/checkout_cart_upsells':
                // caso strano:. lo stesso blocco viene renderizzato due volte con due template
                // diversi. dobbiamo ignorare uno dei due se no duplichiamo le impressions
                if ($block->getTemplate() == 'tbuy/cartupsells/list-message.phtml')
                    return;
                $count = (int)Mage::getStoreConfig('cartupsells/view/max');
                $collection = $block->getCartUpsellsProducts($count);
                break;
            case 'catalog/product_list_related':
                // occhio che di solito il numero di correlati è inchiodato dentro il template,
                // per cui ci tocca avere un parametro di configurazione da backent (a meno che
                // non sia in uso tbuy/utils, allora peschiamo la sua di configurazione)
                if (Mage::getStoreConfig('utils_section/related_products/enable')) {
                    $count = (int)Mage::getStoreConfig('utils_section/related_products/limit');
                    $collection = $block->getRelatedProductsCollection();
                    break;
                }

                // app/design/frontend/benetton/default/template/catalog/product/list/related.phtml
                $count = (int)Mage::getStoreConfig('tracker/google_tag_manager/max_related_products');
                $collection = $block->getItems();
                break;
            case 'looks/completeLook':
                $count = 0;
                $collection = [];
                if (!Mage::getStoreConfig("tbuy_looks/complete_look/enabled")) {
                    break;
                }
                $collection = $block->getSaleableItems();
                if(is_object($collection)) {
                    $count = (int) $collection->getSize();
                }
                $list_type = "look";
                break;
            case 'catalog/product_list_bestsellers':
                $collection = $block->getLoadedProductCollection();
                // FIXME magic number
                $count = 2;
                break;
            case 'utils/widget_Showcase':
                $_skus = $block->getProductSkus();
                $collection = [];
                foreach ($_skus as $inner_array){
                    if (empty($inner_array)) {
                        continue;
                    }
                    $_prod = Mage::helper('catalog/product')->getProduct(reset($inner_array), Mage::app()->getStore()->getId(), 'sku');
                    $collection[] = $_prod;
                }
                $count = count($collection);
                break;
            default:
                $collection = $block->getItems();
                // questo valore è stato inchiodato nei seguenti template:
                // app/design/frontend/benetton/default/template/tbuy/upsellcartrelated/default.phtml
                $count = 4;
        }

        if (count($collection) == 0 || $count < 1)
            return;

        // XXX se convertiamo $collection in array ci va in bomba tutto il resto
        foreach ($collection as $product) {
            if ($count-- == 0) break;
            $array[] = $product;
        }

        $old_html = $transport->getHtml();
        $layout = Mage::getSingleton('core/layout');
        // http://magento-quickies.alanstorm.com/post/84834105880
        $new_html = $layout->createBlock('tracker/googletagmanager_relatedimpressions')
            ->setData('collection', $array)
            ->setData('push',true)
            ->setData('list_type',$list_type)
            ->toHtml();

        $transport->setHtml($old_html . $new_html);
    }

    public function googletagmanagerTrackFacebook($observer) {
        if (!Mage::getStoreConfig('tracker/google_tag_manager/enabled'))
            return;

        $object = $observer->getEvent()->getFbEvents();
        $events = $object->getData('fb_events');

        $events['edge.create'][] = 'dataLayerLike(url);';
        $events['edge.remove'][] = 'dataLayerUnlike(url);';

        $object->setData('fb_events', $events);
        $observer->getEvent()->setFbEvents($object);
    }

    public function googletagmanagerTrackTwitter($observer) {
        if (!Mage::getStoreConfig('tracker/google_tag_manager/enabled'))
            return;

        $object = $observer->getEvent()->getObjEvents();
        $events = $object->getData('obj_events');

        $events['tweet'][] = 'dataLayerTweet();';

        $object->setData('obj_events', $events);
        $observer->getEvent()->setObjEvents($object);
    }

    public function googletagmanagerTrackPinterest($observer) {
        if (!Mage::getStoreConfig('tracker/google_tag_manager/enabled'))
            return;

        $object = $observer->getEvent()->getObjEvents();
        $events = $object->getData('obj_events');

        $events['pin it'][] = 'dataLayerPinIt();';

        $object->setData('obj_events', $events);
        $observer->getEvent()->setObjEvents($object);
    }

    public function googletagmanagerTrackEmail($observer) {
        if (!Mage::getStoreConfig('tracker/google_tag_manager/enabled'))
            return;

        $object = $observer->getEvent()->getObjEvents();
        $events = $object->getData('obj_events');

        $events['send'][] = 'dataLayerEmail();';

        $object->setData('obj_events', $events);
        $observer->getEvent()->setObjEvents($object);
    }

    public function googletagmanagerAddedToCart($observer) {
        // nel caso dei "look" di sisley, possiamo aggiungere più di un prodotto per volta,
        // il che significa che questo evento viene chiamato per ciascun prodotto
        $quoteItem = $observer->getQuoteItem();
        $product   = $quoteItem->getProduct();

        $products = Mage::getModel('core/session')->getProductsToShoppingCart();
        if ($products)
            $ids = $products->getIds();
        else
            $ids = array();

        $ids[]    = $product->getId();
        $products = new Varien_Object(array('ids' => $ids));
        Mage::getModel('core/session')->setProductsToShoppingCart($products);
    }

    /**
     * Added client browser and Os to sales flat order table
     *
     * @observes sales_order_save_before
     * @param Varien_Event_Observer $observer
     */
    public function logCustomerBrowserAndOs(Varien_Event_Observer $observer)
    {
        $httpUserAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
        $browserName = 'Unknown';
        $platform = 'Unknown';
        $ub= "";

        $os_array = array(
            '/windows nt 10/i'      =>  'Windows 10',
            '/windows nt 6.3/i'     =>  'Windows 8.1',
            '/windows nt 6.2/i'     =>  'Windows 8',
            '/windows nt 6.1/i'     =>  'Windows 7',
            '/windows nt 6.0/i'     =>  'Windows Vista',
            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
            '/windows nt 5.1/i'     =>  'Windows XP',
            '/windows xp/i'         =>  'Windows XP',
            '/windows nt 5.0/i'     =>  'Windows 2000',
            '/windows me/i'         =>  'Windows ME',
            '/win98/i'              =>  'Windows 98',
            '/win95/i'              =>  'Windows 95',
            '/win16/i'              =>  'Windows 3.11',
            '/macintosh|mac os x/i' =>  'Mac OS X',
            '/mac_powerpc/i'        =>  'Mac OS 9',
            '/linux/i'              =>  'Linux',
            '/ubuntu/i'             =>  'Ubuntu',
            '/iphone/i'             =>  'iPhone',
            '/ipod/i'               =>  'iPod',
            '/ipad/i'               =>  'iPad',
            '/android/i'            =>  'Android',
            '/blackberry/i'         =>  'BlackBerry',
            '/webos/i'              =>  'Mobile'
        );

        foreach ($os_array as $regex => $value) {
            if (preg_match($regex, $httpUserAgent)) {
                $platform = $value;
            }
        }

        // Next get the name of the useragent yes seperately and for good reason
        if(preg_match('/MSIE/i',$httpUserAgent) && !preg_match('/Opera/i',$httpUserAgent)) {
            $browserName = 'Internet Explorer';
            $ub = "MSIE";
        } elseif(preg_match('/Firefox/i',$httpUserAgent)) {
            $browserName = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif(preg_match('/Chrome/i',$httpUserAgent)) {
            $browserName = 'Google Chrome';
            $ub = "Chrome";
        } elseif(preg_match('/Safari/i',$httpUserAgent)) {
            $browserName = 'Apple Safari';
            $ub = "Safari";
        } elseif(preg_match('/Opera/i',$httpUserAgent)) {
            $browserName = 'Opera';
            $ub = "Opera";
        } elseif(preg_match('/Netscape/i',$httpUserAgent)) {
            $browserName = 'Netscape';
            $ub = "Netscape";
        }

        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        preg_match_all($pattern, $httpUserAgent, $matches);

        if (count($matches['browser']) != 1) {
            if (strripos($httpUserAgent,"Version") < strripos($httpUserAgent,$ub)){
                $version = $matches['version'][0];
            } else {
                $version = isset($matches['version']) && isset($matches['version'][1]) ? $matches['version'][1] : "";
            }
        } else {
            $version = $matches['version'][0];
        }

        if ($version==null || $version=="") {
            $version="?";
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();
        $order->setData('customer_browser', $browserName . " " . $version);
        $order->setData('customer_os', $platform);

    }

    /**
     * @param $observer
     * @return mixed
     */
    public function setCartProductSession($observer){
        $productId = $observer->getEvent()->getProduct()->getId();
        $itemJstInCart = Mage::getSingleton("checkout/session")->getItemJustInCart();
        if(!is_array($itemJstInCart)){
            $itemJstInCart = array();
        }
        if(!in_array($productId,$itemJstInCart)){
            $itemJstInWishlist[] = $productId;
            Mage::getSingleton("checkout/session")->setItemJustInCart($itemJstInCart);
            Mage::getSingleton("checkout/session")->setCartTrackFlag(true);
        }

        return $observer;
    }

    /**
     * @param $oserver
     * @return mixed
     */
    public function setWishlistProductSession($oserver){
        $productId = $oserver->getEvent()->getProduct()->getId();
        $itemJstInWishlist = Mage::getSingleton("checkout/session")->getItemJustInWishlist();
        if(!is_array($itemJstInWishlist)){
            $itemJstInWishlist = array();
        }
        if(!in_array($productId,$itemJstInWishlist)){
            $itemJstInWishlist[] = $productId;
            Mage::getSingleton("checkout/session")->setItemJustInWishlist($itemJstInWishlist);
            Mage::getSingleton("checkout/session")->setWishlistTrackFlag(true);
        }

        return $oserver;
    }

    /**
     * Customer registration logging in the HubSpot
     * @param Varien_Event_Observer $observer
     * @return mixed
     */
    public function customerRegisterSuccess($observer) {
        if (Mage::helper("tracker/hubspot")->isEnabledRegistrationCheck()) {
            $this->sendHubSpotData($observer);
        }

        return $observer;
    }

    /**
     * Customer login logging in the HubSpot
     * @param Varien_Event_Observer $observer
     * @return mixed
     */
    public function customerLogin($observer){
        if (Mage::helper("tracker/hubspot")->isEnabledLoginCheck()) {
            $this->sendHubSpotData($observer);
        }

        return $observer;
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    protected function sendHubSpotData($observer) {
        $event = $observer->getEvent();
        $customer = $event->getCustomer();
        Mage::getModel("tracker/hubspot")->sendHubSpotData($customer);
    }
}
