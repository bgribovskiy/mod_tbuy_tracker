<?php
/**
 * Created by PhpStorm.
 * User: nicola
 * Date: 30/01/17
 * Time: 12.28
 */

class Tbuy_Tracker_Model_Hubspot extends Mage_Core_Block_Template {

    public function getGlobalTracker() {
        if (!Mage::helper("tracker/hubspot")->isEnabled())
            return "";
        $layout = Mage::getSingleton('core/layout');
        $block = $layout->createBlock(
            'tracker/hubspot', 'tracker_hubspot', array(
                'template' => 'tbuy/tracker/hubspot/global.phtml'
            )
        );
        return $block;
    }


    /**
     * @param Mage_Customer_Model_Customer $customer
     * @param string $url
     */
    public function sendHubSpotData($customer) {
        $portalId = Mage::helper("tracker/hubspot")->getTrackingCode();
        $formGuid = Mage::helper("tracker/hubspot")->getFormGuid();
        if ($portalId && $formGuid) {
            $url = sprintf('https://forms.hubspot.com/uploads/form/v2/%s/%s', $portalId, $formGuid);
            $hubspotutk = Mage::getModel('core/cookie')->get('hubspotutk');
            $pageUrl = Mage::helper('core/url')->getCurrentUrl();
            $hsContext = array(
                'pageUrl' => $pageUrl,
                'hutk' => $hubspotutk,
            );

            $strPost = array(
                'firstname' =>  $customer->getFirstname(),
                'lastname' => $customer->getLastname(),
                'email' => $customer->getEmail(),
                'hs_context' => json_encode($hsContext)
            );

            if ($url) {
                $client = new Zend_Http_Client($url);
                $client->setParameterPost($strPost);
                $client->setHeaders('Content-Type', 'application/x-www-form-urlencoded');
                $client->request(Zend_Http_Client::POST);
            }
        }
    }

}