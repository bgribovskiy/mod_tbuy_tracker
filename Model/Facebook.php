<?php

class Tbuy_Tracker_Model_Facebook extends Mage_Core_Block_Template {

	public function getGlobalTracker() {

		if (!Mage::getStoreConfig("facebook/settings/enabled")) {
			return false;
		}

		if (!Mage::getStoreConfig("facebook/settings/enabled_global_tracker")) {
			return false;
		}

		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/facebook', 'tracker_facebook_global', array(
			'template' => 'tbuy/tracker/facebookglobal.phtml'
		));
		return $block->toHtml();
	}

}
