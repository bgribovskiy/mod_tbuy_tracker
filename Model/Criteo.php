<?php

class Tbuy_Tracker_Model_Criteo {

	public function getHomeTracker(){
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/criteo', 'tracker_criteo', array(
						'template' => 'tbuy/tracker/criteo_home.phtml'
				));
		return $block->toHtml();
	}

	public function getProductTracker(){
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/criteo', 'tracker_criteo', array(
						'template' => 'tbuy/tracker/criteo_product.phtml'
				));
		return $block->toHtml();
	}
	
	public function getCategoryTracker(){
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/criteo', 'tracker_criteo', array(
						'template' => 'tbuy/tracker/criteo_category.phtml'
				));
		return $block->toHtml();
	}
	
	public function getSearchResultTracker(){
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/criteo', 'tracker_criteo', array(
						'template' => 'tbuy/tracker/criteo_searchresult.phtml'
				));
		return $block->toHtml();
	}
	
	public function getCartTracker(){
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/criteo', 'tracker_criteo', array(
						'template' => 'tbuy/tracker/criteo_cart.phtml'
				));
		return $block->toHtml();
	}
	
	public function getCheckoutSuccessTracker(){
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/criteo', 'tracker_criteo', array(
						'template' => 'tbuy/tracker/criteo_checkout_success.phtml'
				));
		return $block->toHtml();
	}
	
}
