<?php

class Tbuy_Tracker_Model_Webtrekk {

	public function updateQuantity($observer) {
		if (!Mage::getStoreConfig("webtrekk/settings/enabled"))
			return;
		$helper = Mage::helper("tracker/webtrekk");
		$helper->log("[TRACKER|WEBTREKK] updateQty");
		$event = $observer->getEvent();
		$cart = $event->getCart();
		/* @var $quote Mage_Checkout_Model_Cart */
		$data = $event->getInfo();

		$layout = Mage::getSingleton('core/layout');
		$layout->getUpdate()->addHandle("tbuy_tracker");

		$products = array();

		foreach ($data as $itemId => $itemInfo) {
			$item = $cart->getQuote()->getItemById($itemId);
			$helper->log("[TRACKER|WEBTREKK] \t ItemId {$itemId}");
			if ($item->getQty() >= $itemInfo['qty'])
				continue;
			if ($item->getParentId() != null)
				$product = Mage::getModel("catalog/product")->load($item->getParentId());
			else
				$product = Mage::getModel("catalog/product")->load($item->getProductId());
			$products[$product->getId()] = $itemInfo['qty'] - $item->getQty();
			$helper->log("[TRACKER|WEBTREKK] \t \t added");
		}
		Mage::getSingleton('core/session')->setCartUpdated(1);
		Mage::getSingleton('core/session')->setProducts($products);
		Mage::getSingleton('core/session')->setItems($item);
	}

	public function appendBlockAfterQtyUpdate() {
		$var = Mage::getSingleton('core/session')->getCartUpdated();
		if (!Mage::getStoreConfig("webtrekk/settings/enabled") || empty($var))
			return;
		$helper = Mage::helper("tracker/webtrekk");
		$helper->log("[TRACKER|WEBTREKK] appendBlockAfterQtyUpdate: found session");
		$products = Mage::getSingleton('core/session')->getProducts();
		if (!$products)
			return;

		$helper->log("[TRACKER|WEBTREKK] appendBlockAfterQtyUpdate: found products");

		$layout = Mage::getSingleton('core/layout');
		$layout->getUpdate()->addHandle("tbuy_tracker");
		foreach ($products as $productId => $qty) {
			$product = Mage::getModel("catalog/product")->load($productId);
			$block = $layout->createBlock(
							'Tbuy_Tracker_Block_Webtrekk', "webtrekk_tracker_{$productId}", array(
						'template' => 'tbuy/tracker/webtrekk_cart.phtml'
					))
					->setTrackingProduct($product)
					->setAddQty($qty);
			$layout->getBlock('before_body_end')->append($block);
		}
		Mage::getSingleton('core/session')->setCartUpdated(0);
		Mage::getSingleton('core/session')->setProducts(null);
		return $this;
	}

	/**
	 * 
	 * public function addedProduct($observer = null) {
	  $helper = Mage::helper("tracker/webtrekk");
	  $helper->log("addedProduct observer called...");
	  $event = $observer->getEvent();
	  $product = $event->getProduct();
	  if (!$product)
	  return;
	  $params = $event->getRequest()->getParams();
	  $qty = isset($params['qty']) ? $params['qty'] : 1;

	  $product = Mage::getModel("catalog/product")
	  ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
	  ->load($product->getId());

	  $productName = Mage::getSingleton('core/session')->getProductName() != "" ? Mage::getSingleton('core/session')->getProductName() : array();
	  $productQty = Mage::getSingleton('core/session')->getProductName() != "" ? Mage::getSingleton('core/session')->getProductQty() : array();

	  $productName[] = str_replace("'", "\'", $product->getName());
	  $productQty[] = $qty;

	  //	Tbuy_AjaxCheckout
	  $isAjax = $event->getIsAjax();
	  if ($isAjax == 1) {
	  $ajaxResponse = $event->getAjaxresp();

	  $layout = Mage::getSingleton('core/layout');
	  $layout->getUpdate()->addHandle("tbuy_tracker");

	  $block = $layout->createBlock(
	  'Tbuy_Tracker_Block_Webtrekk', 'webtrekk_add', array(
	  'template' => 'tbuy/tracker/webtrekk_add.phtml',
	  'product_added' => $product
	  ));
	  $ajaxResponse->setData('extra', $block->toHtml());
	  return;
	  }

	  Mage::getSingleton('core/session')->setAddedToCart(1);
	  Mage::getSingleton('core/session')->setProductName($productName);
	  Mage::getSingleton('core/session')->setProductQty($productQty);
	  $helper->log("Product added");
	  $helper->log("\t Name: " . print_r(Mage::getSingleton('core/session')->getProductName(), true));
	  $helper->log("\t Qty: " . print_r(Mage::getSingleton('core/session')->getProductQty(), true));
	  return;
	  }
	 * 
	 */
}
