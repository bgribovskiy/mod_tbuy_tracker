<?php

class Tbuy_Tracker_Model_Acquisio extends Mage_Core_Block_Template {

	public function getGlobalTracker() {
		if (!Mage::helper("tracker/acquisio")->isEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/acquisio', 'tracker_acquisio', array(
			'template' => 'tbuy/tracker/acquisio/global.phtml'
		));
		return $block->toHtml();
	}

	public function getCheckoutSuccessTracker() {
		if (!Mage::helper("tracker/acquisio")->isEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/acquisio', 'tracker_acquisio', array(
			'template' => 'tbuy/tracker/acquisio/success.phtml'
		));
		return $block->toHtml();
	}

}
