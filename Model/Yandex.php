<?php

class Tbuy_Tracker_Model_Yandex extends Mage_Core_Block_Template {

    public function getGlobalTracker() {
        if (!Mage::helper("tracker/yandex")->isEnabled())
            return "";
        $layout = Mage::getSingleton('core/layout');
        $block = $layout->createBlock(
            'tracker/yandex', 'tracker_yandex', array(
                'template' => 'tbuy/tracker/yandex/global.phtml'
            )
        );
        return $block;
    }

}
