<?php

class Tbuy_Tracker_Model_Facebookpixel extends Mage_Core_Block_Template {

	/**
	 * @return string
     */
	public function getGlobalTracker() {
		if (!Mage::helper("tracker/facebookpixel")->isEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/facebookpixel', 'tracker_facebookpixel', array(
			'template' => 'tbuy/tracker/facebookpixel/global.phtml'
		));
		return $block->toHtml();
	}

	/**
	 * @return string
     */
	public function getCartTracker() {
		if (!Mage::helper("tracker/facebookpixel")->isEnabled())
			return "";
		if (!Mage::helper("tracker/facebookpixel")->isEnabledForAddToCart())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/facebookpixel', 'tracker_facebookpixel', array(
			'template' => 'tbuy/tracker/facebookpixel/cart.phtml'
		));
		return $block->toHtml();
	}

	/**
	 * @return string
     */
	public function getCheckoutSuccessTracker() {
		if (!Mage::helper("tracker/facebookpixel")->isEnabled())
			return "";
		if (!Mage::helper("tracker/facebookpixel")->isEnabledForPurchase())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/facebookpixel', 'tracker_facebookpixel', array(
			'template' => 'tbuy/tracker/facebookpixel/success.phtml'
		));
		return $block->toHtml();
	}

	/**
	 * @return string
     */
	public function getOnestepchekoutTracker(){
		if (!Mage::helper("tracker/facebookpixel")->isEnabled())
			return "";

		if (!Mage::helper("tracker/facebookpixel")->isEnabledForInitiateCheckout())
			return "";

		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
			'tracker/facebookpixel', 'tracker_facebookpixel', array(
			'template' => 'tbuy/tracker/facebookpixel/checkout.phtml'
		));
		return $block->toHtml();
	}

	/**
	 * @return string
     */
	public function getSearchTracker(){
		if (!Mage::helper("tracker/facebookpixel")->isEnabled())
			return "";

		if (!Mage::helper("tracker/facebookpixel")->isEnabledForSearch())
			return "";

		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
			'tracker/facebookpixel', 'tracker_facebookpixel', array(
			'template' => 'tbuy/tracker/facebookpixel/search.phtml'
		));
		return $block->toHtml();
	}

	/**
	 * @return string
     */
	public function getWishlistTracker() {
		if (!Mage::helper("tracker/facebookpixel")->isEnabled())
			return "";
		if (!Mage::helper("tracker/facebookpixel")->isEnabledForAddToWishlist())
			return "";
		
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
			'tracker/facebookpixel', 'tracker_facebookpixel', array(
			'template' => 'tbuy/tracker/facebookpixel/wishlist.phtml'
		));
		return $block->toHtml();
	}

	/**
	 * @return string
     */
	public function getCustomerRegTracker(){
		if (!Mage::helper("tracker/facebookpixel")->isEnabled())
			return "";
		if (!Mage::helper("tracker/facebookpixel")->isEnabledForCompleteRegistration())
			return "";

		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
			'tracker/facebookpixel', 'tracker_facebookpixel', array(
			'template' => 'tbuy/tracker/facebookpixel/registration.phtml'
		));
		return $block->toHtml();
	}

}
