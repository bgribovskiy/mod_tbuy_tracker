<?php

class Tbuy_Tracker_Model_Adform extends Mage_Core_Block_Template {

	public function getGlobalTracker() {
		if (!Mage::helper("tracker/adform")->isEnabled())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/adform', 'tracker_adform', array(
			'template' => 'tbuy/tracker/adform/global.phtml'
		));
		return $block->toHtml();
	}

	public function getCartTracker() {
		if (!Mage::helper("tracker/adform")->isEnabled())
			return "";
		if (!Mage::helper("tracker/adform")->isEnabledForCart())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/adform', 'tracker_adform', array(
			'template' => 'tbuy/tracker/adform/cart.phtml'
		));
		return $block->toHtml();
	}

	public function getCheckoutSuccessTracker() {
		if (!Mage::helper("tracker/adform")->isEnabled())
			return "";
		if (!Mage::helper("tracker/adform")->isEnabledForSuccess())
			return "";
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
				'tracker/adform', 'tracker_adform', array(
			'template' => 'tbuy/tracker/adform/success.phtml'
		));
		return $block->toHtml();
	}

	public function getOnestepchekoutTracker(){
		if (!Mage::helper("tracker/adform")->isEnabled())
			return "";

		if (!Mage::helper("tracker/adform")->isEnabledForCheckout())
			return "";

		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock(
			'tracker/adform', 'tracker_adform', array(
			'template' => 'tbuy/tracker/adform/checkout.phtml'
		));
		return $block->toHtml();
	}

}
